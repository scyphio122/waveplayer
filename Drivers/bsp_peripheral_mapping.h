/*
 * bsp_peripheral_mappings.h
 *
 *  Created on: Jan 18, 2020
 *      Author: konrad
 */

#ifndef DRIVERS_BSP_PERIPHERAL_MAPPING_H_
#define DRIVERS_BSP_PERIPHERAL_MAPPING_H_

#include <gpio.h>
#include <stm32l4xx_hal_gpio.h>
#include <stm32l4xx_hal_gpio_ex.h>

#define LOG_UART_NAME                   "/dev/uart2"
#define LOG_UART			            USART2

#define LOG_UART_RX_PORT	            GPIOD
#define LOG_UART_RX_PIN		            GPIO_IO_6
#define LOG_UART_RX_AF		            GPIO_ALTERNATIVE_FUNCTION_7
#define LOG_UART_RX_PIN_DEVICE_NAME     "/dev/gpiod"

#define LOG_UART_TX_PORT	            GPIOD
#define LOG_UART_TX_PIN		            GPIO_IO_5
#define LOG_UART_TX_AF		            GPIO_ALTERNATIVE_FUNCTION_7
#define LOG_UART_TX_PIN_DEVICE_NAME     "/dev/gpiod"

#define QSPI_CS_PORT                    GPIOE
#define QSPI_CS_PIN                     GPIO_IO_11
#define QSPI_CS_AF                      GPIO_ALTERNATIVE_FUNCTION_10

#define QSPI_CLK_PORT                   GPIOE
#define QSPI_CLK_PIN                    GPIO_IO_10
#define QSPI_CLK_AF                     GPIO_ALTERNATIVE_FUNCTION_10

#define QSPI_D0_PORT                    GPIOE
#define QSPI_D0_PIN                     GPIO_IO_12
#define QSPI_D0_AF                      GPIO_ALTERNATIVE_FUNCTION_10

#define QSPI_D1_PORT                    GPIOE
#define QSPI_D1_PIN                     GPIO_IO_13
#define QSPI_D1_AF                      GPIO_ALTERNATIVE_FUNCTION_10

#define QSPI_D2_PORT                    GPIOE
#define QSPI_D2_PIN                     GPIO_IO_14
#define QSPI_D2_AF                      GPIO_ALTERNATIVE_FUNCTION_10

#define QSPI_D3_PORT                    GPIOE
#define QSPI_D3_PIN                     GPIO_IO_15
#define QSPI_D3_AF                      GPIO_ALTERNATIVE_FUNCTION_10

#define USB_DP_PORT                     GPIOA
#define USB_DP_PIN                      GPIO_IO_12
#define USB_DP_AF                       GPIO_ALTERNATIVE_FUNCTION_10

#define USB_DM_PORT                     GPIOA
#define USB_DM_PIN                      GPIO_IO_11
#define USB_DM_AF                       GPIO_ALTERNATIVE_FUNCTION_10

#define I2C_AUDIO_SDA_PORT_NAME        "/dev/gpiob"
#define I2C_AUDIO_SDA_PORT              GPIOB
#define I2C_AUDIO_SDA_PIN               GPIO_IO_7
#define I2C_AUDIO_SDA_AF                GPIO_ALTERNATIVE_FUNCTION_4

#define I2C_AUDIO_SCL_PORT_NAME        "/dev/gpiob"
#define I2C_AUDIO_SCL_PORT              GPIOB
#define I2C_AUDIO_SCL_PIN               GPIO_IO_6
#define I2C_AUDIO_SCL_AF                GPIO_ALTERNATIVE_FUNCTION_4

#define I2C_AUDIO_DRIVER_NAME           "/dev/i2c1"
#define I2C_AUDIO_INSTANCE              I2C1
#define I2C_AUDIO_CLK_ENABLE            __HAL_RCC_I2C1_CLK_ENABLE

#define CS43L22_RESET_PORT_NAME         "/dev/gpioe"
#define CS43L22_RESET_PORT              GPIOE
#define CS43L22_RESET_PIN               GPIO_IO_3

#define SAI_DRIVER_NAME                 "/dev/sai1"
#define SAI_INSTANCE                    SAI1_Block_A
#define SAI_CLK_ENABLE                  __HAL_RCC_SAI1_CLK_ENABLE

#define SAI_MCK_PORT_NAME               "/dev/gpioe"
#define SAI_MCK_PORT                    GPIOE
#define SAI_MCK_PIN                     GPIO_IO_2
#define SAI_MCK_AF                      GPIO_ALTERNATIVE_FUNCTION_13

#define SAI_SCK_PORT_NAME               "/dev/gpioe"
#define SAI_SCK_PORT                    GPIOE
#define SAI_SCK_PIN                     GPIO_IO_5
#define SAI_SCK_AF                      GPIO_ALTERNATIVE_FUNCTION_13

#define SAI_SD_PORT_NAME                "/dev/gpioe"
#define SAI_SD_PORT                     GPIOE
#define SAI_SD_PIN                      GPIO_IO_6
#define SAI_SD_AF                       GPIO_ALTERNATIVE_FUNCTION_13

#define SAI_FS_PORT_NAME                "/dev/gpioe"
#define SAI_FS_PORT                     GPIOE
#define SAI_FS_PIN                      GPIO_IO_4
#define SAI_FS_AF                       GPIO_ALTERNATIVE_FUNCTION_13

#define AUDIO_BUTTON_PLAY_PORT_NAME     "/dev/gpioa"
#define AUDIO_BUTTON_PLAY_PORT          GPIOA
#define AUDIO_BUTTON_PLAY_PIN           GPIO_IO_0
#define AUDIO_BUTTON_PLAY_EXTI_LINE     EXTI_LINE_0

#define AUDIO_BUTTON_LEFT_PORT_NAME     "/dev/gpioa"
#define AUDIO_BUTTON_LEFT_PORT          GPIOA
#define AUDIO_BUTTON_LEFT_PIN           GPIO_IO_1
#define AUDIO_BUTTON_LEFT_EXTI_LINE     EXTI_LINE_1

#define AUDIO_BUTTON_RIGHT_PORT_NAME    "/dev/gpioa"
#define AUDIO_BUTTON_RIGHT_PORT         GPIOA
#define AUDIO_BUTTON_RIGHT_PIN          GPIO_IO_2
#define AUDIO_BUTTON_RIGHT_EXTI_LINE    EXTI_LINE_2

#define AUDIO_BUTTON_UP_PORT_NAME       "/dev/gpioa"
#define AUDIO_BUTTON_UP_PORT            GPIOA
#define AUDIO_BUTTON_UP_PIN             GPIO_IO_3
#define AUDIO_BUTTON_UP_EXTI_LINE       EXTI_LINE_3

#define AUDIO_BUTTON_DOWN_PORT_NAME     "/dev/gpioa"
#define AUDIO_BUTTON_DOWN_PORT          GPIOA
#define AUDIO_BUTTON_DOWN_PIN           GPIO_IO_5
#define AUDIO_BUTTON_DOWN_EXTI_LINE     EXTI_LINE_5

#endif /* DRIVERS_BSP_PERIPHERAL_MAPPING_H_ */
