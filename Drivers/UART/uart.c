/*
 * uart.c
 *
 *  Created on: Jan 17, 2020
 *      Author: konrad
 */

#include "../UART/uart.h"

#include <bsp_types.h>
#include <gpio.h>
#include <bsp_peripheral_mapping.h>
#include <stm32l4xx_hal_dma.h>
#include <stm32l4xx_hal_gpio.h>
#include <stm32l4xx_hal_uart.h>
#include <stm32l4xx_hal_rcc.h>

#include <driver_manager.h>

typedef enum
{
	UART_MODE_POLLING,
	UART_MODE_IRQ,
	UART_MODE_DMA
} uart_mode_e;

typedef enum
{
	UART_CLOSED,
	UART_OPENED,
} uart_state_e;

typedef struct
{
    const char*             device_name;
    uint32_t                baudrate;
	uart_mode_e				mode;
	uart_state_e 			state;
	bsp_periph_config_t     rx_gpio_config;
    bsp_periph_config_t     tx_gpio_config;
	UART_HandleTypeDef 	    config;
} uart_instance_t;

static bsp_status_e s_uart_initialize(void);

static bsp_status_e s_uart_open(int major, int minor, bsp_open_close_args_t* args);

static bsp_status_e s_uart_close(int major, int minor, bsp_open_close_args_t* args);

static bsp_status_e s_uart_read(int major, int minor, bsp_rw_args_t* args);

static bsp_status_e s_uart_write(int major, int minor, bsp_rw_args_t* args);

static bsp_status_e s_uart_ioctl(int major, int minor, bsp_ioctl_args_t* args);

static bsp_status_e s_uart_enable_clock(USART_TypeDef* instance);

#if 0
static bsp_status_e s_uart_disable_clock(USART_TypeDef* instance);
#endif

static uart_instance_t s_usart_instances[USART_INSTANCES_CNT] = {
		{
		    .device_name        =   LOG_UART_NAME,
            .baudrate           =   115200lu,
            .config.Instance 	=	LOG_UART,
            .rx_gpio_config     =   {
                .port                   =   LOG_UART_RX_PORT,
                .pin                    =   LOG_UART_RX_PIN,
                .alternative_function   =   LOG_UART_RX_AF,
                .fd                     =   -1,
            },
            .tx_gpio_config     =   {
                .port                   =   LOG_UART_TX_PORT,
                .pin                    =   LOG_UART_TX_PIN,
                .alternative_function   =   LOG_UART_TX_AF,
                .fd                     =   -1,
            },
            .config.Init		=	{
                    .BaudRate       =   115200lu,
                    .WordLength		=	UART_WORDLENGTH_8B,
                    .StopBits		=	UART_STOPBITS_1,
                    .Parity			=	UART_PARITY_NONE,
                    .Mode			=	UART_MODE_TX,
                    .HwFlowCtl      =   UART_HWCONTROL_NONE,
                    .OverSampling   =   UART_OVERSAMPLING_16,
                    .OneBitSampling =   UART_ONE_BIT_SAMPLE_DISABLE
            }
		}
};

static driver_instance_t    s_uart_driver_handle[] = {
        {
            .driver_name        =    LOG_UART_NAME,
            .driver_callbacks   =   {
                .init_cb    =  s_uart_initialize,
                .open_cb    =  s_uart_open,
                .close_cb   =  s_uart_close,
                .write_cb   =  s_uart_write,
                .read_cb    =  s_uart_read,
                .ioctl_cb   =  s_uart_ioctl
            }
        }
};

int uart_register(void)
{
    int retval = 0;

    retval = driver_register(s_uart_driver_handle, 1);

    return retval;
}

bsp_status_e s_uart_initialize(void)
{
	bsp_status_e retval = BSP_SUCCESS;

	for (int i=0; i<USART_INSTANCES_CNT; ++i)
	{
        if (retval == 0)
        {
            s_usart_instances[i].rx_gpio_config.fd = driver_open(LOG_UART_RX_PIN_DEVICE_NAME, NULL);
            if (s_usart_instances[i].rx_gpio_config.fd < 0)
            {
                retval = -1;
            }

            if (retval == 0)
            {
                bsp_iop_t iop;
                bsp_ioctl_args_t ioctl_args;
                ioctl_args.command = GPIO_SET_MODE_COMMAND;
                ioctl_args.iop = &iop;
                iop.data0 = GPIO_MODE_AF_PP;
                iop.flags = s_usart_instances[i].rx_gpio_config.pin                     |
                            GPIO_NO_PULL                                                |
                            s_usart_instances[i].rx_gpio_config.alternative_function    |
                            GPIO_SPEED_CONFIG_HIGH;

                retval = driver_ioctl(s_usart_instances[i].rx_gpio_config.fd, &ioctl_args);
            }
        }

        if (retval == 0)
        {
            s_usart_instances[i].tx_gpio_config.fd = driver_open(LOG_UART_TX_PIN_DEVICE_NAME, NULL);
            if (s_usart_instances[i].tx_gpio_config.fd < 0)
            {
                retval = -1;
            }

            if (retval == 0)
            {
                bsp_iop_t iop;
                bsp_ioctl_args_t ioctl_args;
                ioctl_args.command = GPIO_SET_MODE_COMMAND;
                ioctl_args.iop = &iop;
                iop.data0 = GPIO_MODE_AF_PP;
                iop.flags = s_usart_instances[i].tx_gpio_config.pin                     |
                            GPIO_NO_PULL                                                |
                            s_usart_instances[i].tx_gpio_config.alternative_function    |
                            GPIO_SPEED_CONFIG_HIGH;

                retval = driver_ioctl(s_usart_instances[i].tx_gpio_config.fd, &ioctl_args);
            }
        }

        if (retval == 0)
        {
            retval = s_uart_enable_clock(s_usart_instances[i].config.Instance);
            if (retval == BSP_SUCCESS)
            {
                s_usart_instances[i].state = UART_OPENED;
            }

            if (retval == 0)
            {
                s_usart_instances[i].config.Init.BaudRate = s_usart_instances[i].baudrate;
                HAL_StatusTypeDef status = HAL_UART_Init(&s_usart_instances[i].config);
                if (status != HAL_OK)
                {
                    retval = -1;
                }
            }
        }

        if (retval != 0)
        {
            break;
        }
	}

	return retval;
}

bsp_status_e s_uart_open(int major, int minor, bsp_open_close_args_t* args)
{
	bsp_status_e retval = BSP_SUCCESS;

	if ((minor < 0) && (minor >= USART_INSTANCES_CNT))
	{
		retval = BSP_INVALID_ARGS;
	}

	if (retval == BSP_SUCCESS)
	{
	    __HAL_UART_ENABLE(&s_usart_instances[minor].config);
	}

	return retval;
}

bsp_status_e s_uart_close(int major, int minor, bsp_open_close_args_t* args)
{
	bsp_status_e retval = BSP_SUCCESS;

	if ((minor < 0) && (minor >= USART_INSTANCES_CNT))
	{
		retval = BSP_INVALID_ARGS;
	}

	if (retval == BSP_SUCCESS)
	{
        __HAL_UART_DISABLE(&s_usart_instances[minor].config);
	}

	return retval;
}

bsp_status_e s_uart_read(int major, int minor, bsp_rw_args_t* args)
{
	bsp_status_e retval = BSP_SUCCESS;

	if ((minor < 0) && (minor >= USART_INSTANCES_CNT))
	{
		retval = BSP_INVALID_ARGS;
	}

	if (args == NULL)
	{
		retval = BSP_INVALID_ARGS;
	}

	if (retval == BSP_SUCCESS)
	{
		if (s_usart_instances[minor].state != UART_OPENED)
		{
			retval = BSP_INVALID_STATE;
		}
	}

	return retval;
}

bsp_status_e s_uart_write(int major, int minor, bsp_rw_args_t* args)
{
	bsp_status_e retval = BSP_SUCCESS;
	HAL_StatusTypeDef status = HAL_OK;

	if ((minor < 0) && (minor >= USART_INSTANCES_CNT))
	{
		retval = BSP_INVALID_ARGS;
	}

	if (args == NULL)
	{
		retval = BSP_INVALID_ARGS;
	}

	if (retval == BSP_SUCCESS)
	{
		if (args->buffer == NULL)
		{
			retval = BSP_INVALID_ARGS;
		}
	}

	if (retval == BSP_SUCCESS)
	{
		if (s_usart_instances[minor].state != UART_OPENED)
		{
			retval = BSP_INVALID_STATE;
		}
	}

	if (retval == BSP_SUCCESS)
	{
		switch (s_usart_instances[minor].mode)
		{
			case UART_MODE_POLLING:
			{
			    status = HAL_UART_Transmit(
                            &s_usart_instances[minor].config,
                            args->buffer,
                            args->bytes_requested,
                            0xFFFF
			            );
			    if (status != HAL_OK)
			    {
			        retval = -1;
			    }
			} break;

			case UART_MODE_IRQ:
			{
			    retval = BSP_NOT_IMPLEMENTED;
			} break;

			case UART_MODE_DMA:
			{
			    status = HAL_UART_Transmit_DMA(
                            &s_usart_instances[minor].config,
                            args->buffer,
                            args->bytes_requested
			            );
			} break;
		}
	}

	if (retval == BSP_SUCCESS)
	{
	    if (status != HAL_OK)
	    {
	        args->bytes_consumed = 0;
	    }
	    else
	    {
	        args->bytes_consumed = args->bytes_requested;
	    }
	}

	return retval;
}

bsp_status_e s_uart_ioctl(int major, int minor, bsp_ioctl_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;

    return retval;
}

static bsp_status_e s_uart_enable_clock(USART_TypeDef* instance)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (instance == NULL)
    {
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        switch ((uint32_t)instance)
        {
            case (uint32_t)USART1:
            {
                __HAL_RCC_USART1_CLK_ENABLE();
            } break;

            case (uint32_t)USART2:
            {
                __HAL_RCC_USART2_CLK_ENABLE();
            } break;

            case (uint32_t)USART3:
            {
                __HAL_RCC_USART3_CLK_ENABLE();
            } break;

            case (uint32_t)UART4:
            {
                __HAL_RCC_UART4_CLK_ENABLE();
            } break;

            case (uint32_t)UART5:
            {
                __HAL_RCC_UART5_CLK_ENABLE();
            } break;

            default:
            {
                retval = BSP_INVALID_ARGS;
            }
        }
    }

    return retval;
}

#if 0
static bsp_status_e s_uart_disable_clock(USART_TypeDef* instance)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (instance == NULL)
    {
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        switch ((uint32_t)instance)
        {
            case (uint32_t)USART1:
            {
                __HAL_RCC_USART1_CLK_DISABLE();
            } break;

            case (uint32_t)USART2:
            {
                __HAL_RCC_USART2_CLK_DISABLE();
            } break;

            case (uint32_t)USART3:
            {
                __HAL_RCC_USART3_CLK_DISABLE();
            } break;

            case (uint32_t)UART4:
            {
                __HAL_RCC_UART4_CLK_DISABLE();

            } break;

            case (uint32_t)UART5:
            {
                __HAL_RCC_UART5_CLK_DISABLE();
            } break;

            default:
            {
                retval = BSP_INVALID_ARGS;
            }
        }
    }

    return retval;
}
#endif
