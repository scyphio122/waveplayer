/*
 * uart.h
 *
 *  Created on: Jan 17, 2020
 *      Author: konrad
 */

#ifndef DRIVERS_UART_UART_H_
#define DRIVERS_UART_UART_H_

#include <bsp_types.h>

#define USART_INSTANCES_CNT			(1u)

int         uart_register(void);

#endif /* DRIVERS_UART_UART_H_ */
