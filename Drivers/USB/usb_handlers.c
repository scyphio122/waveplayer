/*
 * usb_error_handler.c
 *
 *  Created on: Feb 7, 2020
 *      Author: konrad
 */

#include <log.h>
#include <clock_config.h>

void Error_Handler(void);

void SystemClock_Config(void);

void Error_Handler(void)
{
    LOG_ERROR("USB ERROR");
}

void SystemClock_Config(void)
{
    clock_configure();
}
