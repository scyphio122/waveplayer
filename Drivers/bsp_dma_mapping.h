/*
 * bsp_dma_mapping.h
 *
 *  Created on: Jan 17, 2020
 *      Author: konrad
 */

#ifndef DRIVERS_BSP_DMA_MAPPING_H_
#define DRIVERS_BSP_DMA_MAPPING_H_

#include <stm32l476xx.h>
#include <stm32l4xx_hal_dma.h>

#define I2C_AUDIO_CTRL_TX_DMA_CHANNEL           DMA1_Channel1

#define I2C_AUDIO_CTRL_RX_DMA_CHANNEL           DMA1_Channel2

#define SAI_AUDIO_DMA_INSTANCE                  DMA2
#define SAI_AUDIO_DMA_CHANNEL                   DMA2_Channel1
#define SAI_AUDIO_DMA_REQUEST                   DMA_REQUEST_1
#define SAI_AUDIO_DMA_IRQn                      DMA2_Channel1_IRQn

#define QSPI_RX_DMA_INSTANCE                    DMA1
#define QSPI_RX_DMA_CHANNEL                     DMA1_Channel5
#define QSPI_RX_DMA_REQUEST                     DMA_REQUEST_5
#define QSPI_RX_DMA_IRQn                        DMA1_Channel5_IRQn

#endif /* DRIVERS_BSP_DMA_MAPPING_H_ */
