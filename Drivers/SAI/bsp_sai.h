/*
 * bsp_sai.h
 *
 *  Created on: Feb 19, 2020
 *      Author: konrad
 */

#ifndef DRIVERS_SAI_BSP_SAI_H_
#define DRIVERS_SAI_BSP_SAI_H_

#include <bsp_types.h>
#include <stdint-gcc.h>

typedef enum
{
    BSP_SAI_BITS_PER_SAMPLE_16BIT           =   0U,
    BSP_SAI_BITS_PER_SAMPLE_16BIT_EXTENDED  =   1U,
    BSP_SAI_BITS_PER_SAMPLE_24BIT           =   2U,
    BSP_SAI_BITS_PER_SAMPLE_32BIT           =   3U
} bsp_sai_bits_per_sample_e;

typedef enum
{
    BSP_SAI_AUDIO_FREQUENCY_8K,
    BSP_SAI_AUDIO_FREQUENCY_11K,
    BSP_SAI_AUDIO_FREQUENCY_16K,
    BSP_SAI_AUDIO_FREQUENCY_22K,
    BSP_SAI_AUDIO_FREQUENCY_32K,
    BSP_SAI_AUDIO_FREQUENCY_44K,
    BSP_SAI_AUDIO_FREQUENCY_48K,
    BSP_SAI_AUDIO_FREQUENCY_CNT
} bsp_sai_audio_frequency_e;

typedef struct
{
    bsp_sai_audio_frequency_e   audio_frequency;
    bsp_sai_bits_per_sample_e   bits_per_sample;
    uint32_t                    number_of_slot;
} bsp_sai_config_t;

typedef enum
{
    BSP_SAI_EVENT_TX_HALF_COMPLETED    =   0x00000001lu,
    BSP_SAI_EVENT_TX_COMPLETED         =   0x00000002lu,
    BSP_SAI_EVENT_TX_ERROR             =   0x00000004lu,
    BSP_SAI_EVENT_TX_ABORTED           =   0x00000008lu,
} bsp_sai_event_e;

typedef enum
{
    BSP_SAI_IOCTL_CMD_RECONFIGURE,
    BSP_SAI_IOCTL_CMD_ABORT
} bsp_sai_ioctl_cmd_e;

int bsp_sai_register(void);

bsp_status_e bsp_sai_get_events(bsp_sai_event_e * events, uint32_t timeout_ticks);

#endif /* DRIVERS_SAI_BSP_SAI_H_ */
