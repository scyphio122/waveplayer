/*
 * bsp_sai.c
 *
 *  Created on: Feb 19, 2020
 *      Author: konrad
 */


#include <bsp_sai.h>
#include <gpio.h>
#include <driver_manager.h>
#include <bsp_types.h>

#include <stm32l476xx.h>
#include <stm32l4xx_hal_conf.h>
#include <stm32l4xx_hal_rcc.h>
#include <stm32l4xx_hal_dma.h>
#include <stm32l4xx_hal_sai.h>
#include <stm32l4xx_hal_gpio.h>

#include <log.h>

#include <bsp_peripheral_mapping.h>
#include <bsp_dma_mapping.h>

#include <FreeRTOS.h>
#include <event_groups.h>

#include <stdbool.h>

#define LOG_VERBOSE                 1

#define SAI_INSTANCES_CNT           1lu

#define SAIClockDivider(__FREQUENCY__) \
        (__FREQUENCY__ == BSP_SAI_AUDIO_FREQUENCY_8K)  ? 12 \
      : (__FREQUENCY__ == BSP_SAI_AUDIO_FREQUENCY_11K) ? 2 \
      : (__FREQUENCY__ == BSP_SAI_AUDIO_FREQUENCY_16K) ? 6 \
      : (__FREQUENCY__ == BSP_SAI_AUDIO_FREQUENCY_22K) ? 1 \
      : (__FREQUENCY__ == BSP_SAI_AUDIO_FREQUENCY_32K) ? 3 \
      : (__FREQUENCY__ == BSP_SAI_AUDIO_FREQUENCY_44K) ? 0 \
      : (__FREQUENCY__ == BSP_SAI_AUDIO_FREQUENCY_48K) ? 2 : 1  \


typedef struct
{
    const char*         name;
    SAI_HandleTypeDef   instance;

    uint8_t*            data;
    uint32_t            data_size;
    uint32_t            data_cur_offset;

    DMA_HandleTypeDef   tx_dma_config;

    EventGroupHandle_t  event_group;

} bsp_sai_instance_t;

typedef struct
{
    uint8_t     n_divider;
    uint8_t     p_divider;
    uint8_t     mclk_divider;
} bsp_sai_rcc_divider_cfg_t;

void SAI1_IRQHandler(void);

void DMA2_Channel1_IRQHandler(void);

static bsp_status_e s_bsp_sai_initialize(void);

static bsp_status_e s_bsp_sai_open(int major, int minor, bsp_open_close_args_t* args);

static bsp_status_e s_bsp_sai_close(int major, int minor, bsp_open_close_args_t* args);

static bsp_status_e s_bsp_sai_write(int major, int minor, bsp_rw_args_t* args);

static bsp_status_e s_bsp_sai_read(int major, int minor, bsp_rw_args_t* args);

static bsp_status_e s_bsp_sai_ioctl(int major, int minor, bsp_ioctl_args_t* args);

static void s_bsp_sai_tx_hf_cpltd_cb(struct __SAI_HandleTypeDef *hsai);      /*!< SAI transmit complete callback */

static void s_bsp_sai_tx_cpltd_cb(struct __SAI_HandleTypeDef *hsai);  /*!< SAI transmit half complete callback */

static void s_bsp_sai_tx_err_cb(struct __SAI_HandleTypeDef *hsai);       /*!< SAI error callback */

static int s_bsp_sai_configure_clock(bsp_sai_audio_frequency_e sampling_freq);

static int32_t s_sai_should_yield;

#include <stm32l4xx_hal_rcc.h>

        /* F_s 8000 Hz */
static const bsp_sai_rcc_divider_cfg_t s_bsp_sai_clk_cfg[BSP_SAI_AUDIO_FREQUENCY_CNT] = {
        {
            .n_divider      = 52,
            .p_divider      = RCC_PLLP_DIV17,
            .mclk_divider   = 5
        },
        /* F_s 11025 Hz */
        {
            .n_divider      = 48,
            .p_divider      = RCC_PLLP_DIV17,
            .mclk_divider   = 3
        },
        /* F_s 16000 Hz */
        {
            .n_divider      = 52,
            .p_divider      = RCC_PLLP_DIV17,
            .mclk_divider   = 2
        },
        /* F_s 22050 Hz */
        {
            .n_divider      = 48,
            .p_divider      = RCC_PLLP_DIV17,
            .mclk_divider   = 1
        },
        /* F_s 24000 Hz */
        {
            .n_divider      = 52,
            .p_divider      = RCC_PLLP_DIV17,
            .mclk_divider   = 1
        },
        /* F_s 44100 Hz */
        {
            .n_divider      = 48,
            .p_divider      = RCC_PLLP_DIV17,
            .mclk_divider   = 0
        },
        /* F_s 48000 Hz */
        {
            .n_divider      = 52,
            .p_divider      = RCC_PLLP_DIV17,
            .mclk_divider   = 0
        },

};

static bsp_sai_instance_t s_bsp_sai_instances[SAI_INSTANCES_CNT] = {
        {
                .name               =   "SAI1",
                .instance           =   {
                        .Instance       =   SAI_INSTANCE,
                        .Init       =   {
                                .AudioMode          =   SAI_MODEMASTER_TX,
                                .Synchro            =   SAI_ASYNCHRONOUS,
                                .SynchroExt         =   SAI_SYNCEXT_DISABLE,
                                .OutputDrive        =   SAI_OUTPUTDRIVE_ENABLE,
                                .NoDivider          =   SAI_MASTERDIVIDER_ENABLE,
                                .FIFOThreshold      =   SAI_FIFOTHRESHOLD_HF,
                                .AudioFrequency     =   SAI_AUDIO_FREQUENCY_MCKDIV,
                                .Mckdiv             =   0,
                                .MonoStereoMode     =   SAI_STEREOMODE,
                                .CompandingMode     =   SAI_NOCOMPANDING,
                                .TriState           =   SAI_OUTPUT_NOTRELEASED,
                                .Protocol           =   SAI_FREE_PROTOCOL,
                                .DataSize           =   SAI_DATASIZE_16,
                                .FirstBit           =   SAI_FIRSTBIT_MSB,
                                .ClockStrobing      =   SAI_CLOCKSTROBING_FALLINGEDGE
                        },
                },
                .data               =   NULL,
                .data_size          =   0,
                .data_cur_offset    =   0,
                .tx_dma_config      =   {
                        .Instance  = SAI_AUDIO_DMA_CHANNEL,
                        .Init       =   {
                                .Direction              =   DMA_MEMORY_TO_PERIPH,
                                .Request                =   SAI_AUDIO_DMA_REQUEST,
                                .PeriphInc              =   DMA_PINC_DISABLE,
                                .MemInc                 =   DMA_MINC_ENABLE,
                                .PeriphDataAlignment    =   DMA_PDATAALIGN_HALFWORD,
                                .MemDataAlignment       =   DMA_MDATAALIGN_HALFWORD,
                                .Mode                   =   DMA_CIRCULAR,
                                .Priority               =   DMA_PRIORITY_HIGH
                        },
                },
        }
};

static driver_instance_t    s_sai_driver_handle[] = {
        {
            .driver_name        =    SAI_DRIVER_NAME,
            .driver_callbacks   =   {
                .init_cb    =  s_bsp_sai_initialize,
                .open_cb    =  s_bsp_sai_open,
                .close_cb   =  s_bsp_sai_close,
                .write_cb   =  s_bsp_sai_write,
                .read_cb    =  s_bsp_sai_read,
                .ioctl_cb   =  s_bsp_sai_ioctl
            }
        }
};

int bsp_sai_register(void)
{
    int retval = 0;

    retval = driver_register(s_sai_driver_handle, 1);

    return retval;
}

bsp_status_e bsp_sai_get_events(bsp_sai_event_e * events, uint32_t timeout_ticks)
{
    bsp_status_e retval = BSP_SUCCESS;

    *events = xEventGroupWaitBits(
                            s_bsp_sai_instances[0].event_group,
                            BSP_SAI_EVENT_TX_COMPLETED | BSP_SAI_EVENT_TX_HALF_COMPLETED,
                            BSP_SAI_EVENT_TX_COMPLETED | BSP_SAI_EVENT_TX_HALF_COMPLETED,
                            false,
                            timeout_ticks
                        );

    return retval;
}

static bsp_status_e s_bsp_sai_initialize(void)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (retval == BSP_SUCCESS)
    {
        s_bsp_sai_instances[0].event_group = xEventGroupCreate();
        if (s_bsp_sai_instances[0].event_group == 0)
        {
            LOG_ERROR("FAILED TO CREATE %s EVENT GROUP", s_bsp_sai_instances[0].name);
            retval = -1;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        int sai_mck = driver_open(SAI_MCK_PORT_NAME, NULL);
        if (sai_mck < 0)
        {
            LOG_ERROR("FAILED TO OPEN SAI MCK PORT");
            retval = -1;
        }

        if (retval == 0)
        {
            bsp_iop_t iop;
            bsp_ioctl_args_t ioctl_args;
            ioctl_args.command = GPIO_SET_MODE_COMMAND;
            ioctl_args.iop = &iop;
            iop.data0 = GPIO_MODE_AF_PP;
            iop.flags = SAI_MCK_PIN                     |
                        GPIO_NO_PULL                    |
                        SAI_MCK_AF                      |
                        GPIO_SPEED_CONFIG_VERY_HIGH;

            retval = driver_ioctl(sai_mck, &ioctl_args);
            if (retval != 0)
            {
                LOG_ERROR("FAILED TO INITIALIZE SAI MCK PIN");
            }
        }

        int sai_sck = driver_open(SAI_SCK_PORT_NAME, NULL);
        if (sai_sck < 0)
        {
            LOG_ERROR("FAILED TO OPEN SAI SCK PORT");
            retval = -1;
        }

        if (retval == 0)
        {
            bsp_iop_t iop;
            bsp_ioctl_args_t ioctl_args;
            ioctl_args.command = GPIO_SET_MODE_COMMAND;
            ioctl_args.iop = &iop;
            iop.data0 = GPIO_MODE_AF_PP;
            iop.flags = SAI_SCK_PIN                     |
                        GPIO_NO_PULL                    |
                        SAI_SCK_AF                      |
                        GPIO_SPEED_CONFIG_VERY_HIGH;

            retval = driver_ioctl(sai_sck, &ioctl_args);
            if (retval != 0)
            {
                LOG_ERROR("FAILED TO INITIALIZE SAI SCK PIN");
            }
        }

        int sai_sd = driver_open(SAI_SD_PORT_NAME, NULL);
        if (sai_sd < 0)
        {
            LOG_ERROR("FAILED TO OPEN SAI SD PORT");
            retval = -1;
        }

        if (retval == 0)
        {
            bsp_iop_t iop;
            bsp_ioctl_args_t ioctl_args;
            ioctl_args.command = GPIO_SET_MODE_COMMAND;
            ioctl_args.iop = &iop;
            iop.data0 = GPIO_MODE_AF_PP;
            iop.flags = SAI_SD_PIN                     |
                        GPIO_NO_PULL                   |
                        SAI_SD_AF                      |
                        GPIO_SPEED_CONFIG_VERY_HIGH;

            retval = driver_ioctl(sai_sd, &ioctl_args);
            if (retval != 0)
            {
                LOG_ERROR("FAILED TO INITIALIZE SAI SD PIN");
            }
        }

        int sai_fs = driver_open(SAI_FS_PORT_NAME, NULL);
        if (sai_fs < 0)
        {
            LOG_ERROR("FAILED TO OPEN SAI FS PORT");
            retval = -1;
        }

        if (retval == 0)
        {
            bsp_iop_t iop;
            bsp_ioctl_args_t ioctl_args;
            ioctl_args.command = GPIO_SET_MODE_COMMAND;
            ioctl_args.iop = &iop;
            iop.data0 = GPIO_MODE_AF_PP;
            iop.flags = SAI_FS_PIN                     |
                        GPIO_NO_PULL                   |
                        SAI_FS_AF                      |
                        GPIO_SPEED_CONFIG_VERY_HIGH;

            retval = driver_ioctl(sai_fs, &ioctl_args);
            if (retval != 0)
            {
                LOG_ERROR("FAILED TO INITIALIZE SAI FS PIN");
            }
        }
    }

    if (retval == BSP_SUCCESS)
    {
        SAI_CLK_ENABLE();

        __HAL_LINKDMA(&s_bsp_sai_instances[0].instance, hdmatx, s_bsp_sai_instances[0].tx_dma_config);
    }

    if (retval == 0)
    {
        __HAL_RCC_DMA2_CLK_ENABLE();

        HAL_StatusTypeDef status = HAL_OK;
        status = HAL_DMA_Init(&s_bsp_sai_instances[0].tx_dma_config);
        if (status != HAL_OK)
        {
            LOG_ERROR("FAILED TO CONFIGURE SAI TX DMA CHANNEL");
            retval = -1;
        }
        else
        {
            HAL_NVIC_ClearPendingIRQ(SAI_AUDIO_DMA_IRQn);
            HAL_NVIC_SetPriority(SAI_AUDIO_DMA_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY, 0);
            HAL_NVIC_EnableIRQ(SAI_AUDIO_DMA_IRQn);
        }
    }

    return retval;
}

static bsp_status_e s_bsp_sai_open(int major, int minor, bsp_open_close_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;
    HAL_StatusTypeDef status = HAL_OK;

    if ((minor < 0) || (minor >= SAI_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID MINOR NUMBER: %ld", minor);
        retval = BSP_INTERNAL_ERROR;
    }

    if (retval == BSP_SUCCESS)
    {
        if (args == NULL)
        {
            LOG_ERROR("NULL ARGS PROVIDED");
            retval = BSP_INVALID_ARGS;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        if (args->iop == NULL)
        {
            LOG_ERROR("NULL IOP PROVIDED");
            retval = BSP_INVALID_ARGS;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        if (args->iop->data1 == NULL)
        {
            LOG_ERROR("NULL IOP BSP_SAI_OPEN STRUCT PROVIDED");
            retval = BSP_INVALID_ARGS;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        status = HAL_SAI_DeInit(&s_bsp_sai_instances[minor].instance);
        if (status != HAL_OK)
        {
            LOG_ERROR("FAILED TO DEINITIALIZE SAI INTERFACE");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        bsp_sai_config_t* sai_config = (bsp_sai_config_t*)args->iop->data1;

        /* Disable SAI peripheral to allow access to SAI internal registers */
        __HAL_SAI_DISABLE(&s_bsp_sai_instances[minor].instance);

        s_bsp_sai_configure_clock(sai_config->audio_frequency);

        /* Configure SAI_Block_x
        LSBFirst: Disabled
        DataSize: 16 */
        s_bsp_sai_instances[minor].instance.Init.AudioMode      = SAI_MODEMASTER_TX;
        s_bsp_sai_instances[minor].instance.Init.Synchro        = SAI_ASYNCHRONOUS;
        s_bsp_sai_instances[minor].instance.Init.SynchroExt     = SAI_SYNCEXT_DISABLE;
        s_bsp_sai_instances[minor].instance.Init.OutputDrive    = SAI_OUTPUTDRIVE_ENABLE;
        s_bsp_sai_instances[minor].instance.Init.NoDivider      = SAI_MASTERDIVIDER_ENABLE;
        s_bsp_sai_instances[minor].instance.Init.FIFOThreshold  = SAI_FIFOTHRESHOLD_1QF;
        s_bsp_sai_instances[minor].instance.Init.AudioFrequency = SAI_AUDIO_FREQUENCY_MCKDIV;
        s_bsp_sai_instances[minor].instance.Init.Mckdiv         = s_bsp_sai_clk_cfg[sai_config->audio_frequency].mclk_divider;
        s_bsp_sai_instances[minor].instance.Init.MonoStereoMode = SAI_STEREOMODE;
        s_bsp_sai_instances[minor].instance.Init.CompandingMode = SAI_NOCOMPANDING;
        s_bsp_sai_instances[minor].instance.Init.TriState       = SAI_OUTPUT_NOTRELEASED;
        s_bsp_sai_instances[minor].instance.Init.Protocol       = SAI_FREE_PROTOCOL;
        s_bsp_sai_instances[minor].instance.Init.DataSize       = SAI_DATASIZE_16;
        s_bsp_sai_instances[minor].instance.Init.FirstBit       = SAI_FIRSTBIT_MSB;
        s_bsp_sai_instances[minor].instance.Init.ClockStrobing  = SAI_CLOCKSTROBING_FALLINGEDGE;

        /* Configure SAI_Block_x Frame
        Frame Length: 32
        Frame active Length: 16
        FS Definition: Start frame + Channel Side identification
        FS Polarity: FS active Low
        FS Offset: FS asserted one bit before the first bit of slot 0 */
        s_bsp_sai_instances[minor].instance.FrameInit.FrameLength       = 32;
        s_bsp_sai_instances[minor].instance.FrameInit.ActiveFrameLength = 16;
        s_bsp_sai_instances[minor].instance.FrameInit.FSDefinition      = SAI_FS_CHANNEL_IDENTIFICATION;
        s_bsp_sai_instances[minor].instance.FrameInit.FSPolarity        = SAI_FS_ACTIVE_LOW;
        s_bsp_sai_instances[minor].instance.FrameInit.FSOffset          = SAI_FS_BEFOREFIRSTBIT;

        /* Configure SAI Block_x Slot
        Slot First Bit Offset: 0
        Slot Size  : 16
        Slot Number: 2
        Slot Active: Slots 0 and 1 actives */
        s_bsp_sai_instances[minor].instance.SlotInit.FirstBitOffset     = 0;
        s_bsp_sai_instances[minor].instance.SlotInit.SlotSize           = SAI_SLOTSIZE_DATASIZE;
        s_bsp_sai_instances[minor].instance.SlotInit.SlotNumber         = 2;
        s_bsp_sai_instances[minor].instance.SlotInit.SlotActive         = SAI_SLOTACTIVE_0 | SAI_SLOTACTIVE_1;

        /* Initializes the SAI peripheral*/
        if (HAL_SAI_Init(&s_bsp_sai_instances[minor].instance) != HAL_OK)
        {
            LOG_ERROR("FAILED TO INITIALIZE SAI PROTOCOL");
            retval = BSP_INTERNAL_ERROR;
        }
        else
        {
            s_bsp_sai_instances[minor].instance.hdmatx                      = &s_bsp_sai_instances[minor].tx_dma_config;
            s_bsp_sai_instances[minor].instance.TxHalfCpltCallback          = s_bsp_sai_tx_hf_cpltd_cb;
            s_bsp_sai_instances[minor].instance.TxCpltCallback              = s_bsp_sai_tx_cpltd_cb;
            s_bsp_sai_instances[minor].instance.ErrorCallback               = s_bsp_sai_tx_err_cb;

            /* Enable SAI peripheral to generate MCLK */
            __HAL_SAI_ENABLE(&s_bsp_sai_instances[minor].instance);
        }
    }

    return retval;
}


static bsp_status_e s_bsp_sai_close(int major, int minor, bsp_open_close_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;
    HAL_StatusTypeDef status = HAL_OK;

    if ((minor < 0) || (minor >= SAI_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID MINOR NUMBER: %ld", minor);
        retval = -1;
    }

    if (retval == BSP_SUCCESS)
    {
        xEventGroupClearBits(s_bsp_sai_instances[minor].event_group,
                    BSP_SAI_EVENT_TX_COMPLETED          |
                    BSP_SAI_EVENT_TX_HALF_COMPLETED     |
                    BSP_SAI_EVENT_TX_ABORTED            |
                    BSP_SAI_EVENT_TX_ERROR
                );

        status = HAL_SAI_DeInit(&s_bsp_sai_instances[minor].instance);
        if (status != HAL_OK)
        {
            LOG_ERROR("FAILED TO DEINITIALIZE SAI INTERFACE");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    return retval;
}

static bsp_status_e s_bsp_sai_write(int major, int minor, bsp_rw_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;
    bsp_rw_args_t* arg = (bsp_rw_args_t*)args;

    if ((minor < 0) || (minor >= SAI_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID MINOR NUMBER: %ld", minor);
        retval = -1;
    }

    if (args == NULL)
    {
        LOG_ERROR("NULL ARGS PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        if (arg->iop == NULL)
        {
            LOG_ERROR("NULL IOP STRUCT PTR");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        arg->bytes_consumed = 0;

        HAL_StatusTypeDef status = HAL_SAI_Transmit_DMA(
                                        &s_bsp_sai_instances[minor].instance,
                                        arg->buffer,
                                        arg->bytes_requested / sizeof(uint16_t)
                                    );
        if (status != HAL_OK)
        {
            LOG_ERROR("FAILED TO TRANSMIT THE AUDIO SAMPLES VIA SAI");
            retval = BSP_INTERNAL_ERROR;
        }
        else
        {
            arg->bytes_consumed = arg->bytes_requested;
        }
    }


    return retval;
}


static bsp_status_e s_bsp_sai_read(int major, int minor, bsp_rw_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;
    bsp_rw_args_t* arg = (bsp_rw_args_t*)args;

    if ((minor < 0) || (minor >= SAI_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID MINOR NUMBER: %ld", minor);
        retval = -1;
    }

    if (args == NULL)
    {
        LOG_ERROR("NULL ARGS PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        if (arg->iop == NULL)
        {
            LOG_ERROR("NULL IOP STRUCT PTR");
            retval = -1;
        }
    }

    return retval;
}

static bsp_status_e s_bsp_sai_ioctl(int major, int minor, bsp_ioctl_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;

    if ((minor < 0) || (minor >= SAI_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID MINOR NUMBER: %ld", minor);
        retval = -1;
    }

    if (args == NULL)
    {
        LOG_ERROR("NULL ARGS PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        switch (args->command)
        {
            case BSP_SAI_IOCTL_CMD_RECONFIGURE:
            {
                if (retval == BSP_SUCCESS)
                {
                    if (args->iop == NULL)
                    {
                        LOG_ERROR("NULL IOP PROVIDED");
                        retval = BSP_INVALID_ARGS;
                    }
                }

                if (retval == BSP_SUCCESS)
                {
                    if (args->iop->data1 == NULL)
                    {
                        LOG_ERROR("NULL IOP BSP_SAI_OPEN STRUCT PROVIDED");
                        retval = BSP_INVALID_ARGS;
                    }
                }

                if (retval == BSP_SUCCESS)
                {
                    bsp_sai_config_t* sai_config = (bsp_sai_config_t*)args->iop->data1;

                    __HAL_SAI_DISABLE(&s_bsp_sai_instances[minor].instance);
                    s_bsp_sai_instances[minor].instance.Init.AudioFrequency = (uint32_t)sai_config->audio_frequency;

                    HAL_StatusTypeDef status = HAL_SAI_InitProtocol(
                                                &s_bsp_sai_instances[minor].instance,
                                                SAI_I2S_STANDARD,
                                                (uint32_t)sai_config->bits_per_sample,
                                                sai_config->number_of_slot
                                            );
                    if (status != HAL_OK)
                    {
                        LOG_ERROR("FAILED TO REINITIALIZE SAI PROTOCOL");
                        retval = BSP_INTERNAL_ERROR;
                    }
                    else
                    {
                        s_bsp_sai_instances[minor].instance.hdmatx                      = &s_bsp_sai_instances[minor].tx_dma_config;
                        s_bsp_sai_instances[minor].instance.TxHalfCpltCallback          = s_bsp_sai_tx_hf_cpltd_cb;
                        s_bsp_sai_instances[minor].instance.TxCpltCallback              = s_bsp_sai_tx_cpltd_cb;
                        s_bsp_sai_instances[minor].instance.ErrorCallback               = s_bsp_sai_tx_err_cb;

                        __HAL_SAI_ENABLE(&s_bsp_sai_instances[minor].instance);
                    }
                }
            } break;

            case BSP_SAI_IOCTL_CMD_ABORT:
            {
                LOG_INFO("ABORTING SAI DMA TRANSFER");
                HAL_StatusTypeDef status = HAL_SAI_Abort(&s_bsp_sai_instances[minor].instance);
                if (status != HAL_OK)
                {
                    LOG_ERROR("FAILED TO ABORT HAL SAI TRANSFER. STATUS: %lu", status);
                }
            } break;

            default:
            {
                LOG_ERROR("UNKNOWN SAI COMMAND: %lu", args->command);
            }
        }
    }

    return retval;
}


void s_bsp_sai_tx_hf_cpltd_cb(struct __SAI_HandleTypeDef *hsai)      /*!< SAI transmit complete callback */
{
    for (uint32_t i=0; i<SAI_INSTANCES_CNT; ++i)
    {
        if (&s_bsp_sai_instances[i].instance == hsai)
        {
            xEventGroupSetBitsFromISR(s_bsp_sai_instances[i].event_group, BSP_SAI_EVENT_TX_HALF_COMPLETED, &s_sai_should_yield);
            break;
        }
    }
}

void s_bsp_sai_tx_cpltd_cb(struct __SAI_HandleTypeDef *hsai)  /*!< SAI transmit half complete callback */
{
    for (uint32_t i=0; i<SAI_INSTANCES_CNT; ++i)
    {
        if (&s_bsp_sai_instances[i].instance == hsai)
        {
            xEventGroupSetBitsFromISR(s_bsp_sai_instances[i].event_group, BSP_SAI_EVENT_TX_COMPLETED, &s_sai_should_yield);
            break;
        }
    }
}

void s_bsp_sai_tx_err_cb(struct __SAI_HandleTypeDef *hsai)
{
    for (uint32_t i=0; i<SAI_INSTANCES_CNT; ++i)
    {
        if (&s_bsp_sai_instances[i].instance == hsai)
        {
            xEventGroupSetBitsFromISR(s_bsp_sai_instances[i].event_group, BSP_SAI_EVENT_TX_ERROR, NULL);
            break;
        }
    }
}

static int s_bsp_sai_configure_clock(bsp_sai_audio_frequency_e sampling_freq)
{
    int retval = 0;

    if (sampling_freq >= BSP_SAI_AUDIO_FREQUENCY_CNT)
    {
        LOG_ERROR("INVALID SAMPLNG FREQ SETTING");
        retval = -1;
    }

    if (retval == 0)
    {
        RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

        HAL_RCCEx_GetPeriphCLKConfig(&PeriphClkInit);
        PeriphClkInit.PeriphClockSelection      = RCC_PERIPHCLK_SAI1;

        PeriphClkInit.PLLSAI2.PLLSAI2Source     = RCC_PLLSOURCE_MSI;
        PeriphClkInit.PLLSAI2.PLLSAI2N          = s_bsp_sai_clk_cfg[sampling_freq].n_divider;
        PeriphClkInit.PLLSAI2.PLLSAI2P          = s_bsp_sai_clk_cfg[sampling_freq].p_divider;
        PeriphClkInit.PLLSAI2.PLLSAI2R          = RCC_PLLR_DIV2;
        PeriphClkInit.PLLSAI2.PLLSAI2ClockOut   = RCC_PLLSAI2_SAI2CLK;

        PeriphClkInit.Sai1ClockSelection        = RCC_SAI1CLKSOURCE_PLLSAI2;

        if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
        {
            LOG_ERROR("FAILED TO CONFIGURE PLLSAI2");
            retval = -1;
        }
    }

    return retval;
}

void SAI1_IRQHandler(void)
{
    HAL_SAI_IRQHandler(&s_bsp_sai_instances[0].instance);
}

void DMA2_Channel1_IRQHandler(void)
{
    HAL_DMA_IRQHandler(&s_bsp_sai_instances[0].tx_dma_config);
    portYIELD_FROM_ISR(s_sai_should_yield);
}
