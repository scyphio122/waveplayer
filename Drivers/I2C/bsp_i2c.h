/*
 * bsp_i2c.h
 *
 *  Created on: Feb 19, 2020
 *      Author: konrad
 */

#ifndef DRIVERS_I2C_BSP_I2C_H_
#define DRIVERS_I2C_BSP_I2C_H_

int bsp_i2c_register(void);

#endif /* DRIVERS_I2C_BSP_I2C_H_ */
