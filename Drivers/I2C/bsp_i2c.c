/*
 * bsp_i2c.c
 *
 *  Created on: Feb 19, 2020
 *      Author: konrad
 */


#include <bsp_i2c.h>
#include <gpio.h>
#include <driver_manager.h>

#include <stm32l476xx.h>
#include <stm32l4xx_hal_conf.h>
#include <stm32l4xx_hal_rcc.h>
#include <stm32l4xx_hal_dma.h>
#include <stm32l4xx_hal_i2c.h>

#include <log.h>

#include <bsp_peripheral_mapping.h>
#include <bsp_dma_mapping.h>

#include <FreeRTOS.h>
#include <event_groups.h>

#include <stdbool.h>

#define LOG_VERBOSE                 0

#define I2C_INSTANCES_CNT           1lu

#define I2C_TX_COMPLETED            0x00000001lu
#define I2C_RX_COMPLETED            0x00000002lu
#define I2C_TX_ERROR                0x00000004lu
#define I2C_RX_ERROR                0x00000008lu

typedef enum
{
    BSP_I2C_NO_TRANSACTION,
    BSP_I2C_WRITE,
    BSP_I2C_READ
} bsp_i2c_transaction_type_e;

typedef struct
{
    const char*         name;
    I2C_HandleTypeDef   instance;

    bsp_i2c_transaction_type_e transaction_type;

    uint8_t*            data;
    uint32_t            data_size;
    uint32_t            data_cur_offset;

    DMA_HandleTypeDef   tx_dma_config;
    DMA_HandleTypeDef   rx_dma_config;

    EventGroupHandle_t  event_group;

} bsp_i2c_instance_t;

static bsp_status_e s_bsp_i2c_initialize(void);

static bsp_status_e s_bsp_i2c_open(int major, int minor, bsp_open_close_args_t* args);

static bsp_status_e s_bsp_i2c_close(int major, int minor, bsp_open_close_args_t* args);

static bsp_status_e s_bsp_i2c_write(int major, int minor, bsp_rw_args_t* args);

static bsp_status_e s_bsp_i2c_read(int major, int minor, bsp_rw_args_t* args);

static bsp_status_e s_bsp_i2c_ioctl(int major, int minor, bsp_ioctl_args_t* args);

static void s_bsp_i2c_tx_completed(struct __I2C_HandleTypeDef * hdma);

static void s_bsp_i2c_rx_completed(struct __I2C_HandleTypeDef * hdma);

static void s_bsp_i2c_error(struct __I2C_HandleTypeDef * hdma);

void I2C1_EV_IRQHandler(void);

void I2C1_ER_IRQHandler(void);

static bsp_i2c_instance_t s_bsp_i2c_instances[I2C_INSTANCES_CNT] = {
        {
                .name               =   "I2C1",
                .instance           =   {
                        .Instance       =   I2C_AUDIO_INSTANCE,
                        .Mode           =   HAL_I2C_MODE_MASTER,
                },
                .data               =   NULL,
                .data_size          =   0,
                .data_cur_offset    =   0,
                .tx_dma_config      =   {
                        .Instance  = I2C_AUDIO_CTRL_TX_DMA_CHANNEL,
                        .Init       =   {
                                .Request    =   DMA_REQUEST_0,
                                .Direction  =   DMA_MEMORY_TO_PERIPH,
                                .PeriphInc  =   DMA_PINC_DISABLE,
                                .MemInc                 =   DMA_MINC_ENABLE,
                                .PeriphDataAlignment    =   DMA_PDATAALIGN_BYTE,
                                .MemDataAlignment       =   DMA_MDATAALIGN_BYTE,
                                .Mode                   =   DMA_NORMAL,
                                .Priority               =   DMA_PRIORITY_LOW
                        },
                },
                .rx_dma_config      =   {
                        .Instance  = I2C_AUDIO_CTRL_RX_DMA_CHANNEL,
                        .Init       =   {
                                .Request    =   DMA_REQUEST_0,
                                .Direction  =   DMA_PERIPH_TO_MEMORY,
                                .PeriphInc  =   DMA_PINC_DISABLE,
                                .MemInc                 =   DMA_MINC_ENABLE,
                                .PeriphDataAlignment    =   DMA_PDATAALIGN_BYTE,
                                .MemDataAlignment       =   DMA_MDATAALIGN_BYTE,
                                .Mode                   =   DMA_NORMAL,
                                .Priority               =   DMA_PRIORITY_LOW
                        },
                },
        }
};

static driver_instance_t    s_i2c_driver_handle[] = {
        {
            .driver_name        =    I2C_AUDIO_DRIVER_NAME,
            .driver_callbacks   =   {
                .init_cb    =  s_bsp_i2c_initialize,
                .open_cb    =  s_bsp_i2c_open,
                .close_cb   =  s_bsp_i2c_close,
                .write_cb   =  s_bsp_i2c_write,
                .read_cb    =  s_bsp_i2c_read,
                .ioctl_cb   =  s_bsp_i2c_ioctl
            }
        }
};

int bsp_i2c_register(void)
{
    int retval = 0;

    retval = driver_register(s_i2c_driver_handle, 1);

    return retval;
}

static bsp_status_e s_bsp_i2c_initialize(void)
{
    bsp_status_e retval = BSP_SUCCESS;
    HAL_StatusTypeDef status = HAL_OK;

    if (retval == 0)
    {
        s_bsp_i2c_instances[0].event_group = xEventGroupCreate();
        if (s_bsp_i2c_instances[0].event_group == 0)
        {
            LOG_ERROR("FAILED TO CREATE %s EVENT GROUP", s_bsp_i2c_instances[0].name);
            retval = -1;
        }
        else
        {
            s_bsp_i2c_instances[0].transaction_type = BSP_I2C_NO_TRANSACTION;
            xEventGroupClearBits(s_bsp_i2c_instances[0].event_group, 0xFFFF);
        }
    }

    if (retval == 0)
    {
        __HAL_RCC_DMA1_CLK_ENABLE();
        I2C_AUDIO_CLK_ENABLE();
    }

    if (retval == 0)
    {
        status = HAL_DMA_Init(&s_bsp_i2c_instances[0].tx_dma_config);
        if (status != HAL_OK)
        {
            LOG_ERROR("FAILED TO CONFIGURE I2C TX DMA CHANNEL");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        status = HAL_DMA_Init(&s_bsp_i2c_instances[0].rx_dma_config);
        if (status != HAL_OK)
        {
            LOG_ERROR("FAILED TO CONFIGURE I2C RX DMA CHANNEL");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        I2C_InitTypeDef init_config;
        init_config.OwnAddress1         =   0x00;
        init_config.Timing              =   0x00909BEB;
        init_config.AddressingMode      =   I2C_ADDRESSINGMODE_7BIT;
        init_config.DualAddressMode     =   I2C_DUALADDRESS_DISABLE;
        init_config.OwnAddress2         =   0x00;
        init_config.OwnAddress2Masks    =   I2C_OA2_NOMASK;
        init_config.GeneralCallMode     =   I2C_GENERALCALL_DISABLE;
        init_config.NoStretchMode       =   I2C_NOSTRETCH_DISABLE;

        s_bsp_i2c_instances[0].instance.Init    =   init_config;
        s_bsp_i2c_instances[0].instance.hdmatx  =   &s_bsp_i2c_instances[0].tx_dma_config;
        s_bsp_i2c_instances[0].instance.hdmarx  =   &s_bsp_i2c_instances[0].rx_dma_config;

        s_bsp_i2c_instances[0].tx_dma_config.Parent = (void*)&s_bsp_i2c_instances[0].instance;
        s_bsp_i2c_instances[0].rx_dma_config.Parent = (void*)&s_bsp_i2c_instances[0].instance;

        status = HAL_I2C_Init(&s_bsp_i2c_instances[0].instance);
        if (status != HAL_OK)
        {
            LOG_ERROR("FAILED TO INITIALIZE I2C AT MINOR 0");
            retval = -1;
        }
        else
        {
            s_bsp_i2c_instances[0].instance.MasterTxCpltCallback   = s_bsp_i2c_tx_completed;
            s_bsp_i2c_instances[0].instance.MasterRxCpltCallback   = s_bsp_i2c_rx_completed;
            s_bsp_i2c_instances[0].instance.ErrorCallback          = s_bsp_i2c_error;
        }
    }

    if (retval == 0)
    {
        int i2c_sck_pin = driver_open(I2C_AUDIO_SDA_PORT_NAME, NULL);
        if (i2c_sck_pin< 0)
        {
            retval = -1;
        }

        if (retval == 0)
        {
            bsp_iop_t iop;
            bsp_ioctl_args_t ioctl_args;
            ioctl_args.command = GPIO_SET_MODE_COMMAND;
            ioctl_args.iop = &iop;
            iop.data0 = GPIO_MODE_AF_OD;
            iop.flags = I2C_AUDIO_SDA_PIN               |
                        GPIO_PULL_UP                    |
                        I2C_AUDIO_SDA_AF                |
                        GPIO_SPEED_CONFIG_HIGH;

            retval = driver_ioctl(i2c_sck_pin, &ioctl_args);
            if (retval != 0)
            {
                LOG_ERROR("FAILED TO INITIALIZE I2C SDA PIN");
            }
        }
    }

    if (retval == 0)
    {
        int i2c_sck_pin = driver_open(I2C_AUDIO_SCL_PORT_NAME, NULL);
        if (i2c_sck_pin< 0)
        {
            retval = -1;
        }

        if (retval == 0)
        {
            bsp_iop_t iop;
            bsp_ioctl_args_t ioctl_args;
            ioctl_args.command = GPIO_SET_MODE_COMMAND;
            ioctl_args.iop = &iop;
            iop.data0 = GPIO_MODE_AF_OD;
            iop.flags = I2C_AUDIO_SCL_PIN               |
                        GPIO_PULL_UP                    |
                        I2C_AUDIO_SCL_AF                |
                        GPIO_SPEED_CONFIG_HIGH;

            retval = driver_ioctl(i2c_sck_pin, &ioctl_args);
            if (retval != 0)
            {
                LOG_ERROR("FAILED TO INITIALIZE I2C SCL PIN");
            }
        }
    }

    if (retval == BSP_SUCCESS)
    {
        HAL_NVIC_SetPriority(I2C1_EV_IRQn, 7, 0);
        HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);
        HAL_NVIC_SetPriority(I2C1_ER_IRQn, 6, 0);
        HAL_NVIC_EnableIRQ(I2C1_ER_IRQn);
    }

    return retval;
}

static bsp_status_e s_bsp_i2c_open(int major, int minor, bsp_open_close_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;

    if ((minor < 0) || (minor >= I2C_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID MINOR NUMBER: %ld", minor);
        retval = -1;
    }

    return retval;
}


static bsp_status_e s_bsp_i2c_close(int major, int minor, bsp_open_close_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;

    if ((minor < 0) || (minor >= I2C_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID MINOR NUMBER: %ld", minor);
        retval = -1;
    }

    return retval;
}

static bsp_status_e s_bsp_i2c_write(int major, int minor, bsp_rw_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;
    bsp_rw_args_t* arg = (bsp_rw_args_t*)args;

    if ((minor < 0) || (minor >= I2C_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID MINOR NUMBER: %ld", minor);
        retval = -1;
    }

    if (args == NULL)
    {
        LOG_ERROR("NULL ARGS PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        if (arg->iop == NULL)
        {
            LOG_ERROR("NULL IOP STRUCT PTR");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        if (s_bsp_i2c_instances[0].transaction_type != BSP_I2C_NO_TRANSACTION)
        {
            LOG_ERROR("I2C TRANSACTION IN PROGRESS");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        uint16_t address = (uint16_t)arg->flags;

        arg->bytes_consumed = 0;

#if LOG_VERBOSE == 1
        LOG_TRACE("I2C TX START...");
#endif
        s_bsp_i2c_instances[0].transaction_type = BSP_I2C_WRITE;

        xEventGroupClearBits(
                s_bsp_i2c_instances[minor].event_group,
                I2C_TX_COMPLETED | I2C_TX_ERROR
        );
#if 0
        HAL_StatusTypeDef status = HAL_I2C_Master_Transmit_DMA(
                    &s_bsp_i2c_instances[minor].instance,
                    address,
                    arg->buffer,
                    arg->bytes_requested
                );
#endif

        HAL_StatusTypeDef status = HAL_I2C_Master_Transmit_IT(
                    &s_bsp_i2c_instances[minor].instance,
                    address,
                    arg->buffer,
                    arg->bytes_requested
                );
        if (status != HAL_OK)
        {
            LOG_ERROR("FAILED TO TRANSMIT THE I2C DATA USING DMA");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        EventBits_t events = xEventGroupWaitBits(
                                s_bsp_i2c_instances[minor].event_group,
                                I2C_TX_COMPLETED | I2C_TX_ERROR,
                                I2C_TX_COMPLETED | I2C_TX_ERROR,
                                false,
                                portMAX_DELAY
                            );

        if (events & I2C_TX_ERROR)
        {
            LOG_ERROR("I2C TX ERROR!");
        }

        if (events & I2C_TX_COMPLETED)
        {
            arg->bytes_consumed = arg->bytes_requested;
#if LOG_VERBOSE == 1
            LOG_TRACE("I2C TX COMPLETED");
#endif
        }
    }

    s_bsp_i2c_instances[0].transaction_type = BSP_I2C_NO_TRANSACTION;

    return retval;
}


static bsp_status_e s_bsp_i2c_read(int major, int minor, bsp_rw_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;
    bsp_rw_args_t* arg = (bsp_rw_args_t*)args;

    if ((minor < 0) || (minor >= I2C_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID MINOR NUMBER: %ld", minor);
        retval = -1;
    }

    if (args == NULL)
    {
        LOG_ERROR("NULL ARGS PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        if (arg->iop == NULL)
        {
            LOG_ERROR("NULL IOP STRUCT PTR");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        if (s_bsp_i2c_instances[0].transaction_type != BSP_I2C_NO_TRANSACTION)
        {
            LOG_ERROR("I2C TRANSACTION IN PROGRESS");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        uint16_t address = (uint16_t)arg->flags;

        arg->bytes_consumed = 0;
        s_bsp_i2c_instances[0].transaction_type = BSP_I2C_READ;

#if LOG_VERBOSE == 1
        LOG_TRACE("I2C RX START...");
#endif

        xEventGroupClearBits(
                s_bsp_i2c_instances[minor].event_group,
                I2C_TX_COMPLETED | I2C_TX_ERROR
        );

#if 0
        HAL_StatusTypeDef status = HAL_I2C_Master_Receive_DMA(
                    &s_bsp_i2c_instances[minor].instance,
                    address,
                    arg->buffer,
                    arg->bytes_requested
                );
#endif
        HAL_StatusTypeDef status = HAL_I2C_Master_Receive_IT(
                    &s_bsp_i2c_instances[minor].instance,
                    address,
                    arg->buffer,
                    arg->bytes_requested
                );
        if (status != HAL_OK)
        {
            LOG_ERROR("FAILED TO RECEIVE THE I2C DATA USING DMA");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        EventBits_t events = xEventGroupWaitBits(
                                s_bsp_i2c_instances[minor].event_group,
                                I2C_RX_COMPLETED | I2C_RX_ERROR,
                                I2C_RX_COMPLETED | I2C_RX_ERROR,
                                false,
                                portMAX_DELAY
                            );

        if (events & I2C_RX_ERROR)
        {
            LOG_ERROR("I2C RX ERROR!");
        }

        if (events & I2C_RX_COMPLETED)
        {
            arg->bytes_consumed = arg->bytes_requested;
#if LOG_VERBOSE == 1
            LOG_TRACE("I2C RX COMPLETED");
#endif
        }
    }

    s_bsp_i2c_instances[0].transaction_type = BSP_I2C_NO_TRANSACTION;

    return retval;
}

static bsp_status_e s_bsp_i2c_ioctl(int major, int minor, bsp_ioctl_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;

    if ((minor < 0) || (minor >= I2C_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID MINOR NUMBER: %ld", minor);
        retval = -1;
    }

    return retval;
}

static void s_bsp_i2c_tx_completed(struct __I2C_HandleTypeDef * hdma)
{
    for (uint32_t i = 0; i < I2C_INSTANCES_CNT; ++i)
    {
        if (hdma == &s_bsp_i2c_instances[i].instance)
        {
            xEventGroupSetBitsFromISR(s_bsp_i2c_instances[0].event_group, I2C_TX_COMPLETED, NULL);
        }
    }
}

static void s_bsp_i2c_rx_completed(struct __I2C_HandleTypeDef * hdma)
{
    for (uint32_t i = 0; i < I2C_INSTANCES_CNT; ++i)
    {
        if (hdma == &s_bsp_i2c_instances[i].instance)
        {
            xEventGroupSetBitsFromISR(s_bsp_i2c_instances[0].event_group, I2C_RX_COMPLETED, NULL);
        }
    }
}

static void s_bsp_i2c_error(struct __I2C_HandleTypeDef * hdma)
{
    for (uint32_t i = 0; i < I2C_INSTANCES_CNT; ++i)
    {
        if (hdma == &s_bsp_i2c_instances[i].instance)
        {
            if (s_bsp_i2c_instances[0].transaction_type == BSP_I2C_WRITE)
            {
                xEventGroupSetBitsFromISR(s_bsp_i2c_instances[0].event_group, I2C_TX_ERROR, NULL);
            }
            else
            if (s_bsp_i2c_instances[0].transaction_type == BSP_I2C_READ)
            {
                xEventGroupSetBitsFromISR(s_bsp_i2c_instances[0].event_group, I2C_RX_ERROR, NULL);
            }
        }
    }
}

void I2C1_EV_IRQHandler(void)
{
    HAL_I2C_EV_IRQHandler(&s_bsp_i2c_instances[0].instance);
}

void I2C1_ER_IRQHandler(void)
{
    HAL_I2C_ER_IRQHandler(&s_bsp_i2c_instances[0].instance);
}
