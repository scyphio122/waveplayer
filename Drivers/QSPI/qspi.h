/*
 * qspi.h
 *
 *  Created on: Jan 17, 2020
 *      Author: konrad
 */

#ifndef DRIVERS_QSPI_QSPI_H_
#define DRIVERS_QSPI_QSPI_H_

#include <bsp_types.h>

#define QSPI_INSTANCES					(1u)

typedef enum
{
    QSPI_MEM_RESET,
    QSPI_MEM_ERASE_BLOCK,
    QSPI_MEM_ERASE_SECTOR,
    QSPI_MEM_ERASE_CHIP
} qspi_ioctl_cmd_e;

int qspi_register(void);

#endif /* DRIVERS_QSPI_QSPI_H_ */
