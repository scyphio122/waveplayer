/*
 * qspi.c
 *
 *  Created on: Jan 17, 2020
 *      Author: konrad
 */

#include <bsp_types.h>
#include <qspi.h>
#include <gpio.h>
#include <stm32l476xx.h>
#include <stm32l4xx_hal_conf.h>
#include <stm32l4xx_hal_dma.h>
#include <stm32l4xx_hal_qspi.h>
#include <stm32l4xx_hal_rcc.h>
#include <log.h>

#include <bsp_peripheral_mapping.h>
#include <bsp_dma_mapping.h>
#include <bsp_types.h>
#include <driver_manager.h>

#include <FreeRTOS.h>
#include <event_groups.h>

#define QSPI_DRIVER_NAME                    "/dev/memblk"
#define N25Q128A_FLASH_SIZE                  0x1000000
#define N25Q128A_FLASH_ADDRESS_BIT_WIDTH     24
#define N25Q128A_FLASH_SIZE                  0x1000000 /* 128 MBits => 16MBytes */
#define N25Q128A_SECTOR_SIZE                 0x10000   /* 256 sectors of 64KBytes */
#define N25Q128A_SUBSECTOR_SIZE              0x1000    /* 4096 subsectors of 4kBytes */
#define N25Q128A_PAGE_SIZE                   0x100     /* 65536 pages of 256 bytes */

#define N25Q128A_DUMMY_CYCLES_READ           8
#define N25Q128A_DUMMY_CYCLES_READ_QUAD      10

#define N25Q128A_DEFAULT_OP_MAX_TIME         10000
#define N25Q128A_BULK_ERASE_MAX_TIME         2500000
#define N25Q128A_SECTOR_ERASE_MAX_TIME       3000
#define N25Q128A_SUBSECTOR_ERASE_MAX_TIME    10000

/**
  * @brief  N25Q128A Commands
  */
/* Reset Operations */
#define RESET_ENABLE_CMD                     0x66
#define RESET_MEMORY_CMD                     0x99

/* Identification Operations */
#define READ_ID_CMD                          0x9E
#define READ_ID_CMD2                         0x9F
#define MULTIPLE_IO_READ_ID_CMD              0xAF
#define READ_SERIAL_FLASH_DISCO_PARAM_CMD    0x5A

/* Read Operations */
#define READ_CMD                             0x03
#define FAST_READ_CMD                        0x0B
#define DUAL_OUT_FAST_READ_CMD               0x3B
#define DUAL_INOUT_FAST_READ_CMD             0xBB
#define QUAD_OUT_FAST_READ_CMD               0x6B
#define QUAD_INOUT_FAST_READ_CMD             0xEB

/* Write Operations */
#define WRITE_ENABLE_CMD                     0x06
#define WRITE_DISABLE_CMD                    0x04

/* Register Operations */
#define READ_STATUS_REG_CMD                  0x05
#define WRITE_STATUS_REG_CMD                 0x01

#define READ_LOCK_REG_CMD                    0xE8
#define WRITE_LOCK_REG_CMD                   0xE5

#define READ_FLAG_STATUS_REG_CMD             0x70
#define CLEAR_FLAG_STATUS_REG_CMD            0x50

#define READ_NONVOL_CFG_REG_CMD              0xB5
#define WRITE_NONVOL_CFG_REG_CMD             0xB1

#define READ_VOL_CFG_REG_CMD                 0x85
#define WRITE_VOL_CFG_REG_CMD                0x81

#define READ_ENHANCED_VOL_CFG_REG_CMD        0x65
#define WRITE_ENHANCED_VOL_CFG_REG_CMD       0x61

/* Program Operations */
#define PAGE_PROG_CMD                        0x02
#define DUAL_IN_FAST_PROG_CMD                0xA2
#define EXT_DUAL_IN_FAST_PROG_CMD            0xD2
#define QUAD_IN_FAST_PROG_CMD                0x32
#define EXT_QUAD_IN_FAST_PROG_CMD            0x12

/* Erase Operations */
#define SUBSECTOR_ERASE_CMD                  0x20
#define SECTOR_ERASE_CMD                     0xD8
#define BULK_ERASE_CMD                       0xC7

#define PROG_ERASE_RESUME_CMD                0x7A
#define PROG_ERASE_SUSPEND_CMD               0x75

/* One-Time Programmable Operations */
#define READ_OTP_ARRAY_CMD                   0x4B
#define PROG_OTP_ARRAY_CMD                   0x42

/**
  * @brief  N25Q128A Registers
  */
/* Status Register */
#define N25Q128A_SR_WIP                      ((uint8_t)0x01)    /*!< Write in progress */
#define N25Q128A_SR_WREN                     ((uint8_t)0x02)    /*!< Write enable latch */
#define N25Q128A_SR_BLOCKPR                  ((uint8_t)0x5C)    /*!< Block protected against program and erase operations */
#define N25Q128A_SR_PRBOTTOM                 ((uint8_t)0x20)    /*!< Protected memory area defined by BLOCKPR starts from top or bottom */
#define N25Q128A_SR_SRWREN                   ((uint8_t)0x80)    /*!< Status register write enable/disable */

/* Nonvolatile Configuration Register */
#define N25Q128A_NVCR_LOCK                   ((uint16_t)0x0001) /*!< Lock nonvolatile configuration register */
#define N25Q128A_NVCR_DUAL                   ((uint16_t)0x0004) /*!< Dual I/O protocol */
#define N25Q128A_NVCR_QUAB                   ((uint16_t)0x0008) /*!< Quad I/O protocol */
#define N25Q128A_NVCR_RH                     ((uint16_t)0x0010) /*!< Reset/hold */
#define N25Q128A_NVCR_ODS                    ((uint16_t)0x01C0) /*!< Output driver strength */
#define N25Q128A_NVCR_XIP                    ((uint16_t)0x0E00) /*!< XIP mode at power-on reset */
#define N25Q128A_NVCR_NB_DUMMY               ((uint16_t)0xF000) /*!< Number of dummy clock cycles */

/* Volatile Configuration Register */
#define N25Q128A_VCR_WRAP                    ((uint8_t)0x03)    /*!< Wrap */
#define N25Q128A_VCR_XIP                     ((uint8_t)0x08)    /*!< XIP */
#define N25Q128A_VCR_NB_DUMMY                ((uint8_t)0xF0)    /*!< Number of dummy clock cycles */

/* Enhanced Volatile Configuration Register */
#define N25Q128A_EVCR_ODS                    ((uint8_t)0x07)    /*!< Output driver strength */
#define N25Q128A_EVCR_VPPA                   ((uint8_t)0x08)    /*!< Vpp accelerator */
#define N25Q128A_EVCR_RH                     ((uint8_t)0x10)    /*!< Reset/hold */
#define N25Q128A_EVCR_DUAL                   ((uint8_t)0x40)    /*!< Dual I/O protocol */
#define N25Q128A_EVCR_QUAD                   ((uint8_t)0x80)    /*!< Quad I/O protocol */

/* Flag Status Register */
#define N25Q128A_FSR_PRERR                   ((uint8_t)0x02)    /*!< Protection error */
#define N25Q128A_FSR_PGSUS                   ((uint8_t)0x04)    /*!< Program operation suspended */
#define N25Q128A_FSR_VPPERR                  ((uint8_t)0x08)    /*!< Invalid voltage during program or erase */
#define N25Q128A_FSR_PGERR                   ((uint8_t)0x10)    /*!< Program error */
#define N25Q128A_FSR_ERERR                   ((uint8_t)0x20)    /*!< Erase error */
#define N25Q128A_FSR_ERSUS                   ((uint8_t)0x40)    /*!< Erase operation suspended */
#define N25Q128A_FSR_READY                   ((uint8_t)0x80)    /*!< Ready or command in progress */

#define QSPI_EVENT_RX_DMA_COMPLETED          0x00000001lu
#define QSPI_EVENT_RX_DMA_ERROR              0x00000002lu

typedef struct
{
    QSPI_HandleTypeDef      handle;
    DMA_HandleTypeDef       dma_rx_handle;
    uint32_t                memory_size;
    bsp_periph_config_t     cs_gpio_config;
    bsp_periph_config_t     clk_gpio_config;
    bsp_periph_config_t     d0_gpio_config;
    bsp_periph_config_t     d1_gpio_config;
    bsp_periph_config_t     d2_gpio_config;
    bsp_periph_config_t     d3_gpio_config;

    EventGroupHandle_t      event_group;
} qspi_instance_t;

void QUADSPI_IRQHandler(void);

void DMA1_Channel5_IRQHandler(void);

static bsp_status_e s_qspi_initialize(void);

static bsp_status_e s_qspi_open(int major, int minor, bsp_open_close_args_t* args);

static bsp_status_e s_qspi_close(int major, int minor, bsp_open_close_args_t* args);

static bsp_status_e s_qspi_write(int major, int minor, bsp_rw_args_t* args);

static bsp_status_e s_qspi_read(int major, int minor, bsp_rw_args_t* args);

static bsp_status_e s_qspi_ioctl(int major, int minor, bsp_ioctl_args_t* args);

static bsp_status_e s_qspi_reset_memory(qspi_instance_t* instance);

static bsp_status_e s_qspi_dummy_clock_perform(qspi_instance_t* instance);

static bsp_status_e s_qspi_autopolling_mem_ready(qspi_instance_t* instance, uint32_t timeout);

static bsp_status_e s_qspi_write_enable(qspi_instance_t* instance);

static bsp_status_e s_qspi_erase_block(qspi_instance_t* instance, uint32_t block_address);

static bsp_status_e s_qspi_erase_sector(qspi_instance_t* instance, uint32_t sector);

static bsp_status_e s_qspi_erase_chip(qspi_instance_t* instance);

static bsp_status_e s_qspi_program_single_page(qspi_instance_t* instance, uint32_t page_address, uint8_t* buffer, uint32_t buffer_size);

static bsp_status_e s_qspi_read_subsector(qspi_instance_t* instance, uint32_t subsector_address, uint8_t* buffer, uint32_t buffer_size);

static void s_qspi_dma_rx_completed(QSPI_HandleTypeDef* hdma);

static void s_qspi_dma_rx_error(QSPI_HandleTypeDef* hdma);

static qspi_instance_t	s_qspi_instances[QSPI_INSTANCES] = {
        {
             .memory_size = N25Q128A_FLASH_SIZE,
             .handle = {
                     .Instance                = QUADSPI,
                     /* QSPI clock = 80MHz */
                     .Init = {
                             .ClockPrescaler     = 0,
                             .FifoThreshold      = 4,
                             .SampleShifting     = QSPI_SAMPLE_SHIFTING_HALFCYCLE,
                             .FlashSize          = N25Q128A_FLASH_ADDRESS_BIT_WIDTH - 1,
                             .ChipSelectHighTime = QSPI_CS_HIGH_TIME_1_CYCLE,
                             .ClockMode          = QSPI_CLOCK_MODE_0,
                     }
             },
             .dma_rx_handle = {
                     .Instance  = QSPI_RX_DMA_CHANNEL,
                     .Init       =   {
                             .Direction  =   DMA_PERIPH_TO_MEMORY,
                             .Request    =   QSPI_RX_DMA_REQUEST,
                             .PeriphInc  =   DMA_PINC_DISABLE,
                             .MemInc                 =   DMA_MINC_ENABLE,
                             .PeriphDataAlignment    =   DMA_PDATAALIGN_BYTE,
                             .MemDataAlignment       =   DMA_PDATAALIGN_BYTE,
                             .Mode                   =   DMA_NORMAL,
                             .Priority               =   DMA_PRIORITY_HIGH
                     },
             },
             .cs_gpio_config = {
                 .port                   =   QSPI_CS_PORT,
                 .pin                    =   QSPI_CS_PIN,
                 .alternative_function   =   QSPI_CS_AF,
                 .fd                     =   -1,
             },
             .clk_gpio_config = {
                 .port                   =   QSPI_CLK_PORT,
                 .pin                    =   QSPI_CLK_PIN,
                 .alternative_function   =   QSPI_CLK_AF,
                 .fd                     =   -1,
             },
             .d0_gpio_config = {
                 .port                   =   QSPI_D0_PORT,
                 .pin                    =   QSPI_D0_PIN,
                 .alternative_function   =   QSPI_D0_AF,
                 .fd                     =   -1,
             },
             .d1_gpio_config = {
                 .port                   =   QSPI_D1_PORT,
                 .pin                    =   QSPI_D1_PIN,
                 .alternative_function   =   QSPI_D1_AF,
                 .fd                     =   -1,
             },
             .d2_gpio_config = {
                 .port                   =   QSPI_D2_PORT,
                 .pin                    =   QSPI_D2_PIN,
                 .alternative_function   =   QSPI_D2_AF,
                 .fd                     =   -1,
             },
             .d3_gpio_config = {
                 .port                   =   QSPI_D3_PORT,
                 .pin                    =   QSPI_D3_PIN,
                 .alternative_function   =   QSPI_D3_AF,
                 .fd                     =   -1,
             }
        }
};

static driver_instance_t    s_qspi_instance[] = {
        {
            .driver_name        =    QSPI_DRIVER_NAME,
            .driver_callbacks   =   {
                .init_cb    =   s_qspi_initialize,
                .open_cb    =   s_qspi_open,
                .close_cb   =   s_qspi_close,
                .write_cb   =   s_qspi_write,
                .read_cb    =   s_qspi_read,
                .ioctl_cb   =   s_qspi_ioctl
            }
        }
};

static uint8_t  s_qspi_update_buffer[N25Q128A_SUBSECTOR_SIZE];

int qspi_register(void)
{
    int retval = 0;

    retval = driver_register(s_qspi_instance, 1);

    return retval;
}

static bsp_status_e s_qspi_initialize(void)
{
	bsp_status_e retval = BSP_SUCCESS;

	HAL_StatusTypeDef status = HAL_OK;

	for (int i=0; i<sizeof(s_qspi_instances)/sizeof(s_qspi_instances[0]); ++i)
	{
        if (retval == 0)
        {
            s_qspi_instances[i].cs_gpio_config.fd = driver_open("/dev/gpioe", NULL);
            if (s_qspi_instances[i].cs_gpio_config.fd < 0)
            {
                retval = -1;
            }

            if (retval == 0)
            {
                bsp_iop_t iop;
                bsp_ioctl_args_t ioctl_args;
                ioctl_args.command = GPIO_SET_MODE_COMMAND;
                ioctl_args.iop = &iop;
                iop.data0 = GPIO_MODE_AF_PP;
                iop.flags = s_qspi_instances[i].cs_gpio_config.pin                     |
                            GPIO_NO_PULL                                               |
                            s_qspi_instances[i].cs_gpio_config.alternative_function    |
                            GPIO_SPEED_CONFIG_VERY_HIGH;

                retval = driver_ioctl(s_qspi_instances[i].cs_gpio_config.fd, &ioctl_args);
            }
        }

        if (retval == 0)
        {
            s_qspi_instances[i].clk_gpio_config.fd = driver_open("/dev/gpioe", NULL);
            if (s_qspi_instances[i].clk_gpio_config.fd < 0)
            {
                retval = -1;
            }

            if (retval == 0)
            {
                bsp_iop_t iop;
                bsp_ioctl_args_t ioctl_args;
                ioctl_args.command = GPIO_SET_MODE_COMMAND;
                ioctl_args.iop = &iop;
                iop.data0 = GPIO_MODE_AF_PP;
                iop.flags = s_qspi_instances[i].clk_gpio_config.pin                     |
                            GPIO_NO_PULL                                               |
                            s_qspi_instances[i].clk_gpio_config.alternative_function    |
                            GPIO_SPEED_CONFIG_VERY_HIGH;

                retval = driver_ioctl(s_qspi_instances[i].clk_gpio_config.fd, &ioctl_args);
            }
        }

        if (retval == 0)
        {
            s_qspi_instances[i].d0_gpio_config.fd = driver_open("/dev/gpioe", NULL);
            if (s_qspi_instances[i].d0_gpio_config.fd < 0)
            {
                retval = -1;
            }

            if (retval == 0)
            {
                bsp_iop_t iop;
                bsp_ioctl_args_t ioctl_args;
                ioctl_args.command = GPIO_SET_MODE_COMMAND;
                ioctl_args.iop = &iop;
                iop.data0 = GPIO_MODE_AF_PP;
                iop.flags = s_qspi_instances[i].d0_gpio_config.pin                     |
                            GPIO_NO_PULL                                              |
                            s_qspi_instances[i].d0_gpio_config.alternative_function    |
                            GPIO_SPEED_CONFIG_VERY_HIGH;

                retval = driver_ioctl(s_qspi_instances[i].d0_gpio_config.fd, &ioctl_args);
            }
        }

        if (retval == 0)
        {
            s_qspi_instances[i].d1_gpio_config.fd = driver_open("/dev/gpioe", NULL);
            if (s_qspi_instances[i].d1_gpio_config.fd < 0)
            {
                retval = -1;
            }

            if (retval == 0)
            {
                bsp_iop_t iop;
                bsp_ioctl_args_t ioctl_args;
                ioctl_args.command = GPIO_SET_MODE_COMMAND;
                ioctl_args.iop = &iop;
                iop.data0 = GPIO_MODE_AF_PP;
                iop.flags = s_qspi_instances[i].d1_gpio_config.pin                     |
                            GPIO_NO_PULL                                              |
                            s_qspi_instances[i].d1_gpio_config.alternative_function    |
                            GPIO_SPEED_CONFIG_VERY_HIGH;

                retval = driver_ioctl(s_qspi_instances[i].d1_gpio_config.fd, &ioctl_args);
            }
        }

        if (retval == 0)
        {
            s_qspi_instances[i].d2_gpio_config.fd = driver_open("/dev/gpioe", NULL);
            if (s_qspi_instances[i].d2_gpio_config.fd < 0)
            {
                retval = -1;
            }

            if (retval == 0)
            {
                bsp_iop_t iop;
                bsp_ioctl_args_t ioctl_args;
                ioctl_args.command = GPIO_SET_MODE_COMMAND;
                ioctl_args.iop = &iop;
                iop.data0 = GPIO_MODE_AF_PP;
                iop.flags = s_qspi_instances[i].d2_gpio_config.pin                     |
                            GPIO_NO_PULL                                              |
                            s_qspi_instances[i].d2_gpio_config.alternative_function    |
                            GPIO_SPEED_CONFIG_VERY_HIGH;

                retval = driver_ioctl(s_qspi_instances[i].d2_gpio_config.fd, &ioctl_args);
            }
        }

        if (retval == 0)
        {
            s_qspi_instances[i].d3_gpio_config.fd = driver_open("/dev/gpioe", NULL);
            if (s_qspi_instances[i].d3_gpio_config.fd < 0)
            {
                retval = -1;
            }

            if (retval == 0)
            {
                bsp_iop_t iop;
                bsp_ioctl_args_t ioctl_args;
                ioctl_args.command = GPIO_SET_MODE_COMMAND;
                ioctl_args.iop = &iop;
                iop.data0 = GPIO_MODE_AF_PP;
                iop.flags = s_qspi_instances[i].d3_gpio_config.pin                     |
                            GPIO_NO_PULL                                              |
                            s_qspi_instances[i].d3_gpio_config.alternative_function    |
                            GPIO_SPEED_CONFIG_VERY_HIGH;

                retval = driver_ioctl(s_qspi_instances[i].d3_gpio_config.fd, &ioctl_args);
            }
        }

        if (retval == 0)
        {
            __HAL_RCC_DMA1_CLK_ENABLE();

            HAL_StatusTypeDef status = HAL_OK;
            status = HAL_DMA_Init(&s_qspi_instances[i].dma_rx_handle);
            if (status != HAL_OK)
            {
                LOG_ERROR("FAILED TO CONFIGURE QSPI RX DMA CHANNEL");
                retval = -1;
            }
            else
            {
                HAL_NVIC_ClearPendingIRQ(QSPI_RX_DMA_IRQn);
                HAL_NVIC_SetPriority(QSPI_RX_DMA_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY, 0);
                HAL_NVIC_EnableIRQ(QSPI_RX_DMA_IRQn);

                HAL_NVIC_ClearPendingIRQ(QUADSPI_IRQn);
                HAL_NVIC_SetPriority(QUADSPI_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY, 0);
                HAL_NVIC_EnableIRQ(QUADSPI_IRQn);
            }
        }

        if (retval == 0)
        {
            __HAL_RCC_QSPI_CLK_ENABLE();

            status = HAL_QSPI_Init(&s_qspi_instances[i].handle);
            if (status != HAL_OK)
            {
                LOG_ERROR("FAILED TO INITIALIZE QSPI INSTANCE ID %d", i);
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == 0)
        {
            s_qspi_instances[i].handle.hdma                        = &s_qspi_instances[i].dma_rx_handle;
            s_qspi_instances[i].dma_rx_handle.Parent               = &s_qspi_instances[i].handle;
            s_qspi_instances[i].handle.RxCpltCallback              = s_qspi_dma_rx_completed;
            s_qspi_instances[i].handle.ErrorCallback               = s_qspi_dma_rx_error;
        }

        if (retval == 0)
        {
            s_qspi_instances[i].event_group = xEventGroupCreate();
            if (s_qspi_instances[i].event_group == NULL)
            {
                LOG_ERROR("FAIELD TO CREATAE QSPI EMMORY EVENT GROUP");
                retval = -1;
            }
        }
	}

#if 0
	if (retval == 0)
	{
	    s_qspi_erase_chip(&s_qspi_instances[0]);
	}
#endif
	return retval;
}

static bsp_status_e s_qspi_open(int major, int minor, bsp_open_close_args_t* args)
{
	bsp_status_e retval = BSP_SUCCESS;

	if ((minor < 0) && (minor >= QSPI_INSTANCES))
	{
		retval = BSP_INVALID_ARGS;
	}

    if (retval == BSP_SUCCESS)
    {
        __HAL_QSPI_ENABLE(&s_qspi_instances[minor].handle);

        retval = s_qspi_reset_memory(&s_qspi_instances[minor]);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO RESET QSPI MEMORY");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        retval = s_qspi_dummy_clock_perform(&s_qspi_instances[minor]);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SEND DUMMY CLOCKS TO INITIALIZE QSPI MEMORY");
        }
    }

	return retval;
}

static bsp_status_e s_qspi_close(int major, int minor, bsp_open_close_args_t* args)
{
	bsp_status_e retval = BSP_SUCCESS;

    if ((minor < 0) && (minor >= QSPI_INSTANCES))
    {
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        __HAL_QSPI_DISABLE(&s_qspi_instances[minor].handle);
    }

	return retval;
}

static bsp_status_e s_qspi_write(int major, int minor, bsp_rw_args_t* args)
{
	bsp_status_e retval = BSP_SUCCESS;

    if ((minor < 0) && (minor >= QSPI_INSTANCES))
    {
        LOG_ERROR("INVALID MINOR VALUE PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (args == NULL)
    {
        LOG_ERROR("NULL ARGS PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        if (args->buffer == NULL)
        {
            LOG_ERROR("NULL BUFFER PTR PROVIDED");
            retval = BSP_INVALID_ARGS;
        }

        if (args->bytes_requested == 0)
        {
            LOG_ERROR("ZERO DATA SIZE REQUESTED");
            retval = BSP_INVALID_ARGS;
        }

        if (args->iop == NULL)
        {
            LOG_ERROR("NULL IOP STRUCTURE PROVIDED");
            retval = BSP_INVALID_ARGS;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        if (args->iop->offset % N25Q128A_PAGE_SIZE != 0)
        {
            LOG_ERROR("INVALID MEMORY OFFSET. IT CAN BE ONLY MUTIPLE OF THE PAGE SIZE");
            retval = BSP_INVALID_ARGS;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        uint32_t address    = args->iop->offset;
        uint32_t size       = args->bytes_requested;
        uint8_t* buffer     = (uint8_t*)args->buffer;

        args->bytes_consumed = 0;

        uint32_t start_offset = address % N25Q128A_SUBSECTOR_SIZE;
        uint32_t start_sector_begin_address = address - start_offset;
        uint32_t end_sector_begin_address   = 0;
        uint32_t subsectors_cnt = 0;

        if (((address + size) % N25Q128A_SUBSECTOR_SIZE) == 0)
        {
            end_sector_begin_address = address + size;
        }
        else
        {
            end_sector_begin_address = (address + size) - ((address + size) % N25Q128A_SUBSECTOR_SIZE) + N25Q128A_SUBSECTOR_SIZE;
        }

        if (end_sector_begin_address == start_sector_begin_address)
        {
            subsectors_cnt = 1;
        }
        else
        {
            subsectors_cnt = (end_sector_begin_address - start_sector_begin_address) / N25Q128A_SUBSECTOR_SIZE;
        }

        /* Initialize the adress variables */

        uint32_t sector_start_addr = start_sector_begin_address;

        /* Handle the sector update */
        while (subsectors_cnt != 0)
        {
            bool needs_erase = false;

            uint32_t part_stored = 0;

            if (retval == BSP_SUCCESS)
            {

                retval = s_qspi_read_subsector(&s_qspi_instances[minor], sector_start_addr, s_qspi_update_buffer, sizeof(s_qspi_update_buffer));
                if (retval != 0)
                {
                    LOG_ERROR("FAILED TO READ SUBSECTOR 0x%08X", address);
                }
                else
                {
                    /* If last subsector left - copy all data left */
                    if (subsectors_cnt == 1)
                    {
                        part_stored = size;
                        for (uint32_t i = start_offset; i < start_offset + part_stored; ++i)
                        {
                            if (s_qspi_update_buffer[i] != 0xFF)
                            {
                                needs_erase = true;
                                break;
                            }
                        }

                        memcpy(s_qspi_update_buffer + start_offset, buffer, part_stored);
                    }
                    else
                    {
                        part_stored = N25Q128A_SUBSECTOR_SIZE - start_offset;
                        for (uint32_t i = start_offset; i < start_offset + part_stored; ++i)
                        {
                            if (s_qspi_update_buffer[i] != 0xFF)
                            {
                                needs_erase = true;
                                break;
                            }
                        }

                        memcpy(s_qspi_update_buffer + start_offset, buffer, part_stored);
                        start_offset = 0;
                    }

                    buffer += part_stored;
                }
            }

            if (retval == BSP_SUCCESS)
            {
                if (needs_erase)
                {
                    retval = s_qspi_erase_block(&s_qspi_instances[minor], sector_start_addr);
                    if (retval != 0)
                    {
                        LOG_ERROR("FAILED TO ERASE SUBSECTOR 0x%08X", sector_start_addr);
                    }
                }
            }

            uint32_t pages_left = N25Q128A_SUBSECTOR_SIZE / N25Q128A_PAGE_SIZE;
            uint8_t* buf        = s_qspi_update_buffer;

            uint32_t page_address = sector_start_addr;
            LOG_INFO("QSPI STORAGE AT ADDRESS: 0x%08X, SIZE: %lu", address, size);

            while (pages_left != 0)
            {
                LOG_TRACE("QSPI PAGE: 0x%08X, SIZE: %lu", page_address, N25Q128A_PAGE_SIZE);

                retval = s_qspi_program_single_page(&s_qspi_instances[minor], page_address, buf, N25Q128A_PAGE_SIZE);
                if (retval == BSP_SUCCESS)
                {
                    /* Update the address and size variables for next page programming */
                    page_address            += N25Q128A_PAGE_SIZE;
                    buf                     += N25Q128A_PAGE_SIZE;
                    pages_left--;
                }
                else
                {
                    LOG_ERROR("FAILED TO PROGRAM SINGLE PAGE: 0x%08X", page_address);
                    break;
                }
            }

            if (retval != BSP_SUCCESS)
            {
                break;
            }

            size                    -= part_stored;
            subsectors_cnt--;
            sector_start_addr += N25Q128A_SUBSECTOR_SIZE;
        }
    }

	return retval;
}

static bsp_status_e s_qspi_read(int major, int minor, bsp_rw_args_t* args)
{
	bsp_status_e retval = BSP_SUCCESS;

    if ((minor < 0) && (minor >= QSPI_INSTANCES))
    {
        LOG_ERROR("INVALID MINOR VALUE PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (args == NULL)
    {
        LOG_ERROR("NULL ARGS PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        args->bytes_consumed = 0;

        if (args->buffer == NULL)
        {
            LOG_ERROR("NULL BUFFER PTR PROVIDED");
            retval = BSP_INVALID_ARGS;
        }

        if (args->bytes_requested == 0)
        {
            LOG_ERROR("ZERO DATA SIZE REQUESTED");
            retval = BSP_INVALID_ARGS;
        }

        if (args->iop == NULL)
        {
            LOG_ERROR("NULL IOP STRUCTURE PROVIDED");
            retval = BSP_INVALID_ARGS;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        QSPI_CommandTypeDef sCommand;

        /* Initialize the read command */
        sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
        sCommand.Instruction       = QUAD_INOUT_FAST_READ_CMD;
        sCommand.AddressMode       = QSPI_ADDRESS_4_LINES;
        sCommand.AddressSize       = QSPI_ADDRESS_24_BITS;
        sCommand.Address           = args->iop->offset;
        sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
        sCommand.DataMode          = QSPI_DATA_4_LINES;
        sCommand.DummyCycles       = N25Q128A_DUMMY_CYCLES_READ_QUAD;
        sCommand.NbData            = args->bytes_requested;
        sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
        sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
        sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

        if (retval == BSP_SUCCESS)
        {
            HAL_StatusTypeDef status = HAL_QSPI_Command(&s_qspi_instances[minor].handle, &sCommand, N25Q128A_DEFAULT_OP_MAX_TIME);
            /* Configure the command */
            if (status != HAL_OK)
            {
                LOG_ERROR("FAILED TO SEND COMMAND TO READ DATA FROM QSPI MEMORY. STATUS: %lu", status);
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            /* Reception of the data */
            if (HAL_QSPI_Receive_DMA(&s_qspi_instances[minor].handle, args->buffer) != HAL_OK)
            {
                LOG_ERROR("FAILED TO READ DATA FROM QSPI MEMORY");
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            EventBits_t event = xEventGroupWaitBits(
                                    s_qspi_instances[minor].event_group,
                                    QSPI_EVENT_RX_DMA_COMPLETED | QSPI_EVENT_RX_DMA_ERROR,
                                    QSPI_EVENT_RX_DMA_COMPLETED | QSPI_EVENT_RX_DMA_ERROR,
                                    false,
                                    portMAX_DELAY
                                );

            if (event & QSPI_EVENT_RX_DMA_ERROR)
            {
                LOG_ERROR("QSPI DMA RX ERROR DETECTED");
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            args->bytes_consumed += args->bytes_requested;
        }
    }

	return retval;
}

static bsp_status_e s_qspi_ioctl(int major, int minor, bsp_ioctl_args_t* args)
{
	bsp_status_e retval = BSP_SUCCESS;

    if ((minor < 0) && (minor >= QSPI_INSTANCES))
    {
        LOG_ERROR("INVALID MINOR VALUE PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (args == NULL)
    {
        LOG_ERROR("NULL ARGS PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

	if (retval == BSP_SUCCESS)
	{
	    int cmd = (int)args->command;
	    qspi_instance_t* instance = &s_qspi_instances[minor];
	    switch (cmd)
	    {
            case QSPI_MEM_RESET:
            {
                retval = s_qspi_reset_memory(instance);
            } break;

            case QSPI_MEM_ERASE_BLOCK:
            {
                if (retval == BSP_SUCCESS)
                {
                    if (args->iop == NULL)
                    {
                        LOG_ERROR("NULL IOP STRUCTURE PROVIDED");
                        retval = BSP_INVALID_ARGS;
                    }
                }

                if (retval == BSP_SUCCESS)
                {
                    retval = s_qspi_erase_block(instance, args->iop->data0);
                }
            } break;

            case QSPI_MEM_ERASE_SECTOR:
            {
                if (retval == BSP_SUCCESS)
                {
                    if (args->iop == NULL)
                    {
                        LOG_ERROR("NULL IOP STRUCTURE PROVIDED");
                        retval = BSP_INVALID_ARGS;
                    }
                }

                if (retval == BSP_SUCCESS)
                {
                    retval = s_qspi_erase_sector(instance, args->iop->data0);
                }
            } break;

            case QSPI_MEM_ERASE_CHIP:
            {
                retval = s_qspi_erase_chip(instance);

            } break;

            case DRIVER_MEMORY_DEVICE_GET_SIZE:
            {
                *(uint32_t*)(args->buffer) = 16 * 1024 * 1024;
            } break;

            default:
            {
                LOG_ERROR("UNRECOGNIZED IOCTL COMMAND: %ld", cmd);
            }
	    }
	}

	return retval;
}

static bsp_status_e s_qspi_reset_memory(qspi_instance_t* instance)
{
  QSPI_CommandTypeDef sCommand;

  bsp_status_e retval = BSP_SUCCESS;

  if (instance == NULL)
  {
      LOG_ERROR("NULL QSPI INSTANCE PROVIDED");
      retval = BSP_INVALID_ARGS;
  }

  if (retval == BSP_SUCCESS)
  {
      /* Initialize the reset enable command */
      sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
      sCommand.Instruction       = RESET_ENABLE_CMD;
      sCommand.AddressMode       = QSPI_ADDRESS_NONE;
      sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
      sCommand.DataMode          = QSPI_DATA_NONE;
      sCommand.DummyCycles       = 0;
      sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
      sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
      sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

      if (retval == BSP_SUCCESS)
      {
          /* Send the command */
          if (HAL_QSPI_Command(&instance->handle, &sCommand, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
          {
              retval = BSP_INTERNAL_ERROR;
          }
      }

      if (retval == BSP_SUCCESS)
      {
          /* Send the reset memory command */
          sCommand.Instruction = RESET_MEMORY_CMD;
          if (HAL_QSPI_Command(&instance->handle, &sCommand, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
          {
              retval = BSP_INTERNAL_ERROR;
          }
      }

      if (retval == BSP_SUCCESS)
      {
          /* Configure automatic polling mode to wait the memory is ready */
          if (s_qspi_autopolling_mem_ready(instance, N25Q128A_DEFAULT_OP_MAX_TIME) != BSP_SUCCESS)
          {
              retval = BSP_INTERNAL_ERROR;
          }
      }
  }


  return retval;
}

static bsp_status_e s_qspi_dummy_clock_perform(qspi_instance_t* instance)
{
    bsp_status_e retval = BSP_SUCCESS;

    QSPI_CommandTypeDef sCommand;
    uint8_t reg;

    if (instance == NULL)
    {
        LOG_ERROR("NULL QSPI INSTANCE PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        /* Initialize the read volatile configuration register command */
        sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
        sCommand.Instruction       = READ_VOL_CFG_REG_CMD;
        sCommand.AddressMode       = QSPI_ADDRESS_NONE;
        sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
        sCommand.DataMode          = QSPI_DATA_1_LINE;
        sCommand.DummyCycles       = 0;
        sCommand.NbData            = 1;
        sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
        sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
        sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

        if (retval == BSP_SUCCESS)
        {
            /* Configure the command */
            if (HAL_QSPI_Command(&instance->handle, &sCommand, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            /* Reception of the data */
            if (HAL_QSPI_Receive(&instance->handle, &reg, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            /* Enable write operations */
            if (s_qspi_write_enable(instance) != BSP_SUCCESS)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            /* Update volatile configuration register (with new dummy cycles) */
            sCommand.Instruction = WRITE_VOL_CFG_REG_CMD;
            MODIFY_REG(reg, N25Q128A_VCR_NB_DUMMY, (N25Q128A_DUMMY_CYCLES_READ_QUAD << POSITION_VAL(N25Q128A_VCR_NB_DUMMY)));

            /* Configure the write volatile configuration register command */
            if (HAL_QSPI_Command(&instance->handle, &sCommand, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            /* Transmission of the data */
            if (HAL_QSPI_Transmit(&instance->handle, &reg, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }
    }

    return retval;
}

static bsp_status_e s_qspi_autopolling_mem_ready(qspi_instance_t* instance, uint32_t timeout)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (instance == NULL)
    {
        LOG_ERROR("NULL QSPI INSTANCE PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        QSPI_CommandTypeDef     sCommand;
        QSPI_AutoPollingTypeDef sConfig;

        /* Configure automatic polling mode to wait for memory ready */
        sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
        sCommand.Instruction       = READ_STATUS_REG_CMD;
        sCommand.AddressMode       = QSPI_ADDRESS_NONE;
        sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
        sCommand.DataMode          = QSPI_DATA_1_LINE;
        sCommand.DummyCycles       = 0;
        sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
        sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
        sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

        sConfig.Match           = 0;
        sConfig.Mask            = N25Q128A_SR_WIP;
        sConfig.MatchMode       = QSPI_MATCH_MODE_AND;
        sConfig.StatusBytesSize = 1;
        sConfig.Interval        = 0x10;
        sConfig.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

        if (HAL_QSPI_AutoPolling(&instance->handle, &sCommand, &sConfig, timeout) != HAL_OK)
        {
            retval = BSP_INTERNAL_ERROR;
        }
    }

    return retval;
}

static bsp_status_e s_qspi_write_enable(qspi_instance_t* instance)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (instance == NULL)
    {
        LOG_ERROR("NULL QSPI INSTANCE PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        QSPI_CommandTypeDef     sCommand;
        QSPI_AutoPollingTypeDef sConfig;

        if (retval == BSP_SUCCESS)
        {
            /* Enable write operations */
            sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
            sCommand.Instruction       = WRITE_ENABLE_CMD;
            sCommand.AddressMode       = QSPI_ADDRESS_NONE;
            sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
            sCommand.DataMode          = QSPI_DATA_NONE;
            sCommand.DummyCycles       = 0;
            sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
            sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
            sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

            if (HAL_QSPI_Command(&instance->handle, &sCommand, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
            {
                LOG_ERROR("FAILED TO SEND WRITE ENABLE COMMAND");
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            /* Configure automatic polling mode to wait for write enabling */
            sConfig.Match           = N25Q128A_SR_WREN;
            sConfig.Mask            = N25Q128A_SR_WREN;
            sConfig.MatchMode       = QSPI_MATCH_MODE_AND;
            sConfig.StatusBytesSize = 1;
            sConfig.Interval        = 0x10;
            sConfig.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

            sCommand.Instruction    = READ_STATUS_REG_CMD;
            sCommand.DataMode       = QSPI_DATA_1_LINE;

            if (HAL_QSPI_AutoPolling(&instance->handle, &sCommand, &sConfig, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
            {
                LOG_ERROR("TIMEOUT DURING POLLING THE WEL BIT IN QSPI MEMORY");
                retval = BSP_INTERNAL_ERROR;
            }
        }
    }

    return retval;
}

static bsp_status_e s_qspi_erase_block(qspi_instance_t* instance, uint32_t block_address)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (instance == NULL)
    {
        LOG_ERROR("NULL QSPI INSTANCE PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        QSPI_CommandTypeDef sCommand;

        /* Initialize the erase command */
        sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
        sCommand.Instruction       = SUBSECTOR_ERASE_CMD;
        sCommand.AddressMode       = QSPI_ADDRESS_1_LINE;
        sCommand.AddressSize       = QSPI_ADDRESS_24_BITS;
        sCommand.Address           = block_address;
        sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
        sCommand.DataMode          = QSPI_DATA_NONE;
        sCommand.DummyCycles       = 0;
        sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
        sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
        sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

        if (retval == BSP_SUCCESS)
        {
            /* Enable write operations */
            if (s_qspi_write_enable(instance) != BSP_SUCCESS)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            /* Send the command */
            if (HAL_QSPI_Command(&instance->handle, &sCommand, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            /* Configure automatic polling mode to wait for end of erase */
            if (s_qspi_autopolling_mem_ready(instance, N25Q128A_SUBSECTOR_ERASE_MAX_TIME) != BSP_SUCCESS)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }
    }

    return retval;
}

/**
  * @brief  Erases the specified sector of the QSPI memory.
  * @param  Sector: Sector address to erase (0 to 255)
  * @retval QSPI memory status
  * @note This function is non blocking meaning that sector erase
  *       operation is started but not completed when the function
  *       returns. Application has to call BSP_QSPI_GetStatus()
  *       to know when the device is available again (i.e. erase operation
  *       completed).
  */
static bsp_status_e s_qspi_erase_sector(qspi_instance_t* instance, uint32_t sector)
{
    QSPI_CommandTypeDef sCommand;

    bsp_status_e retval = BSP_SUCCESS;

    if (instance == NULL)
    {
        LOG_ERROR("NULL QSPI INSTANCE PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (sector >= (uint32_t)(N25Q128A_FLASH_SIZE / N25Q128A_SECTOR_SIZE))
    {
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        /* Initialize the erase command */
        sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
        sCommand.Instruction       = SECTOR_ERASE_CMD;
        sCommand.AddressMode       = QSPI_ADDRESS_1_LINE;
        sCommand.AddressSize       = QSPI_ADDRESS_24_BITS;
        sCommand.Address           = (sector * N25Q128A_SECTOR_SIZE);
        sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
        sCommand.DataMode          = QSPI_DATA_NONE;
        sCommand.DummyCycles       = 0;
        sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
        sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
        sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

        if (retval == BSP_SUCCESS)
        {
            /* Enable write operations */
            if (s_qspi_write_enable(instance) != BSP_SUCCESS)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            /* Send the command */
            if (HAL_QSPI_Command(&instance->handle, &sCommand, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }
    }

    return retval;
}

/**
  * @brief  Erases the entire QSPI memory.
  * @retval QSPI memory status
  */
static bsp_status_e s_qspi_erase_chip(qspi_instance_t* instance)
{
    QSPI_CommandTypeDef sCommand;
    bsp_status_e retval = BSP_SUCCESS;

    if (instance == NULL)
    {
        LOG_ERROR("NULL QSPI INSTANCE PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        /* Initialize the erase command */
        sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
        sCommand.Instruction       = BULK_ERASE_CMD;
        sCommand.AddressMode       = QSPI_ADDRESS_NONE;
        sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
        sCommand.DataMode          = QSPI_DATA_NONE;
        sCommand.DummyCycles       = 0;
        sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
        sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
        sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

        if (retval == BSP_SUCCESS)
        {
            /* Enable write operations */
            if (s_qspi_write_enable(instance) != BSP_SUCCESS)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            /* Send the command */
            if (HAL_QSPI_Command(&instance->handle, &sCommand, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }

        if (retval == BSP_SUCCESS)
        {
            /* Configure automatic polling mode to wait for end of erase */
            if (s_qspi_autopolling_mem_ready(instance, N25Q128A_BULK_ERASE_MAX_TIME) != BSP_SUCCESS)
            {
                retval = BSP_INTERNAL_ERROR;
            }
        }
    }

    return retval;
}

static bsp_status_e s_qspi_program_single_page(qspi_instance_t* instance, uint32_t page_address, uint8_t* buffer, uint32_t buffer_size)
{
    bsp_status_e retval = BSP_SUCCESS;
    QSPI_CommandTypeDef sCommand;

    if (instance == NULL)
    {
        LOG_ERROR("NULL QSPI INSTANCE PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (buffer == NULL)
    {
        LOG_ERROR("INVALID BUFFER PTR PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (buffer_size != N25Q128A_PAGE_SIZE)
    {
        LOG_ERROR("INVALID BUFFER SIZE. REQUIRED %lu BYTES, GOT: %lu", N25Q128A_PAGE_SIZE, buffer_size);
        retval = BSP_INVALID_ARGS;
    }

    if (page_address > (N25Q128A_FLASH_SIZE + N25Q128A_PAGE_SIZE))
    {
        LOG_ERROR("INVALID PAGE ADDRESS: 0x%08X", page_address);
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        /* Initialize the program command */
        sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
        sCommand.Instruction       = EXT_QUAD_IN_FAST_PROG_CMD;
        sCommand.AddressMode       = QSPI_ADDRESS_4_LINES;
        sCommand.AddressSize       = QSPI_ADDRESS_24_BITS;
        sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
        sCommand.DataMode          = QSPI_DATA_4_LINES;
        sCommand.DummyCycles       = 0;
        sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
        sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
        sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

        sCommand.Address            = page_address;
        sCommand.NbData             = N25Q128A_PAGE_SIZE;
    }

    if (retval == BSP_SUCCESS)
    {
        /* Enable write operations */
        if (s_qspi_write_enable(instance) != BSP_SUCCESS)
        {
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        /* Configure the command */
        if (HAL_QSPI_Command(&instance->handle, &sCommand, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
        {
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        /* Transmission of the data */
        if (HAL_QSPI_Transmit(&instance->handle, buffer, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
        {
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        /* Configure automatic polling mode to wait for end of program */
        if (s_qspi_autopolling_mem_ready(instance, N25Q128A_DEFAULT_OP_MAX_TIME) != BSP_SUCCESS)
        {
            retval = BSP_INTERNAL_ERROR;
        }
    }

    return retval;
}

static bsp_status_e s_qspi_read_subsector(qspi_instance_t* instance, uint32_t subsector_address, uint8_t* buffer, uint32_t buffer_size)
{
    bsp_status_e retval = BSP_SUCCESS;
    QSPI_CommandTypeDef sCommand;

    if (instance == NULL)
    {
        LOG_ERROR("NULL QSPI INSTANCE PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (buffer == NULL)
    {
        LOG_ERROR("INVALID BUFFER PTR PROVIDED");
        retval = BSP_INVALID_ARGS;
    }

    if (buffer_size != N25Q128A_SUBSECTOR_SIZE)
    {
        LOG_ERROR("INVALID BUFFER SIZE. REQUIRED %lu BYTES, GOT: %lu", N25Q128A_SUBSECTOR_SIZE, buffer_size);
        retval = BSP_INVALID_ARGS;
    }

    if (subsector_address % N25Q128A_SUBSECTOR_SIZE)
    {
        LOG_ERROR("INVALID PAGE ADDRESS: 0x%08X", subsector_address);
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        /* Initialize the read command */
        sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
        sCommand.Instruction       = QUAD_INOUT_FAST_READ_CMD;
        sCommand.AddressMode       = QSPI_ADDRESS_4_LINES;
        sCommand.AddressSize       = QSPI_ADDRESS_24_BITS;
        sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
        sCommand.DataMode          = QSPI_DATA_4_LINES;
        sCommand.DummyCycles       = N25Q128A_DUMMY_CYCLES_READ_QUAD;
        sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
        sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
        sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

        sCommand.Address           = subsector_address;
        sCommand.NbData            = N25Q128A_SUBSECTOR_SIZE;

        /* Configure the command */
        if (HAL_QSPI_Command(&instance->handle, &sCommand, N25Q128A_DEFAULT_OP_MAX_TIME) != HAL_OK)
        {
            LOG_ERROR("FAILED TO SEND COMMAND TO READ DATA FROM QSPI MEMORY");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        /* Reception of the data */
        if (HAL_QSPI_Receive_DMA(&instance->handle, buffer) != HAL_OK)
        {
            LOG_ERROR("FAILED TO READ DATA FROM QSPI MEMORY");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        EventBits_t event = xEventGroupWaitBits(
                                instance->event_group,
                                QSPI_EVENT_RX_DMA_COMPLETED | QSPI_EVENT_RX_DMA_ERROR,
                                QSPI_EVENT_RX_DMA_COMPLETED | QSPI_EVENT_RX_DMA_ERROR,
                                false,
                                portMAX_DELAY
                            );

        if (event & QSPI_EVENT_RX_DMA_ERROR)
        {
            LOG_ERROR("QSPI DMA RX ERROR DETECTED");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    return retval;
}

static void s_qspi_dma_rx_completed(QSPI_HandleTypeDef* hdma)
{
    xEventGroupSetBitsFromISR(s_qspi_instances[0].event_group, QSPI_EVENT_RX_DMA_COMPLETED, NULL);
}

static void s_qspi_dma_rx_error(QSPI_HandleTypeDef* hdma)
{
    xEventGroupSetBitsFromISR(s_qspi_instances[0].event_group, QSPI_EVENT_RX_DMA_ERROR, NULL);
}

void QUADSPI_IRQHandler(void)
{
    HAL_QSPI_IRQHandler(&s_qspi_instances[0].handle);
}

void DMA1_Channel5_IRQHandler(void)
{
    HAL_DMA_IRQHandler(&s_qspi_instances[0].dma_rx_handle);
}
