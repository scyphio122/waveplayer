/*
 * gpio.c
 *
 *  Created on: Jan 19, 2020
 *      Author: konrad
 */

#include <bsp_types.h>
#include <gpio.h>
#include <stdint-gcc.h>
#include <stm32l4xx.h>
#include <stm32l4xx_hal_gpio.h>
#include <stm32l4xx_hal_exti.h>
#include <stm32l4xx_ll_exti.h>
#include <stm32l4xx_hal_rcc.h>
#include <stm32l4xx_hal_rcc_ex.h>
#include <stm32l4xx_hal_cortex.h>

#include <driver_manager.h>

#include <log.h>

#include <FreeRTOS.h>
#include <FreeRTOSConfig.h>
#include <queue.h>

#define GPIO_PORTS_CNT          8u
#define GPIO_PINS_IN_PORT_CNT   16u
#define GPIO_FUNC_CNT           4
#define GPIO_IRQ_BLOCK_TIME_MS  20

typedef void (*gpio_irq_f)(void);

typedef struct
{
    uint32_t        registered_func_cnt;
    gpio_irq_f      registered_function_cb[GPIO_FUNC_CNT];
} gpio_irq_function_struct_t;

typedef struct
{
    uint32_t irqn;
    uint32_t end_time;
} gpio_exti_reenable_t;

void EXTI0_IRQHandler(void);

void EXTI1_IRQHandler(void);

void EXTI2_IRQHandler(void);

void EXTI3_IRQHandler(void);

void EXTI4_IRQHandler(void);

void EXTI9_5_IRQHandler(void);

void EXTI15_10_IRQHandler(void);

static bsp_status_e s_gpio_initialize(void);

static bsp_status_e s_gpio_open(int major, int minor, bsp_open_close_args_t* args);

static bsp_status_e s_gpio_close(int major, int minor, bsp_open_close_args_t* args);

static bsp_status_e s_gpio_write(int major, int minor, bsp_rw_args_t* args);

static bsp_status_e s_gpio_read(int major, int minor, bsp_rw_args_t* args);

static bsp_status_e s_gpio_ioctl(int major, int minor, bsp_ioctl_args_t* args);

static bsp_status_e s_gpio_enable_clock(GPIO_TypeDef* instance);

static bsp_status_e s_gpio_disable_clock(GPIO_TypeDef* instance);

static void s_gpio_irq_reenabled_task_worker(void* args);

static GPIO_TypeDef* s_gpio_peripherals[GPIO_PORTS_CNT] = {
        GPIOA,
        GPIOB,
        GPIOC,
        GPIOD,
        GPIOE,
        GPIOF,
        GPIOG,
        GPIOH
};

static driver_instance_t s_gpio_instance[] = {
        {
            .driver_name        =    "/dev/gpioa",
            .driver_callbacks   =   {
                .init_cb    =   s_gpio_initialize,
                .open_cb    =   s_gpio_open,
                .close_cb   =   s_gpio_close,
                .write_cb   =   s_gpio_write,
                .read_cb    =   s_gpio_read,
                .ioctl_cb   =   s_gpio_ioctl
            },
        },
        {
            .driver_name        =    "/dev/gpiob",
            .driver_callbacks   =   {
                .init_cb    =   s_gpio_initialize,
                .open_cb    =   s_gpio_open,
                .close_cb   =   s_gpio_close,
                .write_cb   =   s_gpio_write,
                .read_cb    =   s_gpio_read,
                .ioctl_cb   =   s_gpio_ioctl
            }
        },
        {
            .driver_name        =    "/dev/gpioc",
            .driver_callbacks   =   {
                .init_cb    =   s_gpio_initialize,
                .open_cb    =   s_gpio_open,
                .close_cb   =   s_gpio_close,
                .write_cb   =   s_gpio_write,
                .read_cb    =   s_gpio_read,
                .ioctl_cb   =   s_gpio_ioctl
            }
        },
        {
            .driver_name        =    "/dev/gpiod",
            .driver_callbacks   =   {
                .init_cb    =   s_gpio_initialize,
                .open_cb    =   s_gpio_open,
                .close_cb   =   s_gpio_close,
                .write_cb   =   s_gpio_write,
                .read_cb    =   s_gpio_read,
                .ioctl_cb   =   s_gpio_ioctl
            }
        },
        {
            .driver_name        =    "/dev/gpioe",
            .driver_callbacks   =   {
                .init_cb    =   s_gpio_initialize,
                .open_cb    =   s_gpio_open,
                .close_cb   =   s_gpio_close,
                .write_cb   =   s_gpio_write,
                .read_cb    =   s_gpio_read,
                .ioctl_cb   =   s_gpio_ioctl
            }
        },
        {
            .driver_name        =    "/dev/gpiof",
            .driver_callbacks   =   {
                .init_cb    =   s_gpio_initialize,
                .open_cb    =   s_gpio_open,
                .close_cb   =   s_gpio_close,
                .write_cb   =   s_gpio_write,
                .read_cb    =   s_gpio_read,
                .ioctl_cb   =   s_gpio_ioctl
            }
        },
        {
            .driver_name        =    "/dev/gpiog",
            .driver_callbacks   =   {
                .init_cb    =   s_gpio_initialize,
                .open_cb    =   s_gpio_open,
                .close_cb   =   s_gpio_close,
                .write_cb   =   s_gpio_write,
                .read_cb    =   s_gpio_read,
                .ioctl_cb   =   s_gpio_ioctl
            }
        },
        {
            .driver_name        =    "/dev/gpioh",
            .driver_callbacks   =   {
                .init_cb    =   s_gpio_initialize,
                .open_cb    =   s_gpio_open,
                .close_cb   =   s_gpio_close,
                .write_cb   =   s_gpio_write,
                .read_cb    =   s_gpio_read,
                .ioctl_cb   =   s_gpio_ioctl
            }
        },
};

static gpio_irq_function_struct_t   s_gpio_irq_functions[GPIO_PINS_IN_PORT_CNT];

static QueueHandle_t                s_gpio_irq_reenabler_queue;

static TaskHandle_t                 s_gpio_irq_reenabler_task;

int gpio_register(void)
{
    int retval = 0;

    retval = driver_register(
                s_gpio_instance,
                sizeof(s_gpio_instance)/sizeof(s_gpio_instance[0])
            );

    return retval;
}

static bsp_status_e s_gpio_initialize(void)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (retval == BSP_SUCCESS)
    {
        s_gpio_irq_reenabler_queue = xQueueCreate(8, sizeof(gpio_exti_reenable_t));
        if (s_gpio_irq_reenabler_queue == NULL)
        {
            LOG_ERROR("FAILED TO CREATE EXTI REENABLE QUEUE");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        if (xTaskCreate(s_gpio_irq_reenabled_task_worker, "GPIO_IRQ_REEN", configMINIMAL_STACK_SIZE, NULL, 0, &s_gpio_irq_reenabler_task) != pdTRUE)
        {
            LOG_ERROR("FAILED TO CREATE GPIO IRQ REENABLER TASK");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        HAL_NVIC_ClearPendingIRQ(EXTI0_IRQn);
        HAL_NVIC_ClearPendingIRQ(EXTI1_IRQn);
        HAL_NVIC_ClearPendingIRQ(EXTI2_IRQn);
        HAL_NVIC_ClearPendingIRQ(EXTI3_IRQn);
        HAL_NVIC_ClearPendingIRQ(EXTI4_IRQn);
        HAL_NVIC_ClearPendingIRQ(EXTI9_5_IRQn);
        HAL_NVIC_ClearPendingIRQ(EXTI15_10_IRQn);

        HAL_NVIC_SetPriority(EXTI0_IRQn, configLIBRARY_LOWEST_INTERRUPT_PRIORITY, 0);
        HAL_NVIC_SetPriority(EXTI1_IRQn, configLIBRARY_LOWEST_INTERRUPT_PRIORITY, 0);
        HAL_NVIC_SetPriority(EXTI2_IRQn, configLIBRARY_LOWEST_INTERRUPT_PRIORITY, 0);
        HAL_NVIC_SetPriority(EXTI3_IRQn, configLIBRARY_LOWEST_INTERRUPT_PRIORITY, 0);
        HAL_NVIC_SetPriority(EXTI4_IRQn, configLIBRARY_LOWEST_INTERRUPT_PRIORITY, 0);
        HAL_NVIC_SetPriority(EXTI9_5_IRQn, configLIBRARY_LOWEST_INTERRUPT_PRIORITY, 0);
        HAL_NVIC_SetPriority(EXTI15_10_IRQn, configLIBRARY_LOWEST_INTERRUPT_PRIORITY, 0);

        HAL_NVIC_EnableIRQ(EXTI0_IRQn);
        HAL_NVIC_EnableIRQ(EXTI1_IRQn);
        HAL_NVIC_EnableIRQ(EXTI2_IRQn);
        HAL_NVIC_EnableIRQ(EXTI3_IRQn);
        HAL_NVIC_EnableIRQ(EXTI4_IRQn);
        HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
        HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
    }

    return retval;
}

static bsp_status_e s_gpio_open(int major, int minor, bsp_open_close_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (retval == BSP_SUCCESS)
    {
        if (minor > GPIO_PORTS_CNT)
        {
            retval = BSP_INVALID_ARGS;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        retval = s_gpio_enable_clock(s_gpio_peripherals[minor]);
    }

    return retval;
}

static bsp_status_e s_gpio_close(int major, int minor, bsp_open_close_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (args == NULL)
    {
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        if (minor > GPIO_PORTS_CNT)
        {
            retval = BSP_INVALID_ARGS;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        retval = s_gpio_disable_clock(s_gpio_peripherals[minor]);
    }

    return retval;
}

static bsp_status_e s_gpio_write(int major, int minor, bsp_rw_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (args == NULL)
    {
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        if (minor > GPIO_PORTS_CNT)
        {
            retval = BSP_INVALID_ARGS;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        if (args->buffer == NULL)
        {
            retval = BSP_INVALID_ARGS;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        uint16_t pin    =   args->flags & 0xFFFF;

        if (pin > 16)
        {
            retval = -1;
        }
        else
        {
            uint8_t val     =   ((uint8_t*)args->buffer)[0];
            HAL_GPIO_WritePin(s_gpio_peripherals[minor], (1 << pin), val);
        }
    }

    return retval;
}

static bsp_status_e s_gpio_read(int major, int minor, bsp_rw_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (args == NULL)
    {
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        if (minor > GPIO_PORTS_CNT)
        {
            retval = BSP_INVALID_ARGS;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        if (args->buffer == NULL)
        {
            retval = BSP_INVALID_ARGS;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        uint32_t pin    =   args->flags & 0xFFFF;

        if (pin > 16)
        {
            retval = -1;
        }
        else
        {
            GPIO_PinState state = HAL_GPIO_ReadPin(s_gpio_peripherals[minor], (1 << pin));
            ((uint8_t*)args->buffer)[0] = (uint8_t)state;
        }
    }

    return retval;
}


static bsp_status_e s_gpio_ioctl(int major, int minor, bsp_ioctl_args_t* args)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (args == NULL)
    {
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        switch (args->command)
        {
            case GPIO_SET_MODE_COMMAND:
            {
                if (args->iop != NULL)
                {
                    GPIO_InitTypeDef config;
                    config.Pin          =   (1 << ((args->iop->flags >> 0 ) & 0xFF));
                    config.Mode         =   (args->iop->data0);
                    config.Pull         =   (args->iop->flags >> 16) & 0x03;
                    config.Speed        =   (args->iop->flags >> 18) & 0x03;
                    config.Alternate    =   (args->iop->flags >> 24);

                    if (IS_GPIO_PIN(config.Pin) != true)
                    {
                        retval = -1;
                    }

                    if (IS_GPIO_MODE(config.Mode) != true)
                    {
                        retval = -1;
                    }

                    if (IS_GPIO_PULL(config.Pull) != true)
                    {
                        retval = -1;
                    }

                    if (IS_GPIO_SPEED(config.Speed) != true)
                    {
                        retval = -1;
                    }

                    if (IS_GPIO_AF(config.Alternate) != true)
                    {
                        retval = -1;
                    }

                    if (retval == 0)
                    {
                        HAL_GPIO_Init(s_gpio_peripherals[minor], &config);
                    }
                }
                else
                {
                    retval = BSP_INVALID_ARGS;
                }
            } break;

            case GPIO_EXTI_CONFIG_CMD:
            {
                EXTI_HandleTypeDef exti;
                exti.Line = args->iop->data0;
                exti.PendingCallback = NULL;

                EXTI_ConfigTypeDef exti_config;
                exti_config.GPIOSel =   (uint32_t)args->iop->data1;
                exti_config.Mode    =   args->iop->flags & 0x00FF;
                exti_config.Trigger =   (args->iop->flags >> 8) & 0x00FF;
                exti_config.Line    =   args->iop->data0;

                if (HAL_EXTI_SetConfigLine(&exti, &exti_config) != HAL_OK)
                {
                    LOG_ERROR("FAILED TO CONFIGURE EXTI");
                    retval = BSP_INTERNAL_ERROR;
                }

            } break;

            case GPIO_REGISTER_IRQ_FUNC_CB:
            {
                __disable_irq();

                gpio_irq_f  function    =   (gpio_irq_f)args->iop->data1;
                uint16_t    pin         =   (uint16_t)args->iop->data0;

                if (function == NULL)
                {
                    LOG_ERROR("CANNOT REGISTER NULL FUNCTION PTR");
                    retval = BSP_INVALID_ARGS;
                }

                if (pin >= GPIO_PINS_IN_PORT_CNT)
                {
                    LOG_ERROR("INVALID PIN INDEX");
                    retval = BSP_INVALID_ARGS;
                }

                if (retval == BSP_SUCCESS)
                {
                    if (s_gpio_irq_functions[pin].registered_func_cnt < GPIO_FUNC_CNT)
                    {
                        for (uint32_t i=0; i<GPIO_FUNC_CNT; ++i)
                        {
                            if (s_gpio_irq_functions[pin].registered_function_cb[i] == function)
                            {
                                LOG_ERROR("CALLBACK ALREADY REGISTERED");
                                retval = BSP_INTERNAL_ERROR;
                                break;
                            }
                            else
                            if (s_gpio_irq_functions[pin].registered_function_cb[i] == NULL)
                            {
                                s_gpio_irq_functions[pin].registered_function_cb[i] = function;
                                s_gpio_irq_functions[pin].registered_func_cnt++;
                                break;
                            }
                        }

                    }
                    else
                    {
                        LOG_ERROR("CANNOT REGISTER MORE CALLBACKS TO THE FUNCTION");
                        retval = BSP_INVALID_STATE;
                    }
                }

                __enable_irq();
            } break;

            case GPIO_UNREGISTER_IRQ_FUNC_CB:
            {
                __disable_irq();

                gpio_irq_f  function    =   (gpio_irq_f)args->iop->data1;
                uint16_t    pin         =   (uint16_t)args->iop->data0;

                if (pin >= GPIO_PINS_IN_PORT_CNT)
                {
                    LOG_ERROR("INVALID PIN INDEX");
                    retval = BSP_INVALID_ARGS;
                }

                if (function == NULL)
                {
                    LOG_ERROR("CANNOT UNREGISTER NULL FUNCTION PTR");
                    retval = BSP_INVALID_ARGS;
                }

                if (retval == BSP_SUCCESS)
                {
                    if (s_gpio_irq_functions[pin].registered_func_cnt > 0)
                    {
                        bool found = false;
                        for (uint32_t i=0; i<GPIO_FUNC_CNT; ++i)
                        {
                            if (s_gpio_irq_functions[pin].registered_function_cb[i] == function)
                            {
                                found = true;
                                s_gpio_irq_functions[pin].registered_function_cb[i] = NULL;
                                s_gpio_irq_functions[pin].registered_func_cnt--;
                            }
                        }

                        if (found == false)
                        {
                            LOG_ERROR("FAILED TO UNREGISTER THE GPIO CALLBACK - IT WAS NEVER REGISTERED");
                            retval = BSP_INVALID_ARGS;
                        }
                    }
                    else
                    {
                        LOG_ERROR("NO GPIO IRQ CALLBACKS IS REGISTERED");
                        retval = BSP_INVALID_STATE;
                    }
                }

                __enable_irq();

            } break;

            default:
            {
                retval = BSP_NOT_IMPLEMENTED;
            }
        }
    }

    return retval;
}

static bsp_status_e s_gpio_enable_clock(GPIO_TypeDef* instance)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (instance == NULL)
    {
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        switch ((uint32_t)instance)
        {
            case (uint32_t)GPIOA:
            {
                __HAL_RCC_GPIOA_CLK_ENABLE();
            } break;

            case (uint32_t)GPIOB:
            {
                __HAL_RCC_GPIOB_CLK_ENABLE();
            } break;

            case (uint32_t)GPIOC:
            {
                __HAL_RCC_GPIOC_CLK_ENABLE();
            } break;

            case (uint32_t)GPIOD:
            {
                __HAL_RCC_GPIOD_CLK_ENABLE();
            } break;

            case (uint32_t)GPIOE:
            {
                __HAL_RCC_GPIOE_CLK_ENABLE();
            } break;

            case (uint32_t)GPIOF:
            {
                __HAL_RCC_GPIOF_CLK_ENABLE();
            } break;

            case (uint32_t)GPIOG:
            {
                __HAL_RCC_GPIOG_CLK_ENABLE();
            } break;

            case (uint32_t)GPIOH:
            {
                __HAL_RCC_GPIOH_CLK_ENABLE();
            } break;

            default:
            {
                retval = BSP_NOT_IMPLEMENTED;
            }
        }
    }

    return retval;
}

static bsp_status_e s_gpio_disable_clock(GPIO_TypeDef* instance)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (instance == NULL)
    {
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        switch ((uint32_t)instance)
        {
            case (uint32_t)GPIOA:
            {
                __HAL_RCC_GPIOA_CLK_DISABLE();
            } break;

            case (uint32_t)GPIOB:
            {
                __HAL_RCC_GPIOB_CLK_DISABLE();
            } break;

            case (uint32_t)GPIOC:
            {
                __HAL_RCC_GPIOC_CLK_DISABLE();
            } break;

            case (uint32_t)GPIOD:
            {
                __HAL_RCC_GPIOD_CLK_DISABLE();
            } break;

            case (uint32_t)GPIOE:
            {
                __HAL_RCC_GPIOE_CLK_DISABLE();
            } break;

            case (uint32_t)GPIOF:
            {
                __HAL_RCC_GPIOF_CLK_DISABLE();
            } break;

            case (uint32_t)GPIOG:
            {
                __HAL_RCC_GPIOG_CLK_DISABLE();
            } break;

            case (uint32_t)GPIOH:
            {
                __HAL_RCC_GPIOH_CLK_DISABLE();
            } break;

            default:
            {
                retval = BSP_NOT_IMPLEMENTED;
            }
        }
    }

    return retval;
}

void EXTI0_IRQHandler(void)
{
    HAL_NVIC_DisableIRQ(EXTI0_IRQn);
    __HAL_GPIO_EXTI_CLEAR_IT(EXTI0_IRQn);

    BaseType_t yield = 0;

    gpio_exti_reenable_t exti_reen;
    exti_reen.end_time = xTaskGetTickCountFromISR() + GPIO_IRQ_BLOCK_TIME_MS;
    exti_reen.irqn = EXTI0_IRQn;

    xQueueSendFromISR(s_gpio_irq_reenabler_queue, &exti_reen, &yield);

    uint32_t status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_0);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_0);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_0].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_0].registered_function_cb[i]();
            }
        }
    }

    portYIELD_FROM_ISR(yield);
}

void EXTI1_IRQHandler(void)
{
    HAL_NVIC_DisableIRQ(EXTI1_IRQn);
    __HAL_GPIO_EXTI_CLEAR_IT(EXTI1_IRQn);

    BaseType_t yield = 0;

    gpio_exti_reenable_t exti_reen;
    exti_reen.end_time = xTaskGetTickCountFromISR() + GPIO_IRQ_BLOCK_TIME_MS;
    exti_reen.irqn = EXTI1_IRQn;

    xQueueSendFromISR(s_gpio_irq_reenabler_queue, &exti_reen, &yield);

    uint32_t status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_1);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_1);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_1].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_1].registered_function_cb[i]();
            }
        }
    }

    portYIELD_FROM_ISR(yield);
}

void EXTI2_IRQHandler(void)
{
    HAL_NVIC_DisableIRQ(EXTI2_IRQn);
    __HAL_GPIO_EXTI_CLEAR_IT(EXTI2_IRQn);

    BaseType_t yield = 0;

    gpio_exti_reenable_t exti_reen;
    exti_reen.end_time = xTaskGetTickCountFromISR() + GPIO_IRQ_BLOCK_TIME_MS;
    exti_reen.irqn = EXTI2_IRQn;

    xQueueSendFromISR(s_gpio_irq_reenabler_queue, &exti_reen, &yield);

    uint32_t status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_2);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_2);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_2].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_2].registered_function_cb[i]();
            }
        }
    }

    portYIELD_FROM_ISR(yield);
}

void EXTI3_IRQHandler(void)
{
    HAL_NVIC_DisableIRQ(EXTI3_IRQn);
    __HAL_GPIO_EXTI_CLEAR_IT(EXTI3_IRQn);

    BaseType_t yield = 0;

    gpio_exti_reenable_t exti_reen;
    uint32_t current_time = xTaskGetTickCountFromISR();
    exti_reen.end_time = current_time + GPIO_IRQ_BLOCK_TIME_MS;
    exti_reen.irqn = EXTI3_IRQn;

    xQueueSendFromISR(s_gpio_irq_reenabler_queue, &exti_reen, &yield);

    uint32_t status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_3);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_3);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_3].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_3].registered_function_cb[i]();
            }
        }
    }

    portYIELD_FROM_ISR(yield);
}

void EXTI4_IRQHandler(void)
{
    HAL_NVIC_DisableIRQ(EXTI4_IRQn);
    __HAL_GPIO_EXTI_CLEAR_IT(EXTI4_IRQn);

    BaseType_t yield = 0;

    gpio_exti_reenable_t exti_reen;
    exti_reen.end_time = xTaskGetTickCountFromISR() + GPIO_IRQ_BLOCK_TIME_MS;
    exti_reen.irqn = EXTI4_IRQn;

    xQueueSendFromISR(s_gpio_irq_reenabler_queue, &exti_reen, &yield);

    uint32_t status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_4);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_4);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_4].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_4].registered_function_cb[i]();
            }
        }
    }

    portYIELD_FROM_ISR(yield);
}

void EXTI9_5_IRQHandler(void)
{
    HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
    __HAL_GPIO_EXTI_CLEAR_IT(EXTI9_5_IRQn);

    uint32_t status = 0;
    BaseType_t yield = 0;

    gpio_exti_reenable_t exti_reen;
    exti_reen.end_time = xTaskGetTickCountFromISR() + GPIO_IRQ_BLOCK_TIME_MS;
    exti_reen.irqn = EXTI9_5_IRQn;

    xQueueSendFromISR(s_gpio_irq_reenabler_queue, &exti_reen, &yield);

    status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_5);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_5);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_5].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_5].registered_function_cb[i]();
            }
        }
    }

    status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_6);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_6);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_6].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_6].registered_function_cb[i]();
            }
        }
    }

    status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_7);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_7);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_7].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_7].registered_function_cb[i]();
            }
        }
    }

    status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_8);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_8);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_8].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_8].registered_function_cb[i]();
            }
        }
    }

    status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_9);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_9);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_9].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_9].registered_function_cb[i]();
            }
        }
    }

    portYIELD_FROM_ISR(yield);
}

void EXTI15_10_IRQHandler(void)
{
    HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);
    __HAL_GPIO_EXTI_CLEAR_IT(EXTI15_10_IRQn);

    BaseType_t yield = 0;

    gpio_exti_reenable_t exti_reen;
    exti_reen.end_time = xTaskGetTickCountFromISR() + GPIO_IRQ_BLOCK_TIME_MS;
    exti_reen.irqn = EXTI15_10_IRQn;

    xQueueSendFromISR(s_gpio_irq_reenabler_queue, &exti_reen, &yield);

    uint32_t status = 0;
    status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_10);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_10);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_10].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_10].registered_function_cb[i]();
            }
        }
    }

    status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_11);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_11);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_11].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_11].registered_function_cb[i]();
            }
        }
    }

    status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_12);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_12);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_12].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_12].registered_function_cb[i]();
            }
        }
    }

    status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_13);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_13);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_13].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_13].registered_function_cb[i]();
            }
        }
    }

    status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_14);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_14);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_14].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_14].registered_function_cb[i]();
            }
        }
    }

    status = LL_EXTI_ReadFlag_0_31(LL_EXTI_LINE_15);
    if (status != 0)
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_15);

        for (uint32_t i = 0; i < GPIO_FUNC_CNT; ++i)
        {
            if (s_gpio_irq_functions[GPIO_IO_15].registered_function_cb[i] != NULL)
            {
                s_gpio_irq_functions[GPIO_IO_15].registered_function_cb[i]();
            }
        }
    }

    portYIELD_FROM_ISR(yield);
}

static void s_gpio_irq_reenabled_task_worker(void* args)
{
    gpio_exti_reenable_t exti;

    while (1)
    {
        xQueueReceive(s_gpio_irq_reenabler_queue, &exti, portMAX_DELAY);

        while (xTaskGetTickCount() < exti.end_time)
        {
            vTaskDelay(pdMS_TO_TICKS(10));
        }

        HAL_NVIC_ClearPendingIRQ(exti.irqn);
        HAL_NVIC_EnableIRQ(exti.irqn);
    }
}
