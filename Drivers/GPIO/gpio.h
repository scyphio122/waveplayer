/*
 * gpio.h
 *
 *  Created on: Jan 19, 2020
 *      Author: konrad
 */

#ifndef DRIVERS_GPIO_GPIO_H_
#define DRIVERS_GPIO_GPIO_H_

#include <bsp_types.h>
#include <stm32l4xx_hal_gpio.h>
#include <stm32l4xx_hal_exti.h>

#define GPIO_SET_MODE_COMMAND           (0lu)
#define GPIO_EXTI_CONFIG_CMD            (1lu)
#define GPIO_REGISTER_IRQ_FUNC_CB       (2lu)
#define GPIO_UNREGISTER_IRQ_FUNC_CB     (3lu)


#define GPIO_BUILD_CONFIGURATION(iop, pin, mode, pull, speed, af)    do{                                        \
                                                                            (iop)->flags =    pin;                \
                                                                            (iop)->data0  =   mode;               \
                                                                            (iop)->flags |=   pull;               \
                                                                            (iop)->flags |=   speed;              \
                                                                            (iop)->flags |=   af;                 \
                                                                       } while (0);

typedef enum
{
    GPIO_IO_0,
    GPIO_IO_1,
    GPIO_IO_2,
    GPIO_IO_3,
    GPIO_IO_4,
    GPIO_IO_5,
    GPIO_IO_6,
    GPIO_IO_7,
    GPIO_IO_8,
    GPIO_IO_9,
    GPIO_IO_10,
    GPIO_IO_11,
    GPIO_IO_12,
    GPIO_IO_13,
    GPIO_IO_14,
    GPIO_IO_15,
} gpio_pin_id_e;

typedef enum
{
    GPIO_NO_PULL        =   (GPIO_NOPULL    << 16),
    GPIO_PULL_UP        =   (GPIO_PULLUP    << 16),
    GPIO_PULL_DOWN      =   (GPIO_PULLDOWN  << 16)
} gpio_pull_config_e;

typedef enum
{
    GPIO_SPEED_CONFIG_LOW           =   (GPIO_SPEED_FREQ_LOW        << 18),
    GPIO_SPEED_CONFIG_MEDIUM        =   (GPIO_SPEED_FREQ_MEDIUM     << 18),
    GPIO_SPEED_CONFIG_HIGH          =   (GPIO_SPEED_FREQ_HIGH       << 18),
    GPIO_SPEED_CONFIG_VERY_HIGH     =   (GPIO_SPEED_FREQ_VERY_HIGH  << 18)
} gpio_speed_e;

typedef enum
{
    GPIO_ALTERNATIVE_FUNCTION_0     =   (0  <<  24),
    GPIO_ALTERNATIVE_FUNCTION_1     =   (1  <<  24),
    GPIO_ALTERNATIVE_FUNCTION_2     =   (2  <<  24),
    GPIO_ALTERNATIVE_FUNCTION_3     =   (3  <<  24),
    GPIO_ALTERNATIVE_FUNCTION_4     =   (4  <<  24),
    GPIO_ALTERNATIVE_FUNCTION_5     =   (5  <<  24),
    GPIO_ALTERNATIVE_FUNCTION_6     =   (6  <<  24),
    GPIO_ALTERNATIVE_FUNCTION_7     =   (7  <<  24),
    GPIO_ALTERNATIVE_FUNCTION_8     =   (8  <<  24),
    GPIO_ALTERNATIVE_FUNCTION_9     =   (9  <<  24),
    GPIO_ALTERNATIVE_FUNCTION_10    =   (10 <<  24),
    GPIO_ALTERNATIVE_FUNCTION_11    =   (11 <<  24),
    GPIO_ALTERNATIVE_FUNCTION_12    =   (12 <<  24),
    GPIO_ALTERNATIVE_FUNCTION_13    =   (13 <<  24),
    GPIO_ALTERNATIVE_FUNCTION_14    =   (14 <<  24),
    GPIO_ALTERNATIVE_FUNCTION_15    =   (15 <<  24),

} gpio_alternative_function_e;

typedef enum
{
    GPIO_EXTI_MODE_NONE         =   EXTI_MODE_NONE,
    GPIO_EXTI_MODE_INTERRUPT    =   EXTI_MODE_INTERRUPT,
    GPIO_EXTI_MODE_EVENT        =   EXTI_MODE_EVENT
} gpio_exti_mode_e;

typedef enum
{
    GPIO_EXTI_TRIGGER_NONE      =   EXTI_TRIGGER_NONE       << 8,
    GPIO_EXTI_TRIGGER_RISING    =   EXTI_TRIGGER_RISING     << 8,
    GPIO_EXTI_TRIGGER_FALLING   =   EXTI_TRIGGER_FALLING    << 8
} gpio_exti_trigger_e;

int gpio_register(void);

#endif /* DRIVERS_GPIO_GPIO_H_ */
