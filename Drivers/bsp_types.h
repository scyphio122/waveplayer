/*
 * bsp_types.h
 *
 *  Created on: Jan 17, 2020
 *      Author: konrad
 */

#ifndef DRIVERS_BSP_TYPES_H_
#define DRIVERS_BSP_TYPES_H_

#include <stdint-gcc.h>

typedef enum
{
	BSP_SUCCESS,
	BSP_INVALID_ARGS,
	BSP_INVALID_STATE,
	BSP_NOT_IMPLEMENTED,
	BSP_INTERNAL_ERROR
} bsp_status_e;

typedef struct
{
   uint32_t     offset;
   uint32_t     flags;
   uint32_t     data0;
   void*        data1;
} bsp_iop_t;

typedef struct
{
    uint32_t    flags;
    uint32_t    mode;
    bsp_iop_t*  iop;
} bsp_open_close_args_t;

typedef struct
{
    uint32_t    flags;
	void* 		buffer;
	uint32_t	bytes_requested;
	uint32_t	bytes_consumed;
	bsp_iop_t*  iop;
} bsp_rw_args_t;

typedef struct
{
    uint32_t    command;
    void*       buffer;
    uint32_t    bytes_requested;
    uint32_t    bytes_consumed;
    bsp_iop_t*  iop;
} bsp_ioctl_args_t;

typedef struct
{
	void*		port;
	uint32_t 	pin;
	uint32_t    alternative_function;
	int         fd;
} bsp_periph_config_t;

#endif /* DRIVERS_BSP_TYPES_H_ */
