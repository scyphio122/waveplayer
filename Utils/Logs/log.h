/*
 * log.h
 *
 *  Created on: Jan 17, 2020
 *      Author: konrad
 */

#ifndef UTILS_LOGS_LOG_H_
#define UTILS_LOGS_LOG_H_

#include <string.h>

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

typedef enum
{
    LOG_TYPE_TRACE,
    LOG_TYPE_DEBUG,
    LOG_TYPE_INFO,
    LOG_TYPE_WARN,
    LOG_TYPE_ERROR,
    LOG_TYPE_IMPORTANT
} log_type_e;

int log_init(void);

void log_deinit(void);

void log_write(log_type_e type, const char* filename, const int line, char* log_text, ...);

void log_flush(void);

#ifndef LOG_TAG
    #define LOG_TAG ""
#endif

#define LOG_TRACE(x...)        log_write(LOG_TYPE_TRACE,       __FILENAME__, __LINE__, LOG_TAG x)
#define LOG_DEBUG(x...)        log_write(LOG_TYPE_DEBUG,       __FILENAME__, __LINE__, LOG_TAG x)
#define LOG_INFO(x...)         log_write(LOG_TYPE_INFO,        __FILENAME__, __LINE__, LOG_TAG x)
#define LOG_WARN(x...)         log_write(LOG_TYPE_WARN,        __FILENAME__, __LINE__, LOG_TAG x)
#define LOG_ERROR(x...)        log_write(LOG_TYPE_ERROR,       __FILENAME__, __LINE__, LOG_TAG x)
#define LOG_IMPORTANT(x...)    log_write(LOG_TYPE_IMPORTANT,   __FILENAME__, __LINE__, LOG_TAG x)

#define LOG_BG_ORANGE          "\033[48;5;202m"
#define LOG_BG_VIOLET          "\033[48;5;129m"


#endif /* UTILS_LOGS_LOG_H_ */
