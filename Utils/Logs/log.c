/*
 * log.c
 *
 *  Created on: Jan 17, 2020
 *      Author: konrad
 */

#include <log.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stddef.h>

#include <FreeRTOS.h>
#include <stream_buffer.h>
#include <task.h>
#include <task_configs.h>
#include <bsp_types.h>
#include <bsp_peripheral_mapping.h>
#include <driver_manager.h>

#define LOG_TASK_NAME                   "LOG_TASK"
#define LOG_TASK_PRIO                   (0lu)
#define LOG_TASK_STACK_SIZE             (2048lu)

#define LOG_STREAMBUFFER_SIZE           (8192lu)
#define LOG_BUFFER_SIZE                 (512u)

static void s_log_push(char* log);

static void    s_logger_thread_worker(void* args);

static bool s_is_interrupt(void);

static StreamBufferHandle_t     s_log_stream_buffer_handle;
static TaskHandle_t             s_log_task_handle;

static int                      s_log_device_fd;

static bool                     s_log_initialized = false;

static char                     s_log_buf[LOG_BUFFER_SIZE];

int log_init(void)
{
    int retval = 0;
    s_log_stream_buffer_handle = xStreamBufferGenericCreate(LOG_STREAMBUFFER_SIZE, sizeof(char), pdFALSE);
    if (s_log_stream_buffer_handle == NULL)
    {
        retval = -1;
    }

    if (retval == 0)
    {
        s_log_device_fd = driver_open(LOG_UART_NAME, NULL);
        if (s_log_device_fd == -1)
        {
            retval = -1;
        }
    }

    if (retval == 0)
    {
        BaseType_t status = xTaskCreate(
                                s_logger_thread_worker,
                                LOG_TASK_NAME,
                                LOG_TASK_STACK_SIZE,
                                NULL,
                                LOG_TASK_PRIO,
                                &s_log_task_handle
                            );
        if (status != pdPASS)
        {
            retval = -1;
        }
    }

    if (retval == 0)
    {
        s_log_initialized = true;
    }

    return retval;
}

void log_deinit(void)
{
    if (s_log_stream_buffer_handle != NULL)
    {
        vStreamBufferDelete(s_log_stream_buffer_handle);
        s_log_stream_buffer_handle = NULL;
    }

    if (s_log_task_handle != NULL)
    {
        vTaskDelete(s_log_task_handle);
        s_log_task_handle = NULL;
    }

    s_log_initialized = false;
}

void log_write(log_type_e type, const char* filename, const int line, char* log_text, ...)
{
    char log_buffer[LOG_BUFFER_SIZE];

    uint32_t tick_count = xTaskGetTickCount();
    uint32_t secs    = tick_count / 1000;
    uint8_t  hours   = secs / 3600;
    uint8_t  minutes = (secs / 60) % 60;
    secs = secs % 60;

    if (s_log_initialized == false)
    {
        return;
    }

    switch (type)
    {
        case LOG_TYPE_TRACE:
        {
            snprintf(
                 log_buffer, LOG_BUFFER_SIZE - 1,
                 "%02hu:%02hu:%02lu.%03lu \e[36;1mTRACE: %s:%d \e[0m ",
                 hours,
                 minutes,
                 secs,
                 tick_count % 1000,
                 filename,
                 line
             );
        } break;

        case LOG_TYPE_DEBUG:
        {
            snprintf(
                log_buffer, LOG_BUFFER_SIZE - 1,
                 "%02hu:%02hu:%02lu.%03lu \e[34;1mDEBUG: %s:%d \e[0m ",
                 hours,
                 minutes,
                 secs,
                 tick_count % 1000,
                 filename,
                 line
             );
        } break;

        case LOG_TYPE_INFO:
        {
            snprintf(
                log_buffer, LOG_BUFFER_SIZE - 1,
                 "%02hu:%02hu:%02lu.%03lu \e[32;1mINFO: %s:%d \e[0m ",
                 hours,
                 minutes,
                 secs,
                 tick_count % 1000,
                 filename,
                 line
             );
        } break;

        case LOG_TYPE_WARN:
        {
            snprintf(
                log_buffer,
                 LOG_BUFFER_SIZE - 1,
                 "%02hu:%02hu:%02lu.%03lu \e[33;1mWARNING: %s:%d \e[0m ",
                 hours,
                 minutes,
                 secs,
                 tick_count % 1000,
                 filename,
                 line
             );
        } break;

        case LOG_TYPE_ERROR:
        {
            snprintf(
                 log_buffer, LOG_BUFFER_SIZE - 1,
                 "%02hu:%02hu:%02lu.%03lu \e[31;1mERROR: %s:%d \e[0m ",
                 hours,
                 minutes,
                 secs,
                 tick_count % 1000,
                 filename,
                 line
             );
        } break;

        case LOG_TYPE_IMPORTANT:
        {
            snprintf(
                log_buffer, LOG_BUFFER_SIZE - 1,
                 "%02hu:%02hu:%02lu.%03lu \e[35;1mIMPORTANT: %s:%d \e[0m ",
                 hours,
                 minutes,
                 secs,
                 tick_count % 1000,
                 filename,
                 line
             );
        } break;
    }

    char* header = NULL;
    uint32_t header_size = strlen(log_buffer);
    header = (char*)((uint32_t)log_buffer + header_size);

    va_list args;
    va_start(args, log_text);
    vsprintf(header, log_text, args);
    va_end(args);

    header = (char*)((uint32_t)log_buffer + strlen(log_buffer));
    *header = '\r';
    *(header + 1) = '\n';
    *(header + 2) = '\0';

    s_log_push(log_buffer);
}

void log_flush(void)
{
    if (s_log_stream_buffer_handle != NULL)
    {
        xStreamBufferReset(s_log_stream_buffer_handle);
    }
}

static void s_log_push(char* log)
{
    if (log == NULL)
    {
        return;
    }

    if (s_log_stream_buffer_handle != NULL)
    {
        if (s_is_interrupt())
        {
            driver_write(s_log_device_fd, log, strlen(log));

//            xStreamBufferSendFromISR(s_log_stream_buffer_handle, log, strlen(log), NULL);
        }
        else
        {
            xStreamBufferSend(s_log_stream_buffer_handle, log, strlen(log), portMAX_DELAY);
        }
    }
}

static void s_logger_thread_worker(void* args)
{
    uint32_t was_read = 0;

    while (1)
    {
        uint32_t bytes_read = 0;

        while (xStreamBufferBytesAvailable(s_log_stream_buffer_handle))
        {
            was_read = xStreamBufferReceive(
                                s_log_stream_buffer_handle,
                                &s_log_buf[bytes_read++],
                                sizeof(uint8_t),
                                pdMS_TO_TICKS(1000)
                            );

            if ((was_read == 0) || (bytes_read == sizeof(s_log_buf)))
            {
                break;
            }
        }

        if (bytes_read != 0)
        {
            driver_write(s_log_device_fd, s_log_buf, bytes_read);
        }
        else
        {
            uint32_t ticks = pdMS_TO_TICKS(10);
            vTaskDelay(ticks);
        }
    }
}

static inline bool s_is_interrupt(void)
{
    return (SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk) != 0 ;
}
