/*
 * driver_manager.c
 *
 *  Created on: Jan 19, 2020
 *      Author: konrad
 */

#include <driver_manager.h>
#include <log.h>

#include <stdlib.h>
#include <stdbool.h>

#include <FreeRTOS.h>
#include <semphr.h>

#define DRIVER_INSTANCES_CNT            (32u)

static driver_file_t        s_driver_instances[DRIVER_INSTANCES_CNT];

static uint32_t             s_drivers_registered_cnt;

static SemaphoreHandle_t    s_drivers_manager_mutex;

int driver_initialize(void)
{
    int retval = 0;

    s_drivers_manager_mutex = xSemaphoreCreateMutex();
    if (s_drivers_manager_mutex == NULL)
    {
        LOG_ERROR("FAILED TO CREATE DRIVER MANAGER MUTEX");
        retval = -1;
    }

    return retval;
}

int driver_register(
        driver_instance_t*      instance_to_install,
        uint32_t                instances_cnt
)
{
    int retval = 0;

    if (s_drivers_registered_cnt == DRIVER_INSTANCES_CNT)
    {
        LOG_ERROR("TOO MANY DRIVERS REGISTERED");
        retval = -1;
    }

    if (instance_to_install == NULL)
    {
        LOG_ERROR("NULL DRIVER CALLBACK PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        if (instance_to_install->driver_name == NULL)
        {
            LOG_ERROR("NULL DRIVER NAME PROVIDED");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        xSemaphoreTake(s_drivers_manager_mutex, portMAX_DELAY);

        for (uint32_t i=0; i<instances_cnt; ++i)
        {
            s_driver_instances[s_drivers_registered_cnt].instance                   =   &instance_to_install[i];
            s_driver_instances[s_drivers_registered_cnt].driver_major               =   s_drivers_registered_cnt;
            s_driver_instances[s_drivers_registered_cnt].driver_minor               =   i;
            s_driver_instances[s_drivers_registered_cnt].is_opened                  =   false;
            s_driver_instances[s_drivers_registered_cnt].pos                        =   0;
            s_driver_instances[s_drivers_registered_cnt].size                       =   0;
            s_drivers_registered_cnt++;
        }

        if (instance_to_install->driver_callbacks.init_cb != NULL)
        {
            retval = instance_to_install->driver_callbacks.init_cb();
        }

        xSemaphoreGive(s_drivers_manager_mutex);
    }

    return retval;
}

int driver_find(
        const char*             driver_name,
        int*                    major,
        int*                    minor,
        driver_callbacks_t*     driver_callbacks
)
{
    int retval = 0;

    if (s_drivers_registered_cnt == DRIVER_INSTANCES_CNT)
    {
        LOG_ERROR("TOO MANY DRIVERS REGISTERED");
        retval = -1;
    }

    if (driver_name == NULL)
    {
        LOG_ERROR("NULL DRIVER NAME PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        xSemaphoreTake(s_drivers_manager_mutex, portMAX_DELAY);

        bool found = false;
        for (uint32_t i=0; i<s_drivers_registered_cnt; ++i)
        {
            if (strcmp(s_driver_instances[s_drivers_registered_cnt].instance->driver_name, driver_name) == 0)
            {
                found = true;

                if (driver_callbacks != NULL)
                {
                    *driver_callbacks = s_driver_instances[s_drivers_registered_cnt].instance->driver_callbacks;
                }

                if (major != NULL)
                {
                    *major = s_driver_instances[s_drivers_registered_cnt].driver_major;
                }

                if (minor != NULL)
                {
                    *minor = s_driver_instances[s_drivers_registered_cnt].driver_minor;
                }

                break;
            }
        }

        if (found == false)
        {
            LOG_ERROR("FAILED TO FIND DRIVER NAME: %s", driver_name);
            retval = -1;
        }

        xSemaphoreGive(s_drivers_manager_mutex);
    }

    return retval;
}

int driver_open(char* device_name, void* args)
{
    int fd = 0;
    bool found = false;

    if (device_name == NULL)
    {
        LOG_ERROR("NULL DEVICE NAME PROVIDED");
        fd = -1;
    }

    if (fd != -1)
    {
        for (uint32_t i=0; i<s_drivers_registered_cnt; ++i)
        {
            if (strcmp(s_driver_instances[i].instance->driver_name, device_name) == 0)
            {
                found = true;
                if (s_driver_instances[i].is_opened == true)
                {
#if 0
                    LOG_ERROR("DEVICE: %s IS ALREADY OPENED");
#endif
                    fd = i;
                }
                else
                {
                    bsp_open_close_args_t arg;
                    bsp_iop_t iop;
                    iop.data1 = args;
                    arg.iop = &iop;

                    int retval = s_driver_instances[i].instance->driver_callbacks.open_cb(
                                        s_driver_instances[i].driver_major,
                                        s_driver_instances[i].driver_minor,
                                        &arg
                                 );
                    if (retval != 0)
                    {
                        LOG_ERROR("FAILURE DURING OPENING DRIVER: %s", device_name);
                        fd = -1;
                    }
                    else
                    {
                        fd = (int)i;
                        s_driver_instances[fd].is_opened    =   true;
                        s_driver_instances[fd].pos          =   0;
                    }
                }

                break;
            }
        }
    }

    if (found == false)
    {
        LOG_ERROR("DEVICE %s NOT REGISTERED", device_name);
        fd = -1;
    }

    return fd;
}

int driver_close(int fd)
{
    int retval = 0;
    driver_file_t* descriptor = NULL;

    if ((fd < 0) || (fd >= DRIVER_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID FILE DESCRIPTOR: %ld", fd);
        retval = -1;
    }

    if (retval == 0)
    {
        descriptor = &s_driver_instances[fd];
        if (descriptor->is_opened == false)
        {
            LOG_ERROR("THE FILE %ld IS NOT OPENED", fd);
            retval = -1;
        }
    }

    if (retval == 0)
    {
        retval = descriptor->instance->driver_callbacks.close_cb(
                    descriptor->driver_major,
                    descriptor->driver_minor,
                    NULL
                );
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO CLOSE THE FILE DESCRIPTOR: %ld", fd);
        }
        else
        {
            descriptor->is_opened   =   false;
            descriptor->pos         =   0;
            descriptor->size        =   0;
        }
    }

    return retval;
}

int driver_write(int fd, void* buffer, uint32_t buffer_size)
{
    int retval = 0;
    driver_file_t* descriptor = NULL;

    if ((fd < 0) || (fd >= DRIVER_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID FILE DESCRIPTOR: %ld", fd);
        retval = -1;
    }

    if (buffer == NULL)
    {
        LOG_ERROR("NULL BUFFER PROVIDED");
        retval = -1;
    }

    if (buffer_size == 0)
    {
        LOG_ERROR("ZERO LENGTH BUFFER PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        descriptor = &s_driver_instances[fd];
        if (descriptor->is_opened == false)
        {
            LOG_ERROR("THE FILE %ld IS NOT OPENED", fd);
            retval = -1;
        }
    }

    if (retval == 0)
    {
        bsp_iop_t iop;
        iop.offset = descriptor->pos;

        bsp_rw_args_t args;
        args.buffer             =   buffer;
        args.bytes_consumed     =   0;
        args.bytes_requested    =   buffer_size;
        args.flags              =   s_driver_instances[fd].global_flags;
        args.iop                =   &iop;

        retval = descriptor->instance->driver_callbacks.write_cb(
                    descriptor->driver_major,
                    descriptor->driver_minor,
                    &args
                );
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO WRITE DATA TO THE FILE DESCRIPTOR: %ld", fd);
        }
        else
        {
            descriptor->pos         +=   args.bytes_consumed;
        }
    }

    return retval;
}

int driver_read(int fd, void* buffer, uint32_t buffer_size)
{
    int retval = 0;
    driver_file_t* descriptor = NULL;

    if ((fd < 0) || (fd >= DRIVER_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID FILE DESCRIPTOR: %ld", fd);
        retval = -1;
    }

    if (buffer == NULL)
    {
        LOG_ERROR("NULL BUFFER PROVIDED");
        retval = -1;
    }

    if (buffer_size == 0)
    {
        LOG_ERROR("ZERO LENGTH BUFFER PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        descriptor = &s_driver_instances[fd];
        if (descriptor->is_opened == false)
        {
            LOG_ERROR("THE FILE %ld IS NOT OPENED", fd);
            retval = -1;
        }
    }

    if (retval == 0)
    {
        bsp_iop_t iop;
        iop.offset = s_driver_instances[fd].pos;

        bsp_rw_args_t args;
        args.buffer             =   buffer;
        args.bytes_consumed     =   0;
        args.bytes_requested    =   buffer_size;
        args.flags              =   s_driver_instances[fd].global_flags;
        args.iop                =   &iop;

        retval = descriptor->instance->driver_callbacks.read_cb(
                    descriptor->driver_major,
                    descriptor->driver_minor,
                    &args
                );
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO READ DATA TO THE FILE DESCRIPTOR: %ld", fd);
        }
        else
        {
            descriptor->pos         +=   args.bytes_consumed;
        }
    }

    return retval;
}

int driver_ioctl(int fd, bsp_ioctl_args_t* args)
{
    int retval = 0;
    driver_file_t* descriptor = NULL;

    if ((fd < 0) || (fd >= DRIVER_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID FILE DESCRIPTOR: %ld", fd);
        retval = -1;
    }

    if (retval == 0)
    {
        descriptor = &s_driver_instances[fd];
        if (descriptor->is_opened == false)
        {
            LOG_ERROR("THE FILE %ld IS NOT OPENED", fd);
            retval = -1;
        }
    }

    if (retval == 0)
    {
        if (descriptor->instance->driver_callbacks.ioctl_cb != NULL)
        {
            retval = descriptor->instance->driver_callbacks.ioctl_cb(
                            descriptor->driver_major,
                            descriptor->driver_minor,
                            args
                        );
        }
        else
        {
            LOG_ERROR("IOCTL CALLBACK NOT SET TO THE DEVICE %s", descriptor->instance->driver_name);
            retval = -1;
        }
    }

    return retval;
}

int driver_fseek(int fd, uint32_t offset, uint32_t whence)
{
    int retval = 0;
    driver_file_t* descriptor = NULL;

    if ((fd < 0) || (fd >= DRIVER_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID FILE DESCRIPTOR: %ld", fd);
        retval = -1;
    }

    if (retval == 0)
    {
        descriptor = &s_driver_instances[fd];
        if (descriptor->is_opened == false)
        {
            LOG_ERROR("THE FILE %ld IS NOT OPENED", fd);
            retval = -1;
        }
    }

    if (retval == 0)
    {
        switch (whence)
        {
            case SEEK_SET:
            {
                if (offset < 0)
                {
                    LOG_ERROR("CANNOT SET NEGATIVE OFFSET IN SEEK_SET WHENCE");
                }
                else
                {
                    descriptor->pos = offset;
                }
            } break;

            case SEEK_CUR:
            {
                descriptor->pos += offset;
            } break;

            case SEEK_END:
            {
                if (offset < 0)
                {
                    LOG_ERROR("CANNOT SET NEGATIVE OFFSET IN SEEK_END WHENCE");
                }
                else
                {
                    descriptor->pos = descriptor->size - offset;
                }
            } break;

            default:
            {
                LOG_ERROR("INVALID WHENCE VALUE: %ld. EXPECTING SEEK_SET, SEEK_CUR OR SEEK_END VALUE", whence);
            }
        }

    }

    return retval;
}

int driver_set_flags(int fd, uint32_t flags)
{
    int retval = 0;

    if ((fd < 0) || (fd >= DRIVER_INSTANCES_CNT))
    {
        LOG_ERROR("INVALID FILE DESCRIPTOR: %ld", fd);
        retval = -1;
    }

    if (retval == 0)
    {
        s_driver_instances[fd].global_flags = flags;
    }

    return retval;
}
