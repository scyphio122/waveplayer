/*
 * driver_manager.h
 *
 *  Created on: Jan 19, 2020
 *      Author: konrad
 */

#ifndef OS_DRIVER_MANAGER_DRIVER_MANAGER_H_
#define OS_DRIVER_MANAGER_DRIVER_MANAGER_H_

#include <bsp_types.h>

#include <stdio.h>
#include <stdbool.h>

#define DRIVER_MEMORY_DEVICE_GET_SIZE               0x80000001

typedef bsp_status_e (*driver_initialize_f)(void);

typedef bsp_status_e (*driver_open_f)(int, int, bsp_open_close_args_t*);

typedef bsp_status_e (*driver_close_f)(int, int, bsp_open_close_args_t*);

typedef bsp_status_e (*driver_write_f)(int, int, bsp_rw_args_t*);

typedef bsp_status_e (*driver_read_f)(int, int, bsp_rw_args_t*);

typedef bsp_status_e (*driver_ioctl_f)(int, int, bsp_ioctl_args_t*);

typedef struct
{
    driver_initialize_f     init_cb;

    driver_open_f           open_cb;

    driver_close_f          close_cb;

    driver_write_f          write_cb;

    driver_read_f           read_cb;

    driver_ioctl_f          ioctl_cb;
} driver_callbacks_t;

typedef struct
{
    const char*             driver_name;

    driver_callbacks_t      driver_callbacks;
} driver_instance_t;

int driver_initialize(void);

int driver_register(
        driver_instance_t*      instance_to_install,
        uint32_t                instances_cnt
);

int driver_find(
        const char*             driver_name,
        int*                    major,
        int*                    minor,
        driver_callbacks_t*     driver_callbacks
);

typedef struct
{
    driver_instance_t*  instance;

    bool                is_opened;

    uint32_t            pos;

    uint32_t            size;

    int                 driver_major;

    int                 driver_minor;

    uint32_t            global_flags;

} driver_file_t;

int driver_open(char* device_name, void* args);

int driver_close(int fd);

int driver_write(int fd, void* buffer, uint32_t buffer_size);

int driver_read(int fd, void* buffer, uint32_t buffer_size);

int driver_ioctl(int fd, bsp_ioctl_args_t* args);

int driver_fseek(int fd, uint32_t offset, uint32_t whence);

int driver_set_flags(int fd, uint32_t flags);

#endif /* OS_DRIVER_MANAGER_DRIVER_MANAGER_H_ */
