/*
 * ff_qspi_disk.c
 *
 *  Created on: Jan 23, 2020
 *      Author: konrad
 */


#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "portmacro.h"
#include "ff_qspi_disk.h"

/* FreeRTOS+FAT includes. */
#include "ff_headers.h"
#include "ff_sys.h"

#include <log.h>
#include <driver_manager.h>

#define QSPI_HIDDEN_SECTOR_COUNT      8
#define QSPI_PRIMARY_PARTITIONS       1
#define QSPI_HUNDRED_64_BIT           100ULL
#define QSPI_PARTITION_NUMBER         0 /* Only a single partition is used. */
#define QSPI_BYTES_PER_KB             ( 1024ull )
#define QSPI_SECTORS_PER_KB           ( QSPI_BYTES_PER_KB / QSPI_SECTOR_SIZE )

/* Used as a magic number to indicate that an FF_Disk_t structure is a RAM
disk. */
#define QSPI_SIGNATURE              0xABCDAEBF

/*-----------------------------------------------------------*/

/*
 * The function that writes to the media - as this is implementing a RAM disk
 * the media is just a RAM buffer.
 */
static int32_t s_qspi_write( uint8_t *pucBuffer, uint32_t ulSectorNumber, uint32_t ulSectorCount, FF_Disk_t *pxDisk );

/*
 * The function that reads from the media - as this is implementing a RAM disk
 * the media is just a RAM buffer.
 */
static int32_t s_qspi_read( uint8_t *pucBuffer, uint32_t ulSectorNumber, uint32_t ulSectorCount, FF_Disk_t *pxDisk );

/*
 * This is the driver for a RAM disk.  Unlike most media types, RAM disks are
 * volatile so are created anew each time the system is booted.  As the disk is
 * new and just created, it must also be partitioned and formatted.
 */
static FF_Error_t s_partition_and_format_disk( FF_Disk_t *pxDisk );

static int  s_qspi_fd = -1;

/*-----------------------------------------------------------*/

/* This is the prototype of the function used to initialise the RAM disk driver.
Other media drivers do not have to have the same prototype.

In this example:
 + pcName is the name to give the disk within FreeRTOS+FAT's virtual file system.
 + pucDataBuffer is the start of the RAM to use as the disk.
 + xIOManagerCacheSize is the size of the IO manager's cache, which must be a
   multiple of the sector size, and at least twice as big as the sector size.
*/
FF_Disk_t *FF_QSPIDiskInit( char *pcName, size_t xIOManagerCacheSize )
{
    FF_Error_t xError;
    FF_Disk_t *pxDisk = NULL;
    FF_CreationParameters_t xParameters;
    uint32_t ulSectorCount = 0;

    /* Check the validity of the xIOManagerCacheSize parameter. */
    configASSERT( ( xIOManagerCacheSize % QSPI_SECTOR_SIZE ) == 0 );
    configASSERT( ( xIOManagerCacheSize >= ( 2 * QSPI_SECTOR_SIZE ) ) );

    s_qspi_fd = driver_open("/dev/memblk", NULL);
    if (s_qspi_fd == -1)
    {
        LOG_ERROR("FAILED TO OPEN QSPI DRIVER");
    }
    else
    {
        bsp_ioctl_args_t args;
        uint32_t size_bytes = 0;
        args.command = DRIVER_MEMORY_DEVICE_GET_SIZE;
        args.buffer  = &size_bytes;

        int retval = driver_ioctl(s_qspi_fd, &args);
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO READ READ SIZE OF THE QSPI MEMORY");
        }
        else
        {
            ulSectorCount = size_bytes / QSPI_SECTOR_SIZE;
            /* Attempt to allocated the FF_Disk_t structure. */
            pxDisk = ( FF_Disk_t * ) pvPortMalloc( sizeof( FF_Disk_t ) );
        }
    }

    if( pxDisk != NULL )
    {
        /* Start with every member of the structure set to zero. */
        memset( pxDisk, '\0', sizeof( FF_Disk_t ) );

        /* The pvTag member of the FF_Disk_t structure allows the structure to be
        extended to also include media specific parameters.  The only media
        specific data that needs to be stored in the FF_Disk_t structure for a
        RAM disk is the location of the RAM buffer itself - so this is stored
        directly in the FF_Disk_t's pvTag member. */
        pxDisk->pvTag = ( void * ) 0x00000000;

        /* The signature is used by the disk read and disk write functions to
        ensure the disk being accessed is a RAM disk. */
        pxDisk->ulSignature = QSPI_SIGNATURE;

        /* The number of sectors is recorded for bounds checking in the read and
        write functions. */
        pxDisk->ulNumberOfSectors = ulSectorCount;

        /* Create the IO manager that will be used to control the RAM disk. */
        memset( &xParameters, '\0', sizeof( xParameters ) );
        xParameters.pucCacheMemory  = NULL;
        xParameters.ulMemorySize    = xIOManagerCacheSize;
        xParameters.ulSectorSize    = QSPI_SECTOR_SIZE;
        xParameters.fnWriteBlocks   = s_qspi_write;
        xParameters.fnReadBlocks    = s_qspi_read;
        xParameters.pxDisk          = pxDisk;

        /* Driver is reentrant so xBlockDeviceIsReentrant can be set to pdTRUE.
        In this case the semaphore is only used to protect FAT data
        structures. */
        xParameters.pvSemaphore = ( void * ) xSemaphoreCreateRecursiveMutex();
        xParameters.xBlockDeviceIsReentrant = pdFALSE;

        pxDisk->pxIOManager = FF_CreateIOManger( &xParameters, &xError );

        if( ( pxDisk->pxIOManager != NULL ) && ( FF_isERR( xError ) == pdFALSE ) )
        {
            /* Record that the QSPI disk has been initialised. */
            pxDisk->xStatus.bIsInitialised = pdTRUE;
            if( FF_isERR( xError ) == pdFALSE )
            {
                /* Record the partition number the FF_Disk_t structure is, then
                mount the partition. */
                pxDisk->xStatus.bPartitionNumber = QSPI_PARTITION_NUMBER;

                /* Mount the partition. */
                xError = FF_Mount( pxDisk, QSPI_PARTITION_NUMBER );
                LOG_INFO( "FF_QSPIDiskInit: FF_Mount: %s\n", ( const char * ) FF_GetErrMessage( xError ) );
            }

            /* If no partition existing */
            if ((FF_isERR( xError ) == pdTRUE ) && (xError & FF_ERR_IOMAN_NO_MOUNTABLE_PARTITION))
            {
                /* Create a partition on the QSPI disk. */
                xError = s_partition_and_format_disk( pxDisk );
                if( FF_isERR( xError ) == pdFALSE )
                {
                    xError = FF_Mount( pxDisk, QSPI_PARTITION_NUMBER );
                    LOG_INFO( "FF_QSPIDiskInit: FF_Mount: %s\n", ( const char * ) FF_GetErrMessage( xError ) );
                }
            }

            if( FF_isERR( xError ) == pdFALSE )
            {
                /* The partition mounted successfully, add it to the virtual
                file system - where it will appear as a directory off the file
                system's root directory. */
                FF_FS_Add( pcName, pxDisk );
            }
        }
        else
        {
            LOG_INFO( "FF_QSPIDiskInit: FF_CreateIOManger: %s\n", ( const char * ) FF_GetErrMessage( xError ) );
            pxDisk = NULL;
        }
    }
    else
    {
        LOG_ERROR( "FF_QSPIDiskInit: Malloc failed\n" );
    }

    return pxDisk;
}
/*-----------------------------------------------------------*/

BaseType_t FF_QSPIDiskDelete( FF_Disk_t *pxDisk )
{
    if( pxDisk != NULL )
    {
        pxDisk->ulSignature = 0;
        pxDisk->xStatus.bIsInitialised = 0;
        if( pxDisk->pxIOManager != NULL )
        {
            FF_DeleteIOManager( pxDisk->pxIOManager );
        }

        vPortFree( pxDisk );
    }

    return pdPASS;
}
/*-----------------------------------------------------------*/

static int32_t s_qspi_read( uint8_t *pucDestination, uint32_t ulSectorNumber, uint32_t ulSectorCount, FF_Disk_t *pxDisk )
{
    int32_t lReturn;
    uint8_t *pucSource;

    if( pxDisk != NULL )
    {
        if( pxDisk->ulSignature != QSPI_SIGNATURE )
        {
            /* The disk structure is not valid because it doesn't contain a
            magic number written to the disk when it was created. */
            lReturn = FF_ERR_IOMAN_DRIVER_FATAL_ERROR | FF_ERRFLAG;
        }
        else if( pxDisk->xStatus.bIsInitialised == pdFALSE )
        {
            /* The disk has not been initialised. */
            lReturn = FF_ERR_IOMAN_OUT_OF_BOUNDS_WRITE | FF_ERRFLAG;
        }
        else if( ulSectorNumber >= pxDisk->ulNumberOfSectors )
        {
            /* The start sector is not within the bounds of the disk. */
            lReturn = ( FF_ERR_IOMAN_OUT_OF_BOUNDS_WRITE | FF_ERRFLAG );
        }
        else if( ( pxDisk->ulNumberOfSectors - ulSectorNumber ) < ulSectorCount )
        {
            /* The end sector is not within the bounds of the disk. */
            lReturn = ( FF_ERR_IOMAN_OUT_OF_BOUNDS_WRITE | FF_ERRFLAG );
        }
        else
        {
            /* Move to the start of the sector being read. */
            pucSource = (uint8_t*)( QSPI_SECTOR_SIZE * ulSectorNumber );

            /* Copy the data from the disk.  As this is a RAM disk this can be
            done using memcpy(). */
            if (s_qspi_fd != -1)
            {
                lReturn = driver_fseek(s_qspi_fd, (uint32_t)pucSource, SEEK_SET);
                if (lReturn == -1)
                {
                    LOG_ERROR("FAILED TO SET QSPI MEMORY CURRENT OFFSET TO: 0x%08X", ulSectorNumber * QSPI_SECTOR_SIZE);
                }

                if (lReturn == 0)
                {
                    lReturn = driver_read(s_qspi_fd, pucDestination, ulSectorCount * QSPI_SECTOR_SIZE);
                    if (lReturn)
                    {
                        LOG_ERROR("FAILED TO STORE DATA AT ADDRESS:  0x%08X", ulSectorNumber * QSPI_SECTOR_SIZE);
                    }
                }
            }


            lReturn = FF_ERR_NONE;
        }
    }
    else
    {
        lReturn = FF_ERR_NULL_POINTER | FF_ERRFLAG;
    }

    return lReturn;
}
/*-----------------------------------------------------------*/

static int32_t s_qspi_write( uint8_t *pucSource, uint32_t ulSectorNumber, uint32_t ulSectorCount, FF_Disk_t *pxDisk )
{
    int32_t lReturn = FF_ERR_NONE;

    if( pxDisk != NULL )
    {
        if( pxDisk->ulSignature != QSPI_SIGNATURE )
        {
            /* The disk structure is not valid because it doesn't contain a
            magic number written to the disk when it was created. */
            lReturn = FF_ERR_IOMAN_DRIVER_FATAL_ERROR | FF_ERRFLAG;
        }
        else if( pxDisk->xStatus.bIsInitialised == pdFALSE )
        {
            /* The disk has not been initialised. */
            lReturn = FF_ERR_IOMAN_OUT_OF_BOUNDS_WRITE | FF_ERRFLAG;
        }
        else if( ulSectorNumber >= pxDisk->ulNumberOfSectors )
        {
            /* The start sector is not within the bounds of the disk. */
            lReturn = ( FF_ERR_IOMAN_OUT_OF_BOUNDS_WRITE | FF_ERRFLAG );
        }
        else if( ( pxDisk->ulNumberOfSectors - ulSectorNumber ) < ulSectorCount )
        {
            /* The end sector is not within the bounds of the disk. */
            lReturn = ( FF_ERR_IOMAN_OUT_OF_BOUNDS_WRITE | FF_ERRFLAG );
        }
        else
        {
            /* Write to the disk. */
            if (s_qspi_fd != -1)
            {
                LOG_TRACE("FS: STORING DATA AT BLOCK ADDRESS: 0x%08X. DATA LEN: %lu", ulSectorNumber * QSPI_SECTOR_SIZE, ulSectorCount * QSPI_SECTOR_SIZE);

                lReturn = driver_fseek(s_qspi_fd, ulSectorNumber * QSPI_SECTOR_SIZE, SEEK_SET);
                if (lReturn == -1)
                {
                    LOG_ERROR("FAILED TO SET QSPI MEMORY CURRENT OFFSET TO: 0x%08X", ulSectorNumber * QSPI_SECTOR_SIZE);
                }

                if (lReturn == 0)
                {
                    lReturn = driver_write(s_qspi_fd, pucSource, ulSectorCount * QSPI_SECTOR_SIZE);
                    if (lReturn)
                    {
                        LOG_ERROR("FAILED TO STORE DATA AT ADDRESS:  0x%08X", ulSectorNumber * QSPI_SECTOR_SIZE);
                    }
                }
            }

            lReturn = FF_ERR_NONE;
        }
    }
    else
    {
        lReturn = FF_ERR_NULL_POINTER | FF_ERRFLAG;
    }

    return lReturn;
}
/*-----------------------------------------------------------*/

static FF_Error_t s_partition_and_format_disk( FF_Disk_t *pxDisk )
{
    FF_PartitionParameters_t xPartition;
    FF_Error_t xError;

    /* Create a single partition that fills all available space on the disk. */
    memset( &xPartition, '\0', sizeof( xPartition ) );
    xPartition.ulSectorCount    = pxDisk->ulNumberOfSectors;
    xPartition.ulHiddenSectors  = QSPI_HIDDEN_SECTOR_COUNT;
    xPartition.xPrimaryCount    = QSPI_PRIMARY_PARTITIONS;
    xPartition.eSizeType        = eSizeIsQuota;

    /* Partition the disk */
    xError = FF_Partition( pxDisk, &xPartition );
    LOG_INFO( "FF_Partition: %s\n", ( const char * ) FF_GetErrMessage( xError ) );

    if( FF_isERR( xError ) == pdFALSE )
    {
        /* Format the partition. */
        xError = FF_Format( pxDisk, QSPI_PARTITION_NUMBER, pdFALSE, pdFALSE );
        LOG_INFO( "FF_QSPIDiskInit: FF_Format: %s\n", ( const char * ) FF_GetErrMessage( xError ) );
    }

    return xError;
}
/*-----------------------------------------------------------*/

BaseType_t FF_QSPIDiskShowPartition( FF_Disk_t *pxDisk )
{
    FF_Error_t xError;
    uint64_t ullFreeSectors;
    uint32_t ulTotalSizeKB, ulFreeSizeKB;
    int iPercentageFree;
    FF_IOManager_t *pxIOManager;
    const char *pcTypeName = "unknown type";
    BaseType_t xReturn = pdPASS;

    if( pxDisk == NULL )
    {
        xReturn = pdFAIL;
    }
    else
    {
        pxIOManager = pxDisk->pxIOManager;

        LOG_INFO( "Reading FAT and calculating Free Space\n" );

        switch( pxIOManager->xPartition.ucType )
        {
            case FF_T_FAT12:
                pcTypeName = "FAT12";
                break;

            case FF_T_FAT16:
                pcTypeName = "FAT16";
                break;

            case FF_T_FAT32:
                pcTypeName = "FAT32";
                break;

            default:
                pcTypeName = "UNKOWN";
                break;
        }

        FF_GetFreeSize( pxIOManager, &xError );

        ullFreeSectors = pxIOManager->xPartition.ulFreeClusterCount * pxIOManager->xPartition.ulSectorsPerCluster;
        if( pxIOManager->xPartition.ulDataSectors == ( uint32_t )0 )
        {
            iPercentageFree = 0;
        }
        else
        {
            iPercentageFree = ( int ) ( ( QSPI_HUNDRED_64_BIT * ullFreeSectors + pxIOManager->xPartition.ulDataSectors / 2 ) /
                ( ( uint64_t )pxIOManager->xPartition.ulDataSectors ) );
        }

        ulTotalSizeKB = pxIOManager->xPartition.ulDataSectors / 1024lu;
        ulFreeSizeKB = ( uint32_t ) ( ullFreeSectors / 1024lu );

        /* It is better not to use the 64-bit format such as %Lu because it
        might not be implemented. */
        LOG_INFO( "Partition Nr   %8u\n",                       pxDisk->xStatus.bPartitionNumber );
        LOG_INFO( "Type           %8u (%s)\n",                  pxIOManager->xPartition.ucType, pcTypeName );
        LOG_INFO( "VolLabel       '%8s' \n",                    pxIOManager->xPartition.pcVolumeLabel );
        LOG_INFO( "TotalSectors   %8lu\n",                      pxIOManager->xPartition.ulTotalSectors );
        LOG_INFO( "SecsPerCluster %8lu\n",                      pxIOManager->xPartition.ulSectorsPerCluster );
        LOG_INFO( "Size           %8lu KB\n",                   ulTotalSizeKB );
        LOG_INFO( "FreeSize       %8lu KB ( %d perc free )\n",  ulFreeSizeKB, iPercentageFree );
    }

    return xReturn;
}
/*-----------------------------------------------------------*/

void FF_QSPIDiskFlush( FF_Disk_t *pxDisk )
{
    if( ( pxDisk != NULL ) && ( pxDisk->xStatus.bIsInitialised != 0 ) && ( pxDisk->pxIOManager != NULL ) )
    {
        FF_FlushCache( pxDisk->pxIOManager );
    }
}
