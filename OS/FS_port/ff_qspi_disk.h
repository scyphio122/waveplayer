/*
 * ff_qspi_disc.h
 *
 *  Created on: Jan 23, 2020
 *      Author: konrad
 */

#ifndef OS_FREERTOS_FILE_SYSTEM_PORTABLE_COMMON_FF_QSPI_DISK_H_
#define OS_FREERTOS_FILE_SYSTEM_PORTABLE_COMMON_FF_QSPI_DISK_H_

#include "ff_headers.h"

#define QSPI_SECTOR_SIZE              512lu

/* Create a RAM disk, supplying enough memory to hold N sectors of 512 bytes each */
FF_Disk_t *FF_QSPIDiskInit( char *pcName, size_t xIOManagerCacheSize );

/* Release all resources */
BaseType_t FF_QSPIDiskDelete( FF_Disk_t *pxDisk );

/* Show some partition information */
BaseType_t FF_QSPIDiskShowPartition( FF_Disk_t *pxDisk );

void FF_QSPIDiskFlush( FF_Disk_t *pxDisk );

#endif /* OS_FREERTOS_FILE_SYSTEM_PORTABLE_COMMON_FF_QSPI_DISK_H_ */
