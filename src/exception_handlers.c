/*
 * exception_handlers.c
 *
 *  Created on: Jan 13, 2020
 *      Author: konrad
 */

#include <stm32l4xx.h>
#include <core_cm4.h>

void WWDG_IRQHandler(void);
void NMI_Handler(void);
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);

void WWDG_IRQHandler(void)
{
    __BKPT();
    while (1)
    {

    }
}

void NMI_Handler()
{
    __BKPT();
    while (1)
    {

    }
}

void HardFault_Handler(void)
{
    __BKPT();
//    while (1)
//    {
//
//    }
}

void MemManage_Handler(void)
{
    __BKPT();
    while (1)
    {

    }
}

void BusFault_Handler(void)
{
    __BKPT();
    while (1)
    {

    }
}

void UsageFault_Handler(void)
{
    __BKPT();
    while (1)
    {

    }
}
