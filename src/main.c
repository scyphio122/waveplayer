/*
 * main.c
 *
 *  Created on: Jan 13, 2020
 *      Author: konrad
 */
#include <gpio.h>
#include <uart.h>
#include <qspi.h>
#include <bsp_i2c.h>
#include <bsp_sai.h>
#include <log.h>
#include <driver_manager.h>
#include <clock_config.h>
#include <usb_device.h>
#include <usb_storage_task.h>

#include <audio_sink.h>
#include <audio_source.h>

#include <FreeRTOS.h>
#include <task.h>
#include <ff_stdio.h>
#include <ff_qspi_disk.h>
#include <stm32l4xx.h>
#include <stm32l4xx_hal_rcc.h>
#include <assert.h>

#include <cs43l22_dac.h>

#include <bsp_peripheral_mapping.h>


#define FS_TEST 0

static int  s_configure_clock(void);

static void s_main_task_create(void);

static void s_main_task_worker(void* args);

static void s_MX_GPIO_Init(void);

#if FS_TEST == 1
    static void s_file_system_test(void);
#endif

int main(void)
{
    int retval = 0;

    if (retval == 0)
    {
        retval = s_configure_clock();
    }

    if (retval == 0)
    {
        retval = driver_initialize();
    }

    if (retval == 0)
    {
        retval = gpio_register();
    }

    if (retval == 0)
    {
        retval = uart_register();
    }

    if (retval == 0)
    {
        retval = log_init();
    }

    if (retval == 0)
    {
        retval = qspi_register();
    }

    if (retval == 0)
    {
        retval = bsp_i2c_register();
    }

    if (retval == 0)
    {
        retval = bsp_sai_register();
    }

    if (retval == 0)
    {
        retval = usb_storage_task_init();
    }

    s_main_task_create();

    vTaskStartScheduler();

    LOG_DEBUG("OS STARTED");
    while (1)
    {
        vTaskDelay(pdMS_TO_TICKS(1000));
    }

	return 0;
}

static int s_configure_clock(void)
{
    int retval = 0;
    retval = clock_configure();
    if (retval == 0)
    {
        uint32_t sysclk_clock = HAL_RCC_GetSysClockFreq();
        assert(sysclk_clock == 80000000);
    }

    return retval;
}

static void s_main_task_create(void)
{
    TaskHandle_t main_task_handle;

    if (xTaskCreate(
            s_main_task_worker,
            "MAIN TASK",
            4 * configMINIMAL_STACK_SIZE,
            NULL,
            0,
            &main_task_handle) != pdTRUE)
    {
        LOG_ERROR("FAILED TO CREATE THE MAIN TASK");
    }
}

static void s_main_task_worker(void* args)
{
    LOG_DEBUG("MAIN TASK STARTED");
    int retval = 0;

    FF_Disk_t* nvm_disk = FF_QSPIDiskInit("/dev/sda", 1024);
    if (nvm_disk == NULL)
    {
        LOG_ERROR("FAILED TO INITIALIZE QSPI FAT FILE SYSTEM");
    }

    FF_FindData_t find_data;
    retval = ff_findfirst("/dev/sda", &find_data);
    if (retval != 0)
    {
        LOG_ERROR("FAILED TO FIND FILE IND MAIN DRI");
    }
    else
    {
        LOG_INFO("FOUND FILE: %s", find_data.pcFileName);

        while (ff_findnext(&find_data) == 0)
        {
            LOG_INFO("FOUND FILE: %s", find_data.pcFileName);
        }
    }
#if FS_TEST == 1
    s_file_system_test();
#endif

    s_MX_GPIO_Init();
    MX_USB_DEVICE_Init();

    retval = cs43l22_initialize();
    if (retval != BSP_SUCCESS)
    {
        LOG_ERROR("FAILED TO INITIALIZE CS43L22 DAC CHIP");
    }

    if (retval == 0)
    {
        retval = audio_sink_initialize();
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO INITIALIZE AUDIO SINK");
        }
    }

    if (retval == 0)
    {
        retval = audio_source_initialize();
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO INITIALIZE AUDIO SOURCE");
        }
    }

    if (retval == 0)
    {
        retval = audio_source_open_file("/dev/sda/They_Taking_The_Hobbits_To_Isengard.wav");
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO OPEN SONG");
        }
    }

#if 0
    if (retval == 0)
    {
        retval = audio_source_play();
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO START PLAYING THE SONG");
        }
    }
#endif

    while (1)
    {
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}

static void s_MX_GPIO_Init(void)
{
  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
}

#if FS_TEST == 1

uint8_t buffer[4096];

static void s_file_system_test(void)
{
    const char* text = ""
                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec tellus euismod, condimentum nisi malesuada, venenatis neque. Quisque tellus libero, sodales ac consequat id, porttitor et massa. Phasellus elementum eget nibh eu venenatis. Fusce congue, turpis quis molestie ultrices, augue erat tempor justo, id tempus mi felis non metus. Integer a ex sodales, venenatis mi nec, pharetra lacus. Cras vitae ultrices orci. Praesent ac ex quam. Etiam sollicitudin eu augue id semper. Morbi blandit elit odio, ut suscipit nunc imperdiet eget. Fusce placerat dui augue. Pellentesque velit nisi, fermentum ac tincidunt sed, porttitor nec ligula. Aenean rhoncus sapien eu gravida cursus. Sed egestas porttitor lacus vel rutrum. Etiam nisi sem, blandit ut turpis ut, dictum tristique mauris. Nunc ullamcorper lacus diam, et scelerisque diam laoreet facilisis."
                       "Vivamus ut libero consectetur, vestibulum urna id, semper purus. Integer ullamcorper tristique tortor id sollicitudin. Proin felis eros, blandit rutrum viverra nec, ornare vitae metus. Maecenas vel dapibus nisi. Suspendisse pharetra, nisl vel auctor facilisis, elit odio semper nisi, eu gravida mi neque eu urna. Vestibulum a suscipit orci. Donec posuere dolor at ante euismod facilisis. Duis vel ante pharetra, posuere velit at, pretium dolor. Integer ipsum sem, varius eu lacus lacinia, rhoncus imperdiet ante. Etiam eleifend erat nunc, vel condimentum tellus tincidunt sed. Suspendisse potenti. Curabitur quam urna, pretium at turpis eu, venenatis tincidunt metus. Quisque urna arcu, dapibus nec ante a, tristique bibendum eros."
                       "Duis sodales erat mollis quam maximus pellentesque. Duis consequat, libero vitae egestas ultricies, erat augue cursus ipsum, at efficitur tortor nulla sit amet felis. Vestibulum at ultricies orci, sed fermentum risus. Fusce dui arcu, pharetra ut felis vitae, laoreet iaculis turpis. Sed a gravida quam. Donec et semper turpis. Morbi laoreet tincidunt mi et porttitor. Integer sed consectetur tortor. Morbi in nibh neque. Pellentesque lobortis lacus in nisi rutrum, ut eleifend nisl condimentum. Praesent eget semper justo."
                       "Pellentesque semper suscipit pellentesque. Morbi vestibulum erat vitae odio iaculis bibendum. Ut vehicula, elit a luctus ultrices, dolor justo tincidunt orci, in euismod libero felis quis nunc. Morbi et mi nunc. Integer mollis est eu tellus aliquet, non luctus felis malesuada. Duis non imperdiet nibh. Quisque scelerisque enim euismod ex accumsan condimentum. Sed tincidunt pulvinar nibh ut rhoncus. Donec volutpat eros ut ligula posuere ultricies. Etiam et metus auctor massa placerat scelerisque et sed magna. Praesent congue tristique diam at scelerisque. Integer ultricies sit amet erat non fermentum. Nullam porttitor dignissim tristique. Vivamus cursus viverra semper."
                       "Integer et vehicula sem. Donec sed magna ac lectus suscipit rhoncus a vel sapien. Donec pellentesque facilisis arcu, sit amet sollicitudin purus lobortis eu. Nam turpis erat, consequat eu vestibulum quis, imperdiet id urna. Mauris nec tempus mauris. Fusce lacus felis, placerat vel ultricies ut, faucibus sit amet leo. Maecenas rutrum porttitor purus a interdum. Cras ipsum lorem, mollis ut nibh in, malesuada malesuada est. Duis eleifend id justo porta varius. Maecenas lacus ante, molestie consectetur purus eget, volutpat dignissim tellus. In id libero suscipit, bibendum arcu eu, viverra sapien. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In at nisl vel eros pellentesque eleifend eu sit amet lectus. Pellentesque commodo lacinia pharetra. Vestibulum ligula ex, feugiat a nibh ac, dapibus commodo nunc."
                       "Nunc vel leo ex. Curabitur non velit non nisi eleifend iaculis. Nunc quis libero ornare, suscipit elit eget, lobortis neque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras vulputate eget neque sed efficitur. Ut quis nibh vitae augue auctor gravida vitae sed mauris. Pellentesque a metus sit amet ex sodales bibendum commodo ac enim. Ut tristique quam tortor, eget scelerisque ipsum malesuada vitae. Praesent consectetur nunc nec ultrices tempus. Etiam lacinia est a nunc iaculis, et dictum felis aliquam. Sed eu lacinia quam, vel pretium ipsum. Duis eleifend dignissim dui, id vulputate orci gravida at. Quisque laoreet magna vitae lorem interdum, nec luctus tortor interdum. Etiam congue iaculis rutrum. Fusce euismod viverra hendrerit. Maecenas interdum risus sit amet augue euismod, in elementum libero iaculis."
                       "Donec maximus risus enim, id fermentum neque eleifend in. Praesent vehicula, dui vel rhoncus dictum, dolor nisi faucibus est, in ornare sapien mauris et ex. Ut placerat aliquam lorem, quis accumsan risus sagittis vitae. Ut eu felis in ex aliquet eleifend. Integer dapibus iaculis iaculis. Sed tellus eros, tincidunt ut dignissim a, pulvinar et lorem. Nunc sollicitudin dui nisi, a consectetur velit blandit quis. Donec augue orci, euismod in nisi id, tincidunt ornare tortor. Curabitur vitae viverra justo. Etiam sit amet tincidunt ligula."
                       "Sed sit amet quam vitae tortor pharetra auctor ut ut orci. Sed dignissim neque justo, id ultricies ante dapibus quis. Vestibulum cursus aliquam lectus, vitae auctor ex laoreet et. Fusce congue metus id nunc feugiat mollis. Nunc in quam eget sapien interdum dapibus vitae eget eros. Sed blandit est non suscipit laoreet. Integer varius arcu non aliquam pretium. Aenean tempor egestas consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla iaculis lorem mauris, eget sollicitudin velit hendrerit vitae. Donec sapien lorem, hendrerit eu nibh hendrerit, imperdiet mollis dolor. Aenean cursus neque non quam lobortis, sed imperdiet eros consectetur. Aenean semper, quam in tempor viverra, tortor dui tincidunt elit, non iaculis purus purus ut ante. Suspendisse potenti."
                       "Proin arcu lectus, tincidunt vel felis vitae, ornare imperdiet velit. Nullam eleifend a libero eu porttitor. Praesent lobortis augue in ultricies vestibulum. Fusce a aliquet mauris. Ut at laoreet est. Aliquam nisi orci, malesuada sed congue id, euismod et ex. Suspendisse potenti. Fusce maximus malesuada faucibus. Etiam eu gravida nisl. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur cursus lectus risus, pulvinar consectetur lectus scelerisque vel."
                       "Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam est eros, rutrum eu molestie vel, tristique eu dui. Nunc gravida erat justo, eget interdum ante elementum id. Maecenas fermentum placerat augue sed dapibus. Curabitur eget massa est. Fusce aliquam metus quis lectus rutrum ultrices. Sed tincidunt arcu a ipsum porttitor pharetra. Nulla sit amet nisl congue, rhoncus neque ut, cursus arcu. Quisque hendrerit lorem ac erat rutrum sodales. Integer accumsan iaculis tellus, in aliquet mi molestie non. Donec quis posuere est. Nulla at eleifend nibh. Vivamus quis tincidunt velit. Nulla a faucibus augue, ut tincidunt lectus. Morbi lacinia laoreet convallis."
                       "Aenean commodo fringilla augue, vitae consectetur nibh eleifend et. Aenean in placerat risus. Cras id lectus pharetra, congue tellus vitae, mattis velit. Nullam sit amet ipsum nec libero hendrerit tristique non vitae lorem. Nullam blandit, nisi tristique finibus volutpat, quam nisl tincidunt est, mollis blandit dolor erat quis velit. Integer sodales lorem arcu, non tristique tellus finibus ac. Vivamus erat odio, pulvinar sit amet ligula ullamcorper, pellentesque mollis nibh. Ut sollicitudin, nisl id rhoncus facilisis, velit augue sodales risus, eu posuere arcu magna vel nisl. Aenean sagittis orci magna, vitae ultricies lacus hendrerit nec. Aenean varius odio ex, ornare congue neque varius non. Duis erat mi, luctus et est et, hendrerit viverra neque. Praesent interdum nulla mi, id porta enim vestibulum quis. Pellentesque mollis erat non massa feugiat rhoncus in bibendum tellus. Phasellus in sollicitudin eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec ut diam eu quam aliquet sodales."
                       "Sed eget tincidunt elit. Sed vitae fringilla quam. Sed mauris augue, fringilla ut neque sed, tempus tincidunt libero. Mauris convallis feugiat accumsan. Duis non purus rutrum nisl metus. ";

    LOG_INFO("START OF FILE TEST");
    vTaskDelay(pdMS_TO_TICKS(1000));

    FF_FILE* file = ff_fopen("/dev/sda/testfile.txt", "rw");
    if (file == NULL)
    {
        LOG_ERROR("FAILED TO OPEN TEST FILE");
    }
    else
    {
        int bytes_written = ff_fwrite(text, sizeof(char), 8192, file);
        if (bytes_written != 8192)
        {
            LOG_ERROR("WRITTEN %lu BYTES", bytes_written);
        }
        else
        {
            vTaskDelay(pdMS_TO_TICKS(1000));

            ff_fseek(file, 0, SEEK_SET);

            int bytes_read = ff_fread(buffer, sizeof(char), 4096, file);
            if (bytes_read != 4096)
            {
                LOG_ERROR("READ INVALID NUMBER OF DATA: %lu", bytes_read);
            }
            else
            {
                for (uint32_t i = 0; i < 4096; ++i)
                {
                    if (buffer[i] != text[i])
                    {
                        LOG_ERROR("TEST TEXT MISMATCHES AT INDEX: %lu", i);
                        break;
                    }
                }
            }

            vTaskDelay(pdMS_TO_TICKS(1000));
            bytes_read = ff_fread(buffer, sizeof(char), 4096, file);
            if (bytes_read != 4096)
            {
                LOG_ERROR("READ INVALID NUMBER OF DATA: %lu", bytes_read);
            }
            else
            {
                for (uint32_t i = 0; i < 4096; ++i)
                {
                    if (buffer[i] != text[i + 4096])
                    {
                        LOG_ERROR("TEST TEXT MISMATCHES AT INDEX: %lu", i);
                        break;
                    }
                }
            }

            ff_fclose(file);
        }

    }
}
#endif
