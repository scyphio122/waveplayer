target_include_directories(${CMAKE_PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_LIST_DIR})

include(${CMAKE_CURRENT_LIST_DIR}/AudioPlayer/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/CS43L22/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/USB_Device/CMakeLists.txt)