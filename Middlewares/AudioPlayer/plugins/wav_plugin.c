/*
 * wav_plugin.c
 *
 *  Created on: Feb 17, 2020
 *      Author: konrad
 */

#include <wav_plugin.h>
#include <audio_plugin.h>

#include <log.h>

#include <FreeRTOS.h>
#include <ff_stdio.h>

#define LOG_VERBOSE                     1

#define WAVE_FILE_HEADER_SIZE           44u

static  bool s_wave_player_try_file(FF_FILE* file, audio_file_info_t* file_info);

static  int s_wave_player_read_samples(uint8_t* buffer, uint32_t buffer_size, uint32_t* samples_read);

static  int s_wave_player_get_current_time(uint32_t* minutes, uint32_t* seconds);

static  int s_wave_player_set_current_time(uint32_t minutes, uint32_t seconds);

static  int s_wave_player_close_file(void);

static int s_wave_player_parse_chunk_id(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info);

static int s_wave_player_parse_chunk_size(uint8_t* header, uint32_t header_size, uint32_t file_size, audio_file_info_t* file_info);

static int s_wave_player_parse_format(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info);

static int s_wave_player_parse_subchunk_1_id(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info);

static int s_wave_player_parse_subchunk_1_size(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info);

static int s_wave_player_parse_audio_format(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info);

static int s_wave_player_parse_num_of_channels(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info);

static int s_wave_player_parse_sample_rate(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info);

static int s_wave_player_parse_byte_rate(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info);

static int s_wave_player_parse_block_align(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info);

static int s_wave_player_parse_bits_per_sample(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info);

static int s_wave_player_parse_subchunk_2_id(uint8_t* header, uint32_t header_size, int data_index, audio_file_info_t* file_info);

static int s_wave_player_parse_subchunk_2_size(uint8_t* header, uint32_t header_size, int data_index, audio_file_info_t* file_info);

static int s_wave_player_validate_file_info(audio_file_info_t* file_info);

static int s_wave_player_find_data_chunk(uint8_t* header, uint32_t header_size);

const audio_plugin_t        s_wav_plugin_info = {
    .plugin_name                =   "WAV_PLUGIN",
    .type                       =   E_WAVE_FILE,
    .try_file_cb                =   s_wave_player_try_file,
    .read_next_samples_cb       =   s_wave_player_read_samples,
    .get_current_song_time_cb   =   s_wave_player_get_current_time,
    .set_current_time_cb        =   s_wave_player_set_current_time,
    .close_file_cb              =   s_wave_player_close_file
};

static FF_FILE*             s_opened_file;

static audio_file_info_t    s_opened_file_info;

static  bool s_wave_player_try_file(FF_FILE* file, audio_file_info_t* file_info)
{
    bool    succeeded = false;
    int retval = 0;
    uint32_t file_size = 0;
    int data_index = 0;
    uint8_t header[256];

    if (file == NULL)
    {
        LOG_ERROR("NULL FILE PTR PROVIDED");
        succeeded = false;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PROVIDED");
        succeeded = false;
    }

    if (retval == 0)
    {
        memset(header, 0, sizeof(header));

        /* Rewind the file to the beginning */
        retval = ff_fseek(file, 0, SEEK_SET);
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO REWIND THE FILE");
            succeeded = false;
        }

        if (retval == 0)
        {
            retval = ff_fread(header, sizeof(uint8_t), sizeof(header), file);
            if (retval != sizeof(header))
            {
                LOG_ERROR("FAILED TO READ FILE HEADER");
                succeeded = false;
                retval = -1;
            }
            else
            {
                retval = 0;
            }
        }

        if (retval == 0)
        {
            file_size = ff_filelength(file);
            if (file_size == 0)
            {
                LOG_ERROR("FAILED TO GET THE FILE SIZE");
                retval = -1;
                succeeded = false;
            }
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_chunk_id(header, sizeof(header), file_info);
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_chunk_size(header, sizeof(header), file_size, file_info);
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_format(header, sizeof(header), file_info);
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_subchunk_1_id(header, sizeof(header), file_info);
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_subchunk_1_size(header, sizeof(header), file_info);
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_audio_format(header, sizeof(header), file_info);
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_num_of_channels(header, sizeof(header), file_info);
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_sample_rate(header, sizeof(header), file_info);
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_byte_rate(header, sizeof(header), file_info);
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_block_align(header, sizeof(header), file_info);
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_bits_per_sample(header, sizeof(header), file_info);
        }

        if (retval == 0)
        {
            data_index = s_wave_player_find_data_chunk(header, sizeof(header));
            if (data_index < 0)
            {
                LOG_ERROR("FAILED TO FIND \"data\" subchunk");
                retval = -1;
            }
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_subchunk_2_id(header, sizeof(header), data_index, file_info);
        }

        if (retval == 0)
        {
            retval = s_wave_player_parse_subchunk_2_size(header, sizeof(header), data_index, file_info);
        }

        if (retval == 0)
        {
            retval = s_wave_player_validate_file_info(file_info);
        }

        if (retval == 0)
        {
            file_info->file_type = E_WAVE_FILE;
            succeeded = true;

            retval = ff_fseek(file, file_info->samples_start_offset, SEEK_SET);
            if (retval != 0)
            {
                LOG_ERROR("FAILED TO SEEK FILE TO THE START OF SAMPLES OFFSET: %lu", file_info->samples_start_offset);
                succeeded = false;
                retval = -1;
            }
            else
            {
                s_opened_file = file;
                s_opened_file_info = *file_info;
            }
        }
    }
    else
    {
        succeeded = false;
    }

#if LOG_VERBOSE == 1

    if (succeeded == true)
    {
        LOG_DEBUG("THE FILE IS WAVE AUDIO FILE");
    }
    else
    {
        LOG_DEBUG("THE FILE IS NOT WAVE AUDIO FILE");
    }

#endif

    return succeeded;
}

static  int s_wave_player_read_samples(uint8_t* buffer, uint32_t buffer_size, uint32_t* samples_read)
{
    int retval = 0;

    if (s_opened_file == NULL)
    {
        LOG_ERROR("THE WAV FILE NOT CHOSEN");
        retval = -1;
    }

    if (buffer == NULL)
    {
        LOG_ERROR("NULL SAMPLE BUFFER PROVIDED");
        retval = -1;
    }

    if (samples_read == 0)
    {
        LOG_ERROR("NULL SAMPLES BUFFER PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        /* Clear the buffer */
        memset(buffer, 0, buffer_size);
        /* Read the audio samples */
        int32_t bytes_read = ff_fread(buffer, sizeof(uint8_t), buffer_size, s_opened_file);

        if (bytes_read > 0)
        {
            *samples_read = bytes_read / (s_opened_file_info.bits_per_sample / 8);
        }
        else
        {
            *samples_read = 0;
            retval = -1;
        }
    }

    return retval;
}

static  int s_wave_player_get_current_time(uint32_t* minutes, uint32_t* seconds)
{
    int retval = 0;


    return retval;
}

static  int s_wave_player_set_current_time(uint32_t minutes, uint32_t seconds)
{
    int retval = 0;


    return retval;
}

static  int s_wave_player_close_file(void)
{
    int retval = 0;

    s_opened_file = NULL;
    memset(&s_opened_file_info, 0, sizeof(audio_file_info_t));

    return retval;
}

static int s_wave_player_parse_chunk_id(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        /* "RIFF" in big endian */
        if (memcmp(header, "RIFF", sizeof(uint32_t)) != 0)
        {
#if LOG_VERBOSE == 1
            LOG_TRACE("CHUNK ID NOT SET TO \"RIFF\"");
            retval = -1;
#endif
        }
    }

    return retval;
}

static int s_wave_player_parse_chunk_size(uint8_t* header, uint32_t header_size, uint32_t file_size, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        uint32_t chunk_size = ((uint32_t)header[4] << 0 )  |
                              ((uint32_t)header[5] << 8 )  |
                              ((uint32_t)header[6] << 16)  |
                              ((uint32_t)header[7] << 24);

        if ((file_size - 8) != chunk_size)
        {
#if LOG_VERBOSE == 1
            LOG_TRACE("INVALID CHUNK SIZE: %lu. FILE LENGTH: %lu", chunk_size, file_size);
            retval = -1;
#endif
        }
    }

    return retval;
}

static int s_wave_player_parse_format(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        /* "WAVE" in big endian */
        if (memcmp(&header[8], "WAVE", sizeof(uint32_t)) != 0)
        {
#if LOG_VERBOSE == 1
            LOG_TRACE("FORMAT NOT SET TO \"WAVE\"");
            retval = -1;
#endif
        }
    }

    return retval;
}

static int s_wave_player_parse_subchunk_1_id(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        /* "fmt " in big endian */
        if (memcmp(&header[12], "fmt ", sizeof(uint32_t)) != 0)
        {
#if LOG_VERBOSE == 1
            LOG_TRACE("SUBCHUNK1_ID NOT SET TO \"fmt \"");
            retval = -1;
#endif
        }
    }

    return retval;
}

static int s_wave_player_parse_subchunk_1_size(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        uint32_t subchunk_1_size = 0;
        memcpy(&subchunk_1_size, &header[16], sizeof(uint32_t));
        if (subchunk_1_size != 16)
        {
#if LOG_VERBOSE == 1
            LOG_TRACE("SUBCHUNK1_SIZE NOT SET TO 16 BITS");
            retval = -1;
#endif
        }
    }

    return retval;
}

static int s_wave_player_parse_audio_format(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        if (retval == 0)
        {
            uint16_t audio_format = 0;
            memcpy(&audio_format, &header[20], sizeof(uint16_t));
            if (audio_format != 1)
            {
#if LOG_VERBOSE == 1
                LOG_TRACE("AUDIO FORMAT NOT SET TO 1 - PCM");
                retval = -1;
#endif
            }
        }
    }

    return retval;
}

static int s_wave_player_parse_num_of_channels(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        uint16_t num_of_channels = 0;
        memcpy(&num_of_channels, &header[22], sizeof(uint16_t));
        file_info->number_of_channels = (uint8_t)num_of_channels;

#if LOG_VERBOSE == 1
        LOG_TRACE("WAVE FILE NUMBER OF CHANNELS: %u", num_of_channels);
#endif
    }

    return retval;
}

static int s_wave_player_parse_sample_rate(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        uint32_t sample_rate = 0;
        memcpy(&sample_rate, &header[24], sizeof(uint32_t));

        switch (sample_rate)
        {
            case 8000:
            {
#if LOG_VERBOSE == 1
                LOG_TRACE("WAVE FILE SAMPLE_RATE: %luHz", sample_rate);
#endif
                file_info->sample_rate = E_SAMPLE_RATE_8000Hz;
            } break;

            case 22050:
            {
#if LOG_VERBOSE == 1
                LOG_TRACE("WAVE FILE SAMPLE_RATE: %luHz", sample_rate);
#endif
                file_info->sample_rate = E_SAMPLE_RATE_22050Hz;
            } break;

            case 24000:
            {
#if LOG_VERBOSE == 1
                LOG_TRACE("WAVE FILE SAMPLE_RATE: %luHz", sample_rate);
#endif
                file_info->sample_rate = E_SAMPLE_RATE_24000Hz;
            } break;

            case 44100:
            {
#if LOG_VERBOSE == 1
                LOG_TRACE("WAVE FILE SAMPLE_RATE: %luHz", sample_rate);
#endif
                file_info->sample_rate = E_SAMPLE_RATE_44100Hz;
            } break;

            case 48000:
            {
#if LOG_VERBOSE == 1
                LOG_TRACE("WAVE FILE SAMPLE_RATE: %luHz", sample_rate);
#endif
                file_info->sample_rate = E_SAMPLE_RATE_48000Hz;
            } break;

            default:
            {
#if LOG_VERBOSE == 1
                LOG_ERROR("UNSUPPORTED WAVE FILE SAMPLE_RATE: %luHz", sample_rate);
#endif
                retval = -1;
            }
        }
    }

    return retval;
}

static int s_wave_player_parse_byte_rate(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        uint32_t byte_rate = 0;
        memcpy(&byte_rate, &header[28], sizeof(uint32_t));

        file_info->byte_rate = byte_rate;

#if LOG_VERBOSE == 1
        LOG_TRACE("WAVE FILE BYTE RATE: %u", byte_rate);
#endif
    }

    return retval;
}

static int s_wave_player_parse_block_align(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        uint16_t block_align = 0;
        memcpy(&block_align, &header[32], sizeof(uint16_t));

        file_info->block_alignement = (uint8_t)block_align;

#if LOG_VERBOSE == 1
        LOG_TRACE("WAVE FILE BLOCK ALIGN: %u", block_align);
#endif
    }

    return retval;
}

static int s_wave_player_parse_bits_per_sample(uint8_t* header, uint32_t header_size, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        uint16_t bits_per_sample = 0;
        memcpy(&bits_per_sample, &header[34], sizeof(uint16_t));

        file_info->bits_per_sample = (uint8_t)bits_per_sample;

#if LOG_VERBOSE == 1
        LOG_TRACE("WAVE FILE BITS PER SAMPLE: %u", bits_per_sample);
#endif
    }

    return retval;
}

static int s_wave_player_parse_subchunk_2_id(uint8_t* header, uint32_t header_size, int data_index, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        /* \"data\" in big-endian */
        if (memcmp(&header[data_index], "data", sizeof(uint32_t)) != 0)
        {
            LOG_ERROR("INVALID SUBCHUNK_2_ID FIELD");
            retval = -1;
        }
    }

    return retval;
}

static int s_wave_player_parse_subchunk_2_size(uint8_t* header, uint32_t header_size, int data_index, audio_file_info_t* file_info)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PTR PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        uint32_t subchunk_2_size = 0;
        memcpy(&subchunk_2_size, &header[data_index + 4], sizeof(uint32_t));

        file_info->sample_data_size = subchunk_2_size;
        file_info->samples_start_offset = data_index + 8;

#if LOG_VERBOSE == 1
        LOG_TRACE("WAVE FILE SIZE: %lu", subchunk_2_size);
#endif
    }

    return retval;
}

static int s_wave_player_validate_file_info(audio_file_info_t* file_info)
{
    int retval = 0;

    if (file_info == NULL)
    {
        LOG_ERROR("NULL FILE INFO STRUCTURE PROVIDED");
        retval = -1;
    }

    if (retval == 0)
    {
        uint32_t byte_rate_calc = (uint32_t)file_info->sample_rate * file_info->number_of_channels * (file_info->bits_per_sample / 8);
        if (byte_rate_calc != file_info->byte_rate)
        {
            LOG_ERROR("CALCULATED BYTE RATE [%lu] MISMATCHES THE READ ONE [%lu]", byte_rate_calc, file_info->byte_rate);
            retval = -1;
        }
    }

    if (retval == 0)
    {
        uint32_t block_align = file_info->number_of_channels * file_info->bits_per_sample / 8;
        if (block_align != file_info->block_alignement)
        {
            LOG_ERROR("CALCULATED BLOCK ALIGN [%lu] MISMATCHES THE READ ONE [%lu]", block_align, file_info->block_alignement);
            retval = -1;
        }
    }

    return retval;
}

static int s_wave_player_find_data_chunk(uint8_t* header, uint32_t header_size)
{
    int retval = 0;

    if (header == NULL)
    {
        LOG_ERROR("NULL HEADER PTR PROVIDED");
        retval = -1;
    }

    if (header_size < WAVE_FILE_HEADER_SIZE)
    {
        LOG_ERROR("INVALID HEADER SIZE. EXPECTED %lu BYTES, GOT: %lu", WAVE_FILE_HEADER_SIZE, header_size);
        retval = -1;
    }

    if (header_size < 4)
    {
        LOG_ERROR("INVALID HEADER SIZE");
        retval = -1;
    }

    if (retval == 0)
    {
        uint8_t* tmp = header + 3;

        uint8_t* end_ptr = header + header_size;

        while (tmp != end_ptr)
        {
            if (
                (*(tmp - 3) == 'd') &&
                (*(tmp - 2) == 'a') &&
                (*(tmp - 1) == 't') &&
                (*(tmp - 0) == 'a')
               )
            {
                break;
            }

            retval++;
            tmp++;
        }
    }

    return retval;
}

