/*
 * wav_plugin.h
 *
 *  Created on: Feb 17, 2020
 *      Author: konrad
 */

#ifndef MIDDLEWARES_AUDIOPLAYER_PLUGINS_WAV_PLUGIN_H_
#define MIDDLEWARES_AUDIOPLAYER_PLUGINS_WAV_PLUGIN_H_

#include <audio_plugin.h>

extern const audio_plugin_t        s_wav_plugin_info;

#endif /* MIDDLEWARES_AUDIOPLAYER_PLUGINS_WAV_PLUGIN_H_ */
