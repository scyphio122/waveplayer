/*
 * audio_plugin.h
 *
 *  Created on: Feb 17, 2020
 *      Author: konrad
 */

#ifndef MIDDLEWARES_AUDIOPLAYER_AUDIO_PLUGIN_H_
#define MIDDLEWARES_AUDIOPLAYER_AUDIO_PLUGIN_H_

#include <stdint-gcc.h>
#include <stdbool.h>

#include <FreeRTOS.h>
#include <ff_stdio.h>

typedef enum
{
    E_WAVE_FILE,
    E_MP3_FILE
} audio_plugin_type_e;

typedef enum
{
    E_SAMPLE_8_BIT      =   8,
    E_SAMPLE_16_BIT     =   16,
} audio_plugin_bits_per_sample_e;

typedef enum
{
    E_SAMPLE_RATE_VARIABLE      =   0,
    E_SAMPLE_RATE_8000Hz        =   8000,
    E_SAMPLE_RATE_22050Hz       =   22050,
    E_SAMPLE_RATE_24000Hz       =   24000,
    E_SAMPLE_RATE_44100Hz       =   44100,
    E_SAMPLE_RATE_48000Hz       =   48000,
} audio_plugin_sample_rate_e;

typedef struct
{
    audio_plugin_type_e             file_type;

    uint8_t                         number_of_channels;

    uint32_t                        byte_rate;

    uint8_t                         block_alignement;           /*< Number of bytes per sample including all of the channels */

    audio_plugin_bits_per_sample_e  bits_per_sample;

    audio_plugin_sample_rate_e      sample_rate;

    uint32_t                        sample_data_size;

    uint32_t                        samples_start_offset;
} audio_file_info_t;

typedef bool (*audio_plugin_try_file_f)(FF_FILE* file, audio_file_info_t* file_info);

typedef int (*audio_plugin_read_samples_f)(uint8_t* buffer, uint32_t buffer_size, uint32_t* samples_read);

typedef int (*audio_plugin_get_current_time_f)(uint32_t* minutes, uint32_t* seconds);

typedef int (*audio_plugin_set_current_time_f)(uint32_t minutes, uint32_t seconds);

typedef int (*audio_plugin_close_file_f)(void);

typedef struct
{
    const char*                         plugin_name;

    audio_plugin_type_e                 type;

    audio_plugin_try_file_f             try_file_cb;

    audio_plugin_read_samples_f         read_next_samples_cb;

    audio_plugin_get_current_time_f     get_current_song_time_cb;

    audio_plugin_set_current_time_f     set_current_time_cb;

    audio_plugin_close_file_f           close_file_cb;

} audio_plugin_t;

#endif /* MIDDLEWARES_AUDIOPLAYER_AUDIO_PLUGIN_H_ */
