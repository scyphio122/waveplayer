/*
 * cs43l22_dac.c
 *
 *  Created on: Feb 19, 2020
 *      Author: konrad
 */

#include <cs43l22_dac.h>
#include <bsp_types.h>
#include <driver_manager.h>
#include <bsp_peripheral_mapping.h>

#include <gpio.h>
#include <bsp_i2c.h>
#include <log.h>

#include <FreeRTOS.h>
#include <task.h>

#define LOG_VERBOSE                         1

#define CS43L22_I2C_ADDRESS                 0x94

#define VOLUME_CONVERT(Volume)    (((Volume) > 100)? 255:((uint8_t)(((Volume) * 255) / 100)))

#define CS43L22_ID_REG_ADDRESS              0x01
#define CS43L22_POWER_CTL_1                 0x02
#define CS43L22_POWER_CTL_2                 0x04
#define CS43L22_CLOCKING_CTL                0x05
#define CS43L22_INTERFACE_CTL_1             0x06
#define CS43L22_INTERFACE_CTL_2             0x07
#define CS43L22_PASSTHROUGH_A_SELECT        0x08
#define CS43L22_PASSTHROUGH_B_SELECT        0x09
#define CS43L22_ANALOG_ZC_SR_SETTINGS       0x0A
#define CS43L22_PASSTHROUGH_GANG_CONTROL    0x0C
#define CS43L22_PLAYBACK_CTL_1              0x0D
#define CS43L22_MISC_CTL                    0x0E
#define CS43L22_PLAYBACK_CTL_2              0x0F
#define CS43L22_PASSTHROUGH_A_VOL           0x14
#define CS43L22_PASSTHROUGH_B_VOL           0x15
#define CS43L22_PCMA_VOL                    0x1A
#define CS43L22_PCMB_VOL                    0x1B
#define CS43L22_BEEP_FREQ_ON_TIME           0x1C
#define CS43L22_BEEP_VOL_OFF_TIME           0x1D
#define CS43L22_BEEP_TONE_CFG               0x1E
#define CS43L22_TONE_CTL                    0x1F
#define CS43L22_MASTER_A_VOL                0x20
#define CS43L22_MASTER_B_VOL                0x21
#define CS43L22_HEADPHONE_A_VOL             0x22
#define CS43L22_HEADPHONE_B_VOL             0x23
#define CS43L22_SPEAKER_A_VOL               0x24
#define CS43L22_SPEAKER_B_VOL               0x25
#define CS43L22_CHANNEL_MIXER_AND_SWAP      0x26
#define CS43L22_LIMIT_CTL1_THRESHOLDS       0x27
#define CS43L22_LIMIT_CTL2_RELEASE_RATE     0x28
#define CS43L22_LIMITER_ATTACK_RATE         0x29
#define CS43L22_OVERFLOW_AND_CLOCK_STAT     0x2E
#define CS43L22_BATTERY_COMPENSANTION       0x2F
#define CS43L22_VP_BATT_LEVEL               0x30
#define CS43L22_SPEAKER_STATUS              0x31
#define CS43L22_CHARGE_PUMP_FREQ            0x34

typedef enum
{
    C43L22_RESET_HELD,
    C43L22_RESET_RELEASED
} cs43l22_reset_state_e;

typedef struct
{
    cs43l22_reset_state_e       reset_state;
    int                         reset_pin_fd;
    int                         i2c_fd;
    int                         sai_fd;

} cs43l22_status_t;

static bsp_status_e s_cs43l22_reset_release(void);

static bsp_status_e s_cs43l22_write_register(uint8_t reg_address, uint8_t value);

static bsp_status_e s_cs43l22_read_register(uint8_t reg_address, uint8_t* value);

static uint8_t      s_cs43l22_calculate_master_volume(uint8_t perc);

static uint8_t      s_cs43l22_calculate_output_volume(uint8_t perc);

static cs43l22_status_t     s_cs43l22_status;

#if 0
static const uint8_t        s_cs43l22_clock_ctrl_vals[CS43L22_SAMPLE_RATE_CNT] = {
    /* 8000Hz  */ 0b01110010,
    /* 11025Hz */ 0b01100110,
    /* 12000Hz */ 0b01100010,
    /* 16000Hz */ 0b01010010,
    /* 22050Hz */ 0b01000110,
    /* 24000Hz */ 0b01000010,
    /* 32000Hz */ 0b00110010,
    /* 44100Hz */ 0b00100110,
    /* 48000Hz */ 0b00100010,
    /* 88235Hz */ 0b00000110,
    /* 96000Hz */ 0b00000010
};
#endif

bsp_status_e cs43l22_initialize(void)
{
    bsp_status_e retval = BSP_SUCCESS;

    memset(&s_cs43l22_status, 0, sizeof(s_cs43l22_status));

    if (retval == BSP_SUCCESS)
    {
        s_cs43l22_status.reset_pin_fd = driver_open(CS43L22_RESET_PORT_NAME, NULL);
        if (s_cs43l22_status.reset_pin_fd < 0)
        {
            LOG_ERROR("FAILED TO OPEN CS43L22 RESET PIN");
            retval = BSP_INTERNAL_ERROR;
        }
        else
        {
            bsp_ioctl_args_t args;
            bsp_iop_t iop;
            iop.flags = (CS43L22_RESET_PIN | GPIO_NO_PULL | GPIO_SPEED_CONFIG_LOW);
            iop.data0 = GPIO_MODE_OUTPUT_PP;

            args.command = GPIO_SET_MODE_COMMAND;
            args.iop = &iop;

            int ret = driver_ioctl(s_cs43l22_status.reset_pin_fd, &args);
            if (ret != 0)
            {
                LOG_ERROR("FAILED TO CONFIGURE CS43L22 RESET PIN AS OUTPUT");
                retval = BSP_INTERNAL_ERROR;
            }
        }
    }

    if (retval == BSP_SUCCESS)
    {
        s_cs43l22_status.i2c_fd = driver_open(I2C_AUDIO_DRIVER_NAME, NULL);
        if (s_cs43l22_status.i2c_fd < 0)
        {
            LOG_ERROR("FAILED TO OPEN CS43L22 I2C");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_reset_release();
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO RELEASE THE CHIP FROM RESET");
        }

        retval = cs43l22_power_down();
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO POWER DOWN THE CHIP");
        }

        retval = cs43l22_assert_chip_id();
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO ASSERT CHIP ID");
        }

        retval = cs43l22_set_output_enabled(CS43L22_ALWAYS_OFF, CS43L22_ALWAYS_OFF, CS43L22_ALWAYS_OFF, CS43L22_ALWAYS_OFF);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO CONFIGURE THE OUTPUT INTERFACE");
        }

        retval = cs43l22_set_interface_type(
                    CS43L22_MODE_SLAVE,
                    CS43L22_CLOCK_POL_NOT_INVERTED,
                    CS43L22_DSP_MODE_DISABLED,
                    CS43L22_INTERFACE_I2S,
                    CS43L22_WORD_LEN_RJ_16bit
                );
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO CONFIGURE THE INTERFACE TYPE");
        }

        retval = cs43l22_set_sample_rate(CS43L22_SAMPLE_RATE_22050Hz);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO CONFIGURE THE SAMPLE RATE");
        }

        retval = cs43l22_set_passthrough_sources(CS43L22_PASSTHROUGH_SRC_NONE, CS43L22_PASSTHROUGH_SRC_NONE);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SET THE ANALOG PASSTHROUGH SOURCES");
        }

        retval = cs43l22_set_audio_mode(CS43L22_AUDIO_INPUT_MODE_I2S);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SET THE AUDIO MODE TO I2S");
        }

        retval = cs43l22_set_master_volume(75);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP MASTER VOLUME");
        }

        retval = cs43l22_set_headphone_volume(100);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP HEADPHONE VOLUME");
        }

        retval = cs43l22_set_speaker_volume(0);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP SPEAKER VOLUME");
        }

        if (retval == BSP_SUCCESS)
        {
            retval = cs43l22_unmute_output();
            if (retval != BSP_SUCCESS)
            {
                LOG_ERROR("FAILED TO UNMUTE THE OUTPUT");
            }
        }

        if (retval == BSP_SUCCESS)
        {
            retval = cs43l22_set_pcm_volume(95);
            if (retval != BSP_SUCCESS)
            {
                LOG_ERROR("FAILED TO SETUP PCM VOLUME");
            }
        }

        if (retval == BSP_SUCCESS)
        {
            retval = cs43l22_set_passthrough_volume(0);
            if (retval != BSP_SUCCESS)
            {
                LOG_ERROR("FAILED TO SETUP PASSTHROUGH VOLUME");
            }
        }

        if (retval == BSP_SUCCESS)
        {
            retval = cs43l22_power_up();
            if (retval != BSP_SUCCESS)
            {
                LOG_ERROR("FAILED TO POWER UP THE CS43L22");
            }
        }

        if (retval == BSP_SUCCESS)
        {
            retval = s_cs43l22_write_register(0x00, 0x99);
            if (retval != BSP_SUCCESS)
            {
                LOG_ERROR("FAILED TO WRITE 0x00 TO REGISTER 0x99");
            }
        }

        if (retval == BSP_SUCCESS)
        {
            retval = s_cs43l22_write_register(0x47, 0x80);
            if (retval != BSP_SUCCESS)
            {
                LOG_ERROR("FAILED TO WRITE 0x80 TO REGISTER 0x47");
            }
        }

        if (retval == BSP_SUCCESS)
        {
            uint8_t val = 0;
            retval = s_cs43l22_read_register(0x32, &val);
            if (retval != BSP_SUCCESS)
            {
                LOG_ERROR("FAILED TO READ REG 0x32");
            }
            else
            {
                val |= 0x80;
                retval = s_cs43l22_write_register(0x32, val);
                if (retval != BSP_SUCCESS)
                {
                    LOG_ERROR("FAILED TO WRITE THE REGISTER 0x32");
                }
            }
        }

        if (retval == BSP_SUCCESS)
        {
            uint8_t val = 0;
            retval = s_cs43l22_read_register(0x32, &val);
            if (retval != BSP_SUCCESS)
            {
                LOG_ERROR("FAILED TO READ REG 0x32");
            }
            else
            {
                val &= ~(0x80);
                retval = s_cs43l22_write_register(0x32, val);
                if (retval != BSP_SUCCESS)
                {
                    LOG_ERROR("FAILED TO WRITE THE REGISTER 0x32");
                }
            }
        }

        if (retval == BSP_SUCCESS)
        {
            retval = s_cs43l22_write_register(0x00, 0x00);
            if (retval != BSP_SUCCESS)
            {
                LOG_ERROR("FAILED TO WRITE 0x00 TO REGISTER 0x00");
            }
        }
    }

#if 0
    retval = cs43l22_beep_set_enabled(true);
#endif

    return retval;
}

bsp_status_e cs43l22_assert_chip_id(void)
{
    bsp_status_e retval = BSP_SUCCESS;
    uint8_t value = 0;

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_read_register(CS43L22_ID_REG_ADDRESS, &value);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO READ CS43L22 CHIP ID REGISTER");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        if ((value >> 3) != 0x1C)
        {
            LOG_ERROR("INVALID CHIP ID: 0x%02X", (value >> 3));
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        switch (value & 0x07)
        {
            case 0:
            {
#if LOG_VERBOSE == 1
                LOG_DEBUG("CS43L22 REVISION: A0");
#endif
            } break;

            case 1:
            {
#if LOG_VERBOSE == 1
                LOG_DEBUG("CS43L22 REVISION: A1");
#endif
            } break;

            case 2:
            {
#if LOG_VERBOSE == 1
                LOG_DEBUG("CS43L22 REVISION: B0");
#endif
            } break;

            case 3:
            {
#if LOG_VERBOSE == 1
                LOG_DEBUG("CS43L22 REVISION: B1");
#endif
            } break;

            default:
            {
                LOG_ERROR("INVALID CS43L22 ID: %hu", value & 0x07);
                retval = BSP_INTERNAL_ERROR;
            }
        }
    }

    return retval;
}

bsp_status_e cs43l22_power_up(void)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_POWER_CTL_1, 0x9E);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP CS43L22_POWER_CTL1 REGISTER");
        }
    }

    return retval;
}

bsp_status_e cs43l22_power_down(void)
{
    bsp_status_e retval = BSP_SUCCESS;

    LOG_WARN("CS43L22 POWER DOWN NOT IMPLEMENTED");

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_POWER_CTL_1, 0x01);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP CS43L22_POWER_CTL1 REGISTER");
        }
    }

    return retval;
}

bsp_status_e cs43l22_set_interface_type(
        cs43l22_audio_mode_e        mode,
        cs43l22_clock_polarity_e    polarity,
        cs43l22_dsp_mode_e          dsp_mode,
        cs43l22_audio_if_type_e     if_type,
        cs43l22_audio_word_length_e audio_word_length
)
{
    bsp_status_e retval = BSP_SUCCESS;
    uint8_t interface_ctrl_reg  =   0;

    if (mode >= CS43L22_MODE_CNT)
    {
        LOG_ERROR("INVALID MODE");
        retval = BSP_INVALID_ARGS;
    }

    if (polarity >= CS43L22_CLOCK_POL_CNT)
    {
        LOG_ERROR("INVALID CLOCK POLARITY");
        retval = BSP_INVALID_ARGS;
    }

    if (dsp_mode >= CS43L22_DSP_MODE_CNT)
    {
        LOG_ERROR("INVALID DSP MODE");
        retval = BSP_INVALID_ARGS;
    }

    if (if_type >= CS43L22_INTERFACE_CNT)
    {
        LOG_ERROR("INVALID INTERFACE TYPE");
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        int ret = s_cs43l22_read_register(CS43L22_INTERFACE_CTL_1, &interface_ctrl_reg);
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO READ INTERFACE CTRL REG");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        interface_ctrl_reg &= (1 << 5);         /* Clear all bits except for the reserved one */
        interface_ctrl_reg |= (mode     << 7);
        interface_ctrl_reg |= (polarity << 6);
        interface_ctrl_reg |= (dsp_mode << 4);
        interface_ctrl_reg |= (if_type  << 2);

        if (dsp_mode == CS43L22_DSP_MODE_ENABLED)
        {
            interface_ctrl_reg |= ((audio_word_length - 1) >> 4) & 0x03;
        }
        else
        {
            interface_ctrl_reg |= ((audio_word_length - 1) >> 0) & 0x03;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        int ret = s_cs43l22_write_register(CS43L22_INTERFACE_CTL_1, interface_ctrl_reg);
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO WRITE INTERFACE CTRL 1 REG");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    return retval;
}

bsp_status_e cs43l22_set_passthrough_sources(
        cs43l22_passthrough_src_e passthrough_a,
        cs43l22_passthrough_src_e passthrough_b
)
{
    bsp_status_e retval = BSP_SUCCESS;

    uint8_t val_a      =   0;
    uint8_t val_b      =   0;

    if (retval == BSP_SUCCESS)
    {
        int ret = s_cs43l22_read_register(CS43L22_PASSTHROUGH_A_SELECT, &val_a);
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO READ PASSTHROUGH_A_SELECT REG");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        int ret = s_cs43l22_read_register(CS43L22_PASSTHROUGH_B_SELECT, &val_b);
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO READ PASSTHROUGH_B_SELECT REG");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        val_a &= 0xF0;                      /* Keep the reserved bits */
        val_a |= (passthrough_a & 0x0F);

        int ret = s_cs43l22_write_register(CS43L22_PASSTHROUGH_A_SELECT, val_a);
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO WRITE PASSTHROUGH_A_SELECT REG");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        val_b &= 0xF0;                      /* Keep the reserved bits */
        val_b |= (passthrough_b & 0x0F);

        int ret = s_cs43l22_write_register(CS43L22_PASSTHROUGH_B_SELECT, val_b);
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO WRITE PASSTHROUGH_B_SELECT REG");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    return retval;
}

bsp_status_e cs43l22_set_audio_mode(cs43l22_audio_input_mode_e audio_mode)
{
    bsp_status_e retval = BSP_SUCCESS;
    uint8_t val = 0;

    if (retval == BSP_SUCCESS)
    {
        int ret = s_cs43l22_read_register(CS43L22_MISC_CTL, &val);
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO READ MISCELLANEOUS REG");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        if (audio_mode == CS43L22_AUDIO_INPUT_MODE_ANALOG)
        {
            val |=  (1 << 7);   // Enable passthrough for AIN-A
            val |=  (1 << 6);   // Enable passthrough for AIN-B
            val &= ~(1 << 5);   // Unmute passthrough on AIN-A
            val &= ~(1 << 4);   // Unmute passthrough on AIN-B
            val &= ~(1 << 3);   // Changed settings take affect immediately
        }
        else
        {
            val = 0x36;
        }

        int ret = s_cs43l22_write_register(CS43L22_MISC_CTL, val);
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO WRITE MISCELLANEOUS REG");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    return retval;
}

bsp_status_e cs43l22_set_sample_rate(cs43l22_sample_rate_e sample_rate)
{
    bsp_status_e retval = BSP_SUCCESS;

    uint8_t clock_ctrl_reg      =   0;

    if (s_cs43l22_status.reset_state == C43L22_RESET_HELD)
    {
        LOG_ERROR("CS43L22 IS HELD RESET. CANNOT WRITE REGISTER");
        retval = BSP_INVALID_STATE;
    }

    if (sample_rate >= CS43L22_SAMPLE_RATE_CNT)
    {
        LOG_ERROR("CS43L22 SAMPLE RATE OUT OF RANGE: %lu", sample_rate);
        retval = BSP_INVALID_ARGS;
    }

    if (retval == BSP_SUCCESS)
    {
        clock_ctrl_reg = 0x80;
        int ret = s_cs43l22_write_register(CS43L22_CLOCKING_CTL, clock_ctrl_reg);
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO WRITE CLOCK CTRL REG");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    return retval;
}

bsp_status_e cs43l22_beep_set_enabled(bool enabled)
{
    bsp_status_e retval = BSP_SUCCESS;
    uint8_t beep_and_tone_cfg = 0;

    if (retval == BSP_SUCCESS)
    {
        int ret = s_cs43l22_read_register(CS43L22_BEEP_TONE_CFG, &beep_and_tone_cfg);
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO READ BEEP TONE REG");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        if (enabled)
        {
            beep_and_tone_cfg &= 0x3F;
            beep_and_tone_cfg |= 0xC0;
        }
        else
        {
            beep_and_tone_cfg &= 0x3F;
        }

        int ret = s_cs43l22_write_register(CS43L22_BEEP_TONE_CFG, beep_and_tone_cfg);
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO WRITE BEEP TONE REG");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    return retval;
}

bsp_status_e cs43l22_set_master_volume(uint8_t percent)
{
    bsp_status_e retval = BSP_SUCCESS;

    uint8_t val = s_cs43l22_calculate_master_volume(percent);

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_MASTER_A_VOL, (uint8_t)val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP MASTER VOLUME");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_MASTER_B_VOL, (uint8_t)val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP MASTER VOLUME");
        }
    }

    return retval;
}

bsp_status_e cs43l22_set_pcm_volume(uint8_t percent)
{
    bsp_status_e retval = BSP_SUCCESS;

    uint8_t val = s_cs43l22_calculate_master_volume(percent);

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_PCMA_VOL, (uint8_t)val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP PCMA VOLUME");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_PCMB_VOL, (uint8_t)val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP PCMB VOLUME");
        }
    }

    return retval;
}

bsp_status_e cs43l22_set_passthrough_volume(uint8_t percent)
{
    bsp_status_e retval = BSP_SUCCESS;

    uint8_t val = s_cs43l22_calculate_master_volume(percent);

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_PASSTHROUGH_A_VOL, (uint8_t)val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP CS43L22_PASSTHROUGH_A_VOL VOLUME");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_PASSTHROUGH_B_VOL, (uint8_t)val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP CS43L22_PASSTHROUGH_B_VOL VOLUME");
        }
    }

    return retval;
}

bsp_status_e cs43l22_set_headphone_volume(uint8_t percent)
{
    bsp_status_e retval = BSP_SUCCESS;

    uint8_t val = s_cs43l22_calculate_output_volume(percent);

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_HEADPHONE_A_VOL, val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP HEADPHONE A VOLUME");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_HEADPHONE_B_VOL, val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP HEADPHONE B VOLUME");
        }
    }

    return retval;
}

bsp_status_e cs43l22_set_speaker_volume(uint8_t percent)
{
    bsp_status_e retval = BSP_SUCCESS;

    uint8_t val = s_cs43l22_calculate_output_volume(percent);

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_SPEAKER_A_VOL, val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP SPEAKER A VOLUME");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_SPEAKER_B_VOL, val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP SPEAKER B VOLUME");
        }
    }

    return retval;
}

bsp_status_e cs43l22_set_output_enabled(
        cs43l22_pwr_ctrl_e speaker_a_channel_opt,
        cs43l22_pwr_ctrl_e speaker_b_channel_opt,
        cs43l22_pwr_ctrl_e headphone_a_channel_opt,
        cs43l22_pwr_ctrl_e headphone_b_channel_opt
)
{
    bsp_status_e retval = BSP_SUCCESS;
    uint8_t val = 0;

    val = (((uint8_t)speaker_a_channel_opt)     << 0) |
          (((uint8_t)speaker_b_channel_opt)     << 2) |
          (((uint8_t)headphone_a_channel_opt)   << 4) |
          (((uint8_t)headphone_b_channel_opt)   << 6);

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_write_register(CS43L22_POWER_CTL_2, val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO SETUP MASTER VOLUME");
        }
    }

    return retval;
}

bsp_status_e cs43l22_unmute_output(void)
{
    bsp_status_e retval = BSP_SUCCESS;
    uint8_t val = 0;

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_read_register(CS43L22_PLAYBACK_CTL_1, &val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO READ PLAYBACK CTRL1 REG");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        val = 0;
        retval = s_cs43l22_write_register(CS43L22_PLAYBACK_CTL_1, val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO WRITE PLAYBACK CTRL1 REG");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_read_register(CS43L22_PLAYBACK_CTL_2, &val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO READ PLAYBACK CTRL2 REG");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        val = 0;
        retval = s_cs43l22_write_register(CS43L22_PLAYBACK_CTL_2, val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO WRITE PLAYBACK CTRL2 REG");
        }
    }

    return retval;
}

bsp_status_e cs43l22_check_speaker(bool* connected)
{
    bsp_status_e retval = BSP_SUCCESS;
    uint8_t val = 0;

    if (retval == BSP_SUCCESS)
    {
        retval = s_cs43l22_read_register(CS43L22_SPEAKER_STATUS, &val);
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO READ SPEAKER STATUS REG");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        if (val & 0x08)
        {
            LOG_INFO("SPEAKER CONNECTED!");
            if (connected != NULL)
            {
                *connected = true;
            }
        }
        else
        {
            LOG_INFO("SPEAKER NOT CONNECTED");
            if (connected != NULL)
            {
                *connected = false;
            }
        }
    }

    return retval;
}

/*
 * ********************************************************************************************************************
 * *                                                                                                                  *
 * *                                            STATIC FUNCTIONS                                                      *
 * *                                                                                                                  *
 * ********************************************************************************************************************
 */

static bsp_status_e s_cs43l22_reset_release(void)
{
    bsp_status_e retval = BSP_SUCCESS;
    uint32_t value = 0;

    if (s_cs43l22_status.reset_pin_fd < 0)
    {
        LOG_ERROR("RESET PIN FD NOT ACQUIRED");
        retval = BSP_INTERNAL_ERROR;
    }
    else
    {
        retval = driver_read(s_cs43l22_status.reset_pin_fd, &value, sizeof(value));
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO READ STRATE OF THE PORT WHERE THE C43L22 PIN RESIDES");
        }
    }

    if (retval == 0)
    {
        /* Set high to release the RESET */
        driver_set_flags(s_cs43l22_status.reset_pin_fd, CS43L22_RESET_PIN);

        value = 0;
        retval = driver_write(s_cs43l22_status.reset_pin_fd, &value, sizeof(value));
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO DEASSERT CS32L22 RESET PIN");
        }
        else
        {
            s_cs43l22_status.reset_state = C43L22_RESET_RELEASED;
            vTaskDelay(pdMS_TO_TICKS(100));
        }
    }

    if (retval == 0)
    {
        /* Set high to release the RESET */
        driver_set_flags(s_cs43l22_status.reset_pin_fd, CS43L22_RESET_PIN);

        value = 1;
        retval = driver_write(s_cs43l22_status.reset_pin_fd, &value, sizeof(value));
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO DEASSERT CS32L22 RESET PIN");
        }
        else
        {
            s_cs43l22_status.reset_state = C43L22_RESET_RELEASED;
        }
    }

    return retval;
}

static bsp_status_e s_cs43l22_write_register(uint8_t reg_address, uint8_t value)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (s_cs43l22_status.reset_state == C43L22_RESET_HELD)
    {
        LOG_ERROR("CS43L22 IS HELD RESET. CANNOT WRITE REGISTER");
        retval = BSP_INVALID_STATE;
    }

    if (retval == BSP_SUCCESS)
    {
        int ret = driver_set_flags(s_cs43l22_status.i2c_fd, (uint32_t)CS43L22_I2C_ADDRESS);
        if (ret != 0)
        {
            LOG_ERROR("FAILED OT SET I2C DEVICE ADDRESS");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        uint8_t buffer[2];
        buffer[0] = reg_address;
        buffer[1] = value;

        int ret = driver_write(s_cs43l22_status.i2c_fd, buffer, sizeof(buffer));
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO WRITE CS43L22 REGISTER");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        uint8_t buffer[2];
        buffer[0] = reg_address;
        buffer[1] = value;

        int ret = driver_read(s_cs43l22_status.i2c_fd, buffer, sizeof(buffer));
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO READ CS43L22 REGISTER");
            retval = BSP_INTERNAL_ERROR;
        }
        else
        {
            if (buffer[1] != value)
            {
                LOG_ERROR("READ VALUE MISMATCHES THE REQUESTED ONE");
                retval = BSP_INTERNAL_ERROR;
            }
        }
    }

    return retval;
}

static bsp_status_e s_cs43l22_read_register(uint8_t reg_address, uint8_t* value)
{
    bsp_status_e retval = BSP_SUCCESS;

    if (s_cs43l22_status.reset_state == C43L22_RESET_HELD)
    {
        LOG_ERROR("CS43L22 IS HELD RESET. CANNOT WRITE REGISTER");
        retval = BSP_INVALID_STATE;
    }

    if (retval == BSP_SUCCESS)
    {
        int ret = driver_set_flags(s_cs43l22_status.i2c_fd, (uint32_t)CS43L22_I2C_ADDRESS);
        if (ret != 0)
        {
            LOG_ERROR("FAILED OT SET I2C DEVICE ADDRESS");
        }
    }

    if (retval == BSP_SUCCESS)
    {
        int ret = driver_write(s_cs43l22_status.i2c_fd, &reg_address, sizeof(reg_address));
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO SET CS43L22 REGISTER READ ADDRESS");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    if (retval == BSP_SUCCESS)
    {
        int ret = driver_read(s_cs43l22_status.i2c_fd, value, sizeof(uint8_t));
        if (ret != 0)
        {
            LOG_ERROR("FAILED TO READ CS43L22 REGISTER");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    return retval;
}

static uint8_t s_cs43l22_calculate_master_volume(uint8_t perc)
{
    uint8_t convertedvol = VOLUME_CONVERT(perc);

    if(convertedvol > 0xE6)
    {
        /* Set the Master volume */
        convertedvol = convertedvol - 0xE7;
    }
    else
    {
        /* Set the Master volume */
        convertedvol = convertedvol + 0x19;
    }

    return convertedvol;
}

static uint8_t s_cs43l22_calculate_output_volume(uint8_t perc)
{
    int16_t val = 0;

//    if (perc > 100)
//    {
//        perc = 100;
//    }
//
//    val = (((uint32_t)perc * 255) / 100) - 127;

    return (uint8_t)(val & 0x00FF);
}
