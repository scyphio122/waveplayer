/*
 * cs43l22_dac.h
 *
 *  Created on: Feb 19, 2020
 *      Author: konrad
 */

#ifndef MIDDLEWARES_CS43L22_CS43L22_DAC_H_
#define MIDDLEWARES_CS43L22_CS43L22_DAC_H_

#include <bsp_types.h>
#include <stdbool.h>

typedef enum
{
    CS43L22_SAMPLE_RATE_8000Hz,
    CS43L22_SAMPLE_RATE_11025Hz,
    CS43L22_SAMPLE_RATE_12000Hz,
    CS43L22_SAMPLE_RATE_16000Hz,
    CS43L22_SAMPLE_RATE_22050Hz,
    CS43L22_SAMPLE_RATE_24000Hz,
    CS43L22_SAMPLE_RATE_32000Hz,
    CS43L22_SAMPLE_RATE_44100Hz,
    CS43L22_SAMPLE_RATE_48000Hz,
    CS43L22_SAMPLE_RATE_88235Hz,
    CS43L22_SAMPLE_RATE_96000Hz,
    CS43L22_SAMPLE_RATE_CNT
} cs43l22_sample_rate_e;

typedef enum
{
    CS43L22_MODE_SLAVE,
    CS43L22_MODE_MASTER,
    CS43L22_MODE_CNT
} cs43l22_audio_mode_e;

typedef enum
{
    CS43L22_CLOCK_POL_NOT_INVERTED,
    CS43L22_CLOCK_POL_INVERTED,
    CS43L22_CLOCK_POL_CNT
} cs43l22_clock_polarity_e;

typedef enum
{
    CS43L22_DSP_MODE_DISABLED,
    CS43L22_DSP_MODE_ENABLED,
    CS43L22_DSP_MODE_CNT
} cs43l22_dsp_mode_e;

typedef enum
{
    CS43L22_INTERFACE_LEFT_JUSTIFIED,
    CS43L22_INTERFACE_I2S,
    CS43L22_INTERFACE_RIGHT_JUSTIFIED,
    CS43L22_INTERFACE_CNT
} cs43l22_audio_if_type_e;

typedef enum
{
    CS43L22_WORD_LEN_RJ_24bit  = 1,
    CS43L22_WORD_LEN_RJ_20bit  = 2,
    CS43L22_WORD_LEN_RJ_18bit  = 3,
    CS43L22_WORD_LEN_RJ_16bit  = 4,
    CS43L22_WORD_LEN_DSP_32bit = 16,
    CS43L22_WORD_LEN_DSP_24bit = 17,
    CS43L22_WORD_LEN_DSP_20bit = 18,
    CS43L22_WORD_LEN_DSP_16bit = 19,
} cs43l22_audio_word_length_e;

typedef enum
{
    CS43L22_ON_WHEN_SPKR_HP_PIN_LO,
    CS43L22_ON_WHEN_SPKR_HP_PIN_HI,
    CS43L22_ALWAYS_ON,
    CS43L22_ALWAYS_OFF
} cs43l22_pwr_ctrl_e;

typedef enum
{
    CS43L22_PASSTHROUGH_SRC_NONE = 0,
    CS43L22_PASSTHROUGH_SRC_AIN1 = 1,
    CS43L22_PASSTHROUGH_SRC_AIN2 = 2,
    CS43L22_PASSTHROUGH_SRC_AIN3 = 4,
    CS43L22_PASSTHROUGH_SRC_AIN4 = 8
} cs43l22_passthrough_src_e;

typedef enum
{
    CS43L22_AUDIO_INPUT_MODE_I2S,
    CS43L22_AUDIO_INPUT_MODE_ANALOG
} cs43l22_audio_input_mode_e;

bsp_status_e cs43l22_initialize(void);

bsp_status_e cs43l22_reset_release(void);

bsp_status_e cs43l22_power_up(void);

bsp_status_e cs43l22_power_down(void);

bsp_status_e cs43l22_assert_chip_id(void);

bsp_status_e cs43l22_set_interface_type(
        cs43l22_audio_mode_e        mode,
        cs43l22_clock_polarity_e    polarity,
        cs43l22_dsp_mode_e          dsp_mode,
        cs43l22_audio_if_type_e     if_type,
        cs43l22_audio_word_length_e audio_word_length
);

bsp_status_e cs43l22_set_sample_rate(cs43l22_sample_rate_e sample_rate);

bsp_status_e cs43l22_set_passthrough_sources(
        cs43l22_passthrough_src_e passthrough_a,
        cs43l22_passthrough_src_e passthrough_b
);

bsp_status_e cs43l22_set_audio_mode(cs43l22_audio_input_mode_e audio_mode);

bsp_status_e cs43l22_beep_set_enabled(bool enabled);

bsp_status_e cs43l22_unmute_output(void);

bsp_status_e cs43l22_set_master_volume(uint8_t percent);

bsp_status_e cs43l22_set_pcm_volume(uint8_t percent);

bsp_status_e cs43l22_set_passthrough_volume(uint8_t percent);

bsp_status_e cs43l22_set_headphone_volume(uint8_t percent);

bsp_status_e cs43l22_set_speaker_volume(uint8_t percent);

bsp_status_e cs43l22_set_output_enabled(
        cs43l22_pwr_ctrl_e speaker_a_channel_opt,
        cs43l22_pwr_ctrl_e speaker_b_channel_opt,
        cs43l22_pwr_ctrl_e headphone_a_channel_opt,
        cs43l22_pwr_ctrl_e headphone_b_channel_opt
);

bsp_status_e cs43l22_check_speaker(bool* connected);

#endif /* MIDDLEWARES_CS43L22_CS43L22_DAC_H_ */
