/*
 * task_configs.h
 *
 *  Created on: Jan 19, 2020
 *      Author: konrad
 */

#ifndef SRC_TASK_CONFIGS_H_
#define SRC_TASK_CONFIGS_H_

#define AUDIO_SOURCE_TASK_NAME      "AUDIO_SRC_TASK"
#define AUDIO_SOURCE_TASK_PRIO      (5lu)
#define AUDIO_SOURCE_STACK_SIZE     (512)

#define AUDIO_SINK_TASK_NAME        "AUDIO_SINK_TASK"
#define AUDIO_SINK_TASK_PRIO        (1lu)
#define AUDIO_SINK_STACK_SIZE       (512)

#endif /* SRC_TASK_CONFIGS_H_ */
