#!/bin/bash

rm -rf build/*
mkdir -p build/
cd build/

cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE=arm-gcc-toolchain.cmake ..
make all -j8
