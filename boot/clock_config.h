/*
 * clock_config.h
 *
 *  Created on: Jan 26, 2020
 *      Author: konrad
 */

#ifndef BOOT_CLOCK_CONFIG_H_
#define BOOT_CLOCK_CONFIG_H_

int clock_configure(void);

#endif /* BOOT_CLOCK_CONFIG_H_ */
