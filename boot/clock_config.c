/*
 * clock_config.c
 *
 *  Created on: Jan 26, 2020
 *      Author: konrad
 */

#include <clock_config.h>
#include <stm32l4xx.h>
#include <stm32l4xx_hal_rcc.h>
#include <stm32l4xx_hal_pwr_ex.h>
#include <system_stm32l4xx.h>

#define PLLM_DIVIDER            (1lu)
#define PLLN_DIVIDER            (40lu)
#define PLLR_DIVIDER            (2lu)

#define PLLP_DIVIDER            (0lu)
#define PLLQ_DIVIDER            (0lu)

#define HCLK_DIVIDER            (1lu)
#define APB1_DIVIDER            (1lu)
#define APB2_DIVIDER            (1lu)

static int s_clock_setup_ahb_clock(void);

static int s_clock_setup_apb1_clock(void);

static int s_clock_setup_apb2_clock(void);

static int s_flash_waitcycles_config(uint32_t wait_cycles);

int clock_configure(void)
{
    int retval = 0;

    HAL_RCC_DeInit();

    /* Wait until the HSI is enabled */
    while ((RCC->CR & RCC_CR_MSIRDY_Msk) == 0)
    {

    }
    RCC->PLLCFGR = 0;

    /* Select the PLL Source as the MSI */
    RCC->PLLCFGR |= RCC_PLLCFGR_PLLSRC_MSI;

    /* Configure the dividers */
    RCC->PLLCFGR |= ((PLLM_DIVIDER - 1) << RCC_PLLCFGR_PLLM_Pos);
    RCC->PLLCFGR |= (PLLN_DIVIDER << RCC_PLLCFGR_PLLN_Pos);
    RCC->PLLCFGR |= (((PLLR_DIVIDER >> 1) - 1) << RCC_PLLCFGR_PLLR_Pos);
    RCC->PLLCFGR |= (PLLQ_DIVIDER << RCC_PLLCFGR_PLLQ_Pos);
    RCC->PLLCFGR |= (PLLP_DIVIDER << RCC_PLLCFGR_PLLP_Pos);

    /* Enable the PLL */
    RCC->PLLCFGR |= RCC_PLLCFGR_PLLREN_Msk;

    RCC->CR |= RCC_CR_PLLON_Msk;

    /* Wait until PLL is stable */
    while ((RCC->CR & RCC_CR_PLLRDY_Msk) == 0)
    {

    }

    if (retval == 0)
    {
        retval = s_flash_waitcycles_config(5);
    }

    /* Set the HCLK divider */
    if (retval == 0)
    {
        retval = s_clock_setup_ahb_clock();
    }

    /* Set the APB1 divider */
    if (retval == 0)
    {
        retval = s_clock_setup_apb1_clock();
    }

    /* Set the APB2 divider */
    if (retval == 0)
    {
        retval = s_clock_setup_apb2_clock();
    }

    /* Select the SYSCLK source as PLL */
    RCC->CFGR &= ~(RCC_CFGR_SW_0 | RCC_CFGR_SW_1);  /*< Clear the sysclk source field */
    RCC->CFGR |= RCC_CFGR_SW_PLL;                   /*<  */

    /* Wait until the SYSCLK is setup */
    while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL)
    {

    }

    if (retval == 0)
    {
        RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};
        PeriphClkInit.PeriphClockSelection      = RCC_PERIPHCLK_USB | RCC_PERIPHCLK_SAI1;

        PeriphClkInit.PLLSAI1.PLLSAI1Source     = RCC_PLLSOURCE_MSI;
        PeriphClkInit.PLLSAI1.PLLSAI1M          = PLLM_DIVIDER;
        PeriphClkInit.PLLSAI1.PLLSAI1N          = 24;   /* USB = 24, Audio - 21 */
        PeriphClkInit.PLLSAI1.PLLSAI1P          = RCC_PLLP_DIV7;
        PeriphClkInit.PLLSAI1.PLLSAI1Q          = RCC_PLLQ_DIV2;
        PeriphClkInit.PLLSAI1.PLLSAI1R          = RCC_PLLR_DIV2;
        PeriphClkInit.PLLSAI1.PLLSAI1ClockOut   = RCC_PLLSAI1_48M2CLK;

        PeriphClkInit.PLLSAI2.PLLSAI2Source     = RCC_PLLSOURCE_MSI;
        PeriphClkInit.PLLSAI2.PLLSAI2M          = PLLM_DIVIDER;
        PeriphClkInit.PLLSAI2.PLLSAI2N          = 21;
        PeriphClkInit.PLLSAI2.PLLSAI2P          = RCC_PLLP_DIV7;
        PeriphClkInit.PLLSAI2.PLLSAI2R          = RCC_PLLR_DIV2;
        PeriphClkInit.PLLSAI2.PLLSAI2ClockOut   = RCC_PLLSAI2_SAI2CLK;

        PeriphClkInit.UsbClockSelection         = RCC_USBCLKSOURCE_PLLSAI1;
        PeriphClkInit.Sai1ClockSelection        = RCC_SAI1CLKSOURCE_PLLSAI2;

        if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
        {
            retval = -1;
        }
    }

    /** Configure the main internal regulator output voltage
    */
    if (retval == 0)
    {
        if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
        {
            retval = -1;
        }
    }

    if (retval == 0)
    {
        SystemCoreClockUpdate();
    }

    return retval;
}

static int s_clock_setup_ahb_clock(void)
{
    int retval = 0;
    RCC->CFGR &= ~(RCC_CFGR_HPRE);

    switch (HCLK_DIVIDER)
    {
        case 1:
        {
            RCC->CFGR |= RCC_CFGR_HPRE_DIV1;
        } break;

        case 2:
        {
            RCC->CFGR |= RCC_CFGR_HPRE_DIV2;
        } break;

        case 4:
        {
            RCC->CFGR |= RCC_CFGR_HPRE_DIV4;
        } break;

        case 8:
        {
            RCC->CFGR |= RCC_CFGR_HPRE_DIV8;
        } break;

        case 16:
        {
            RCC->CFGR |= RCC_CFGR_HPRE_DIV16;
        } break;

        case 64:
        {
            RCC->CFGR |= RCC_CFGR_HPRE_DIV64;
        } break;

        case 128:
        {
            RCC->CFGR |= RCC_CFGR_HPRE_DIV128;

        } break;

        case 256:
        {
            RCC->CFGR |= RCC_CFGR_HPRE_DIV256;
        } break;

        case 512:
        {
            RCC->CFGR |= RCC_CFGR_HPRE_DIV512;
        } break;

        default:
        {
            retval = -1;
        }
    }

    return retval;
}

static int s_clock_setup_apb1_clock(void)
{
    int retval = 0;
    RCC->CFGR &= ~(RCC_CFGR_PPRE1);

    switch (HCLK_DIVIDER)
    {
        case 1:
        {
            RCC->CFGR |= RCC_CFGR_PPRE1_DIV1;
        } break;

        case 2:
        {
            RCC->CFGR |= RCC_CFGR_PPRE1_DIV2;
        } break;

        case 4:
        {
            RCC->CFGR |= RCC_CFGR_PPRE1_DIV4;
        } break;

        case 8:
        {
            RCC->CFGR |= RCC_CFGR_PPRE1_DIV8;
        } break;

        case 16:
        {
            RCC->CFGR |= RCC_CFGR_PPRE1_DIV16;
        } break;

        default:
        {
            retval = -1;
        }
    }

    return retval;
}

static int s_clock_setup_apb2_clock(void)
{
    int retval = 0;
    RCC->CFGR &= ~(RCC_CFGR_HPRE);

    switch (HCLK_DIVIDER)
    {
        case 1:
        {
            RCC->CFGR |= RCC_CFGR_PPRE2_DIV1;
        } break;

        case 2:
        {
            RCC->CFGR |= RCC_CFGR_PPRE2_DIV2;
        } break;

        case 4:
        {
            RCC->CFGR |= RCC_CFGR_PPRE2_DIV4;
        } break;

        case 8:
        {
            RCC->CFGR |= RCC_CFGR_PPRE2_DIV8;
        } break;

        case 16:
        {
            RCC->CFGR |= RCC_CFGR_PPRE2_DIV16;
        } break;

        default:
        {
            retval = -1;
        }
    }

    return retval;
}

static int s_flash_waitcycles_config(uint32_t wait_cycles)
{
    int retval = 0;

    FLASH->ACR |= FLASH_ACR_PRFTEN_Msk;
    FLASH->ACR |= (wait_cycles << FLASH_ACR_LATENCY_Pos);

    return retval;
}
