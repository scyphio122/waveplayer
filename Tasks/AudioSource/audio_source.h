/*
 * audio_source.h
 *
 *  Created on: Jan 21, 2020
 *      Author: konrad
 */

#ifndef TASKS_AUDIOSOURCE_AUDIO_SOURCE_H_
#define TASKS_AUDIOSOURCE_AUDIO_SOURCE_H_

#include <stdint-gcc.h>

int audio_source_initialize(void);

int audio_source_open_file(char* file_name);

int audio_source_play_pause(void);

int audio_source_stop(void);

int audio_source_set_time(uint32_t minutes, uint32_t seconds);

int audio_source_get_time(uint32_t* minutes, uint32_t* seconds);

#endif /* TASKS_AUDIOSOURCE_AUDIO_SOURCE_H_ */
