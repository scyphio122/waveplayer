/*
 * audio_source.c
 *
 *  Created on: Jan 21, 2020
 *      Author: konrad
 */

#include <audio_source.h>
#include <audio_sink.h>
#include <task_configs.h>
#include <bsp_peripheral_mapping.h>
#include <gpio.h>

#include <audio_plugin.h>
#include <wav_plugin.h>
#include <driver_manager.h>

#include <FreeRTOS.h>
#include <event_groups.h>
#include <stdio.h>
#include <task.h>
#include <stream_buffer.h>
#include <ff_stdio.h>

#include <cs43l22_dac.h>
#include <log.h>

#define AUDIO_SRC_EVT_PLAY              0x00000001lu
#define AUDIO_SRC_EVT_PAUSE             0x00000002lu
#define AUDIO_SRC_EVT_STOP              0x00000004lu

typedef enum
{
    AUDIO_SOURCE_FILE_NOT_CHOSEN,
    AUDIO_SOURCE_FILE_STOPPED,
    AUDIO_SOURCE_FILE_PLAYING,
    AUDIO_SOURCE_FILE_PAUSED
} audio_source_state_e;

static const audio_plugin_t*    s_audio_source_registered_plugins[] = {
        &s_wav_plugin_info
};

static TaskHandle_t                     s_audio_source_task_handle;

static EventGroupHandle_t               s_audio_source_task_event;

static FF_FILE*                         s_audio_source_chosen_file;

static audio_file_info_t                s_audio_file_info;

static volatile  audio_source_state_e   s_audio_source_state;

static int                              s_audio_source_used_plugin_index = -1;

static int                              s_audio_source_play_pause_button_fd = -1;

static void s_audio_source_task_worker(void* args);

static int s_audio_source_close_file(void);

int audio_source_initialize(void)
{
    int retval = 0;

    s_audio_source_state = AUDIO_SOURCE_FILE_NOT_CHOSEN;
    s_audio_source_used_plugin_index = -1;

    s_audio_source_task_event = xEventGroupCreate();
    if (s_audio_source_task_event == NULL)
    {
        LOG_ERROR("FAILED TO CREATE AUDIO SOURCE TASK EVENTS");
        retval = -1;
    }

    if (retval == 0)
    {
        retval = xTaskCreate(
                    s_audio_source_task_worker,
                    AUDIO_SOURCE_TASK_NAME,
                    AUDIO_SOURCE_STACK_SIZE,
                    NULL,
                    AUDIO_SOURCE_TASK_PRIO,
                    &s_audio_source_task_handle
                 );
        if (retval != pdPASS)
        {
            LOG_ERROR("FAILED TO CREATE AUDIO SOURCE TASK");
            retval = -1;
        }
        else
        {
            retval = 0;
        }
    }

    if (retval == 0)
    {
        s_audio_source_play_pause_button_fd = driver_open(AUDIO_BUTTON_PLAY_PORT_NAME, NULL);
        if (s_audio_source_play_pause_button_fd == -1)
        {
            LOG_ERROR("FAILED TO OPEN AUDIO BUTTON PLAY/PAUSE BUTTON");
            retval = -1;
        }
        else
        {
            bsp_ioctl_args_t    ioctl_args;
            bsp_iop_t           iop;
            ioctl_args.command  =   GPIO_SET_MODE_COMMAND;
            ioctl_args.iop      =   &iop;
            GPIO_BUILD_CONFIGURATION(&iop, AUDIO_BUTTON_PLAY_PIN, GPIO_MODE_IT_RISING, GPIO_PULL_UP, GPIO_SPEED_CONFIG_LOW, 0);

            retval = driver_ioctl(s_audio_source_play_pause_button_fd, &ioctl_args);
            if (retval != 0)
            {
                LOG_ERROR("FAILED TO CONFIGURE EXTI LINE FOR AUDIO PLAY/PAUSE BUTTON");
            }
        }
    }

    if (retval == 0)
    {
        bsp_ioctl_args_t    ioctl_args;
        bsp_iop_t           iop;
        ioctl_args.command  =   GPIO_EXTI_CONFIG_CMD;
        ioctl_args.iop      =   &iop;
        iop.data0           =   AUDIO_BUTTON_PLAY_EXTI_LINE;
        iop.data1           =   (void*)EXTI_GPIOA;
        iop.flags           =   GPIO_EXTI_MODE_INTERRUPT    |   GPIO_EXTI_TRIGGER_RISING;

        retval = driver_ioctl(s_audio_source_play_pause_button_fd, &ioctl_args);
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO CONFIGURE EXTI LINE FOR AUDIO PLAY/PAUSE BUTTON");
        }
    }

    if (retval == 0)
    {
        bsp_ioctl_args_t    ioctl_args;
        bsp_iop_t           iop;
        ioctl_args.command  =   GPIO_REGISTER_IRQ_FUNC_CB;
        ioctl_args.iop      =   &iop;
        iop.data0           =   AUDIO_BUTTON_PLAY_EXTI_LINE;
        iop.data1           =   audio_source_play_pause;

        retval = driver_ioctl(s_audio_source_play_pause_button_fd, &ioctl_args);
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO CONFIGURE EXTI LINE FOR AUDIO PLAY/PAUSE BUTTON");
        }
    }

    return retval;
}

int audio_source_open_file(char* file_name)
{
    int retval = 0;
    s_audio_source_used_plugin_index = -1;

    if (file_name == NULL)
    {
        LOG_ERROR("NULL FILE NAME PROVIDED");
        retval = -1;
    }

    if (s_audio_source_chosen_file != NULL)
    {
        LOG_ERROR("AUDIO FILE ALREADY OPENED...");
        retval = -1;
    }

    if (retval == 0)
    {
        s_audio_source_chosen_file = ff_fopen(file_name, "r");
        if (s_audio_source_chosen_file == NULL)
        {
            LOG_ERROR("FILE %s NOT FOUND", file_name);
            retval = -1;
        }
    }

    if (retval == 0)
    {
        uint32_t registered_plugins_cnt = sizeof(s_audio_source_registered_plugins)/sizeof(s_audio_source_registered_plugins[0]);

        for (uint32_t i=0; i < registered_plugins_cnt; ++i)
        {
            if (s_audio_source_registered_plugins[i]->try_file_cb(s_audio_source_chosen_file, &s_audio_file_info) == true)
            {
                s_audio_source_used_plugin_index = i;
                break;
            }
        }

        /* Valid plugin not found */
        if (s_audio_source_used_plugin_index == -1)
        {
            LOG_ERROR("REQUIRED AUDIO PLUGIN NOT REGISTERED");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        s_audio_source_state = AUDIO_SOURCE_FILE_STOPPED;
    }

    if (retval == 0)
    {
        retval = cs43l22_set_output_enabled(
                    CS43L22_ALWAYS_OFF,
                    CS43L22_ALWAYS_OFF,
                    CS43L22_ALWAYS_ON,
                    CS43L22_ALWAYS_ON
                );
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO ENABLE CS43L22 OUTPUTS");
        }
    }

    return retval;
}

int audio_source_play_pause(void)
{
    int retval = 0;

    if (
        (s_audio_source_state == AUDIO_SOURCE_FILE_STOPPED) ||
        (s_audio_source_state == AUDIO_SOURCE_FILE_PAUSED)
       )
    {
        xEventGroupSetBitsFromISR(s_audio_source_task_event, AUDIO_SRC_EVT_PLAY, NULL);
    }
    else
    if (s_audio_source_state == AUDIO_SOURCE_FILE_PLAYING)
    {
        xEventGroupSetBitsFromISR(s_audio_source_task_event, AUDIO_SRC_EVT_PAUSE, NULL);
    }

    return retval;
}

int audio_source_stop(void)
{
    int retval = 0;

    if (s_audio_source_state != AUDIO_SOURCE_FILE_PLAYING)
    {
        LOG_ERROR("FILE IS NOT PLAYING. CANNOT STOP IT");
        retval = -1;
    }

    if (retval == 0)
    {
        xEventGroupSetBits(s_audio_source_task_event, AUDIO_SRC_EVT_STOP);
    }

    return retval;
}

int audio_source_set_time(uint32_t minutes, uint32_t seconds)
{
    int retval = 0;


    return retval;
}

int audio_source_get_time(uint32_t* minutes, uint32_t* seconds)
{
    int retval = 0;


    return retval;
}

static void s_audio_source_task_worker(void* args)
{
    int retval = 0;
    uint32_t event = 0;
    uint8_t* buffer         = NULL;
    uint32_t buffer_size    = 0;

    LOG_INFO("AUDIO SOURCE TASK STARTED");

    while (1)
    {
        event = xEventGroupWaitBits(s_audio_source_task_event, 0xFF, 0xFF, false, 0);
        if (event != 0)
        {
            switch (event)
            {
                case AUDIO_SRC_EVT_PLAY:
                {
                    if (s_audio_source_state == AUDIO_SOURCE_FILE_STOPPED)
                    {
                        LOG_INFO("STARTING PLAYING THE SONG");
                    }
                    else
                    {
                        LOG_INFO("RESUMING CURRENTLY PLAYED SONG");
                    }

                    s_audio_source_state = AUDIO_SOURCE_FILE_PLAYING;
                } break;

                case AUDIO_SRC_EVT_PAUSE:
                {
                    LOG_INFO("PAUSING CURRENTLY PLAYED SONG");
                    s_audio_source_state = AUDIO_SOURCE_FILE_PAUSED;
                } break;

                case AUDIO_SRC_EVT_STOP:
                {
                    LOG_INFO("STOPPING CURRENTLY PLAYED SONG");
                    s_audio_source_state = AUDIO_SOURCE_FILE_STOPPED;
                    ff_fseek(s_audio_source_chosen_file, s_audio_file_info.samples_start_offset, SEEK_SET);

                } break;

                default:
                {
                    LOG_ERROR("UNRECOGNIZED EVENT: %lu", event);
                }
            }
        }

        if (s_audio_source_state == AUDIO_SOURCE_FILE_PLAYING)
        {
            retval = audio_sink_get_free_buffer(&buffer, &buffer_size);
            if (retval == 0)
            {
                uint32_t samples_read = 0;
                if (s_audio_source_used_plugin_index != -1)
                {
                    retval = s_audio_source_registered_plugins[s_audio_source_used_plugin_index]->read_next_samples_cb(buffer, buffer_size, &samples_read);
                    if (retval != 0)
                    {
                        LOG_ERROR("FAILED TO READ DATA USING AUDIO PLUGIN: %s", s_audio_source_registered_plugins[s_audio_source_used_plugin_index]->plugin_name);
                    }

                    /* If end of file or any error detected - close the file */
                    if ((samples_read == 0) || (retval != 0))
                    {
                        s_audio_source_registered_plugins[s_audio_source_used_plugin_index]->close_file_cb();
                        s_audio_source_close_file();

                        retval = audio_source_open_file("/dev/sda/They_Taking_The_Hobbits_To_Isengard.wav");
                        if (retval != 0)
                        {
                            LOG_ERROR("FAILED TO OPEN SONG");
                        }

                        /* Buffer was taken but no more samples so clear the buffer and return it to the available pool */
                        memset(buffer, 0, buffer_size);
                        retval = audio_sink_push_samples(buffer);
                        if (retval != 0)
                        {
                            LOG_ERROR("FAILED TO PUSH FULL BUFFER TO THE SINK QUEUE");
                        }
                    }
                    else
                    {
                        /* Push valid samples */
                        retval = audio_sink_push_samples(buffer);
                        if (retval != 0)
                        {
                            LOG_ERROR("FAILED TO PUSH FULL BUFFER TO THE SINK QUEUE");
                        }
                    }
                }
                else
                {
                    LOG_ERROR("NO VALID PLUGIN AVAILABLE TO READ THE FILE DETECTED");
                    retval = -1;
                }
            }
        }
        else
        {
            /* Not playing so generate silence */
            retval = audio_sink_get_free_buffer(&buffer, &buffer_size);
            if (retval == 0)
            {
                memset(buffer, 0, buffer_size);

                retval = audio_sink_push_samples(buffer);
                if (retval != 0)
                {
                    LOG_ERROR("FAILED TO PUSH FULL BUFFER TO THE SINK QUEUE");
                }
            }

            vTaskDelay(pdMS_TO_TICKS(10));
        }
    }
}

static int s_audio_source_close_file(void)
{
    int retval = 0;

    if (s_audio_source_chosen_file == NULL)
    {
        LOG_ERROR("FILE NOT OPENED");
        retval = -1;
    }

    if (retval == 0)
    {
        LOG_INFO("CLOSING SONG FILE");
        ff_fclose(s_audio_source_chosen_file);
        s_audio_source_chosen_file = NULL;
        s_audio_source_state = AUDIO_SOURCE_FILE_NOT_CHOSEN;

        retval = cs43l22_set_output_enabled(
                    CS43L22_ALWAYS_OFF,
                    CS43L22_ALWAYS_OFF,
                    CS43L22_ALWAYS_OFF,
                    CS43L22_ALWAYS_OFF
                );
        if (retval != BSP_SUCCESS)
        {
            LOG_ERROR("FAILED TO DISABLE CS43L22 OUTPUTS");
        }
    }

    return retval;
}
