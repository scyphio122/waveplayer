/*
 * audio_sink.h
 *
 *  Created on: Jan 21, 2020
 *      Author: konrad
 */

#ifndef TASKS_AUDIOSINK_AUDIO_SINK_H_
#define TASKS_AUDIOSINK_AUDIO_SINK_H_

#include <stdint-gcc.h>

int audio_sink_initialize(void);

int audio_sink_get_free_buffer(uint8_t** data, uint32_t* data_size);

int audio_sink_volume_up(void);

int audio_sink_volume_down(void);

int audio_sink_push_samples(uint8_t* data);

#endif /* TASKS_AUDIOSINK_AUDIO_SINK_H_ */
