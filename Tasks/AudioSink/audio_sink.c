/*
 * audio_sink.c
 *
 *  Created on: Jan 21, 2020
 *      Author: konrad
 */

#include <audio_sink.h>
#include <task_configs.h>

#include <bsp_sai.h>
#include <gpio.h>
#include <bsp_peripheral_mapping.h>
#include <driver_manager.h>
#include <cs43l22_dac.h>

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <log.h>

#include <stdint-gcc.h>

#define     AUDIO_EVENT_QUEUE_SIZE                  4
#define     AUDIO_BUFFERS_CNT                       2
#define     AUDIO_SINK_SAMPLES_BUFFER_SIZE          (2 * 8192u)

#define     AUDIO_SINK_EVT_VOL_UP                   0x00000001lu
#define     AUDIO_SINK_EVT_VOL_DOWN                 0x00000002lu

static void s_audio_sink_throw_volume_up_evt(void);

static void s_audio_sink_throw_volume_down_evt(void);

static int s_audio_sink_volume_up(void);

static int s_audio_sink_volume_down(void);

static void s_audio_sink_task_worker(void* args);

static TaskHandle_t             s_audio_sink_task_handle;

static QueueHandle_t            s_audio_sink_empty_buffers_handle;

static QueueHandle_t            s_audio_sink_full_buffers_handle;

static QueueHandle_t            s_audio_sink_evt_queue;

static uint8_t*                 s_current_audio_buffer;

__attribute__((section(".sram2")))
static uint8_t                  s_audio_buffer[AUDIO_BUFFERS_CNT][AUDIO_SINK_SAMPLES_BUFFER_SIZE];

static int                      s_sai_fd = -1;

static int8_t                   s_volume_level = 0;

static volatile bool            s_sink_playing;

static int                      s_audio_sink_vol_up_fd = -1;

static int                      s_audio_sink_vol_down_fd = -1;

int audio_sink_initialize(void)
{
    int retval = 0;

    s_volume_level = 75;

    cs43l22_set_master_volume(s_volume_level);

    if (retval == 0)
    {
        bsp_sai_config_t arg;
        arg.audio_frequency = BSP_SAI_AUDIO_FREQUENCY_22K;
        arg.bits_per_sample = BSP_SAI_BITS_PER_SAMPLE_16BIT;
        arg.number_of_slot  = 2;

        s_sai_fd = driver_open("/dev/sai1", &arg);
        if (s_sai_fd < 0)
        {
            LOG_ERROR("FAILED TO OPEN SAI INTERFACE");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        s_audio_sink_evt_queue = xQueueCreate(AUDIO_EVENT_QUEUE_SIZE, sizeof(uint32_t));
        if (s_audio_sink_evt_queue == NULL)
        {
            LOG_ERROR("FAILED TO CREATE AUDIO SINK EVENT QUEUE");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        s_audio_sink_empty_buffers_handle = xQueueCreate(AUDIO_BUFFERS_CNT, sizeof(uint8_t*));
        if (s_audio_sink_empty_buffers_handle == NULL)
        {
            LOG_ERROR("FAILED TO CREATE AUDIO SINK EMPTY BUFFERS QUEUE");
            retval = -1;
        }
        else
        {
            for (uint32_t i = 0; i<AUDIO_BUFFERS_CNT; ++i)
            {
                uint8_t* buf = s_audio_buffer[i];
                if (xQueueSend(s_audio_sink_empty_buffers_handle, &buf, 0) == errQUEUE_FULL)
                {
                    LOG_ERROR("FAILED TO PUISH AUDIO SINK EMPTY BUFFER %lu TO THE QUEUE", i);
                    retval = BSP_INTERNAL_ERROR;
                }
            }
        }
    }

    if (retval == 0)
    {
        /* Set the full audio buffers queue to be one smaller than the empty - to block the audio source */
        s_audio_sink_full_buffers_handle = xQueueCreate(AUDIO_BUFFERS_CNT, sizeof(uint8_t*));
        if (s_audio_sink_full_buffers_handle == NULL)
        {
            LOG_ERROR("FAILED TO CREATE AUDIO SINK FULL BUFFERS QUEUE");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        retval = xTaskCreate(
                    s_audio_sink_task_worker,
                    AUDIO_SINK_TASK_NAME,
                    AUDIO_SINK_STACK_SIZE,
                    NULL,
                    AUDIO_SINK_TASK_PRIO,
                    &s_audio_sink_task_handle
                 );
        if (retval != pdPASS)
        {
            LOG_ERROR("FAILED TO CREATE AUDIO SINK TASK");
            retval = -1;
        }
        else
        {
            retval = 0;
        }
    }

    if (retval == 0)
    {
        s_audio_sink_vol_up_fd = driver_open(AUDIO_BUTTON_UP_PORT_NAME, NULL);
        if (s_audio_sink_vol_up_fd == -1)
        {
            LOG_ERROR("FAILED TO OPEN AUDIO BUTTON VOLUME UP BUTTON");
            retval = -1;
        }
        else
        {
            bsp_ioctl_args_t    ioctl_args;
            bsp_iop_t           iop;
            ioctl_args.command  =   GPIO_SET_MODE_COMMAND;
            ioctl_args.iop      =   &iop;
            iop.data0 = GPIO_MODE_IT_RISING;
            iop.flags = AUDIO_BUTTON_UP_PIN                    |
                        GPIO_PULL_DOWN                         |
                        GPIO_SPEED_CONFIG_LOW;

            retval = driver_ioctl(s_audio_sink_vol_up_fd, &ioctl_args);
            if (retval != 0)
            {
                LOG_ERROR("FAILED TO CONFIGURE EXTI LINE FOR AUDIO VOLUME UP BUTTON");
            }
        }
    }
#if 0
    if (retval == 0)
    {
        bsp_ioctl_args_t    ioctl_args;
        bsp_iop_t           iop;
        ioctl_args.command  =   GPIO_EXTI_CONFIG_CMD;
        ioctl_args.iop      =   &iop;
        iop.data0           =   AUDIO_BUTTON_UP_EXTI_LINE;
        iop.data1           =   (void*)EXTI_GPIOA;
        iop.flags           =   GPIO_EXTI_MODE_INTERRUPT    |   GPIO_EXTI_TRIGGER_RISING;

        retval = driver_ioctl(s_audio_sink_vol_up_fd, &ioctl_args);
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO CONFIGURE EXTI LINE FOR AUDIO VOLUME UP BUTTON");
        }
    }
#endif

    if (retval == 0)
    {
        bsp_ioctl_args_t    ioctl_args;
        bsp_iop_t           iop;
        ioctl_args.command  =   GPIO_REGISTER_IRQ_FUNC_CB;
        ioctl_args.iop      =   &iop;
        iop.data0           =   AUDIO_BUTTON_UP_EXTI_LINE;
        iop.data1           =   s_audio_sink_throw_volume_up_evt;

        retval = driver_ioctl(s_audio_sink_vol_up_fd, &ioctl_args);
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO CONFIGURE EXTI LINE FOR AUDIO VOLUME UP BUTTON");
        }
    }

    if (retval == 0)
    {
        s_audio_sink_vol_down_fd = driver_open(AUDIO_BUTTON_DOWN_PORT_NAME, NULL);
        if (s_audio_sink_vol_down_fd == -1)
        {
            LOG_ERROR("FAILED TO OPEN AUDIO VOLUME DOWN BUTTON");
            retval = -1;
        }
        else
        {
            bsp_ioctl_args_t    ioctl_args;
            bsp_iop_t           iop;
            ioctl_args.command  =   GPIO_SET_MODE_COMMAND;
            ioctl_args.iop      =   &iop;
            iop.data0 = GPIO_MODE_IT_RISING;
            iop.flags = AUDIO_BUTTON_DOWN_PIN                    |
                        GPIO_PULL_DOWN                           |
                        GPIO_SPEED_CONFIG_LOW;

            retval = driver_ioctl(s_audio_sink_vol_down_fd, &ioctl_args);
            if (retval != 0)
            {
                LOG_ERROR("FAILED TO CONFIGURE EXTI LINE FOR AUDIO VOLUME DOWN BUTTON");
            }
        }
    }

#if 0
    if (retval == 0)
    {
        bsp_ioctl_args_t    ioctl_args;
        bsp_iop_t           iop;
        ioctl_args.command  =   GPIO_EXTI_CONFIG_CMD;
        ioctl_args.iop      =   &iop;
        iop.data0           =   AUDIO_BUTTON_DOWN_EXTI_LINE;
        iop.data1           =   (void*)EXTI_GPIOA;
        iop.flags           =   GPIO_EXTI_MODE_INTERRUPT    |   GPIO_EXTI_TRIGGER_RISING;

        retval = driver_ioctl(s_audio_sink_vol_down_fd, &ioctl_args);
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO CONFIGURE EXTI LINE FOR AUDIO VOLUME DOWN BUTTON");
        }
    }
#endif

    if (retval == 0)
    {
        bsp_ioctl_args_t    ioctl_args;
        bsp_iop_t           iop;
        ioctl_args.command  =   GPIO_REGISTER_IRQ_FUNC_CB;
        ioctl_args.iop      =   &iop;
        iop.data0           =   AUDIO_BUTTON_DOWN_EXTI_LINE;
        iop.data1           =   s_audio_sink_throw_volume_down_evt;

        retval = driver_ioctl(s_audio_sink_vol_down_fd, &ioctl_args);
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO CONFIGURE EXTI LINE FOR AUDIO VOLUME DOWN BUTTON");
        }
    }

    return retval;
}

int audio_sink_get_free_buffer(uint8_t** data, uint32_t* data_size)
{
    int retval = 0;

    if (data == NULL)
    {
        LOG_ERROR("NULL DATA PTR PROVIDED");
        retval = -1;
    }

    if (data_size == NULL)
    {
        LOG_ERROR("NULL DATA SIZE PTR PROVIDED");
        retval = -1;
    }

    if (s_audio_sink_empty_buffers_handle == NULL)
    {
        LOG_ERROR("NULL EMPTY BUFFERS QUEUE");
        retval = -1;
    }

    if (retval == 0)
    {
        uint8_t* buf = NULL;
        xQueueReceive(s_audio_sink_empty_buffers_handle, &buf, portMAX_DELAY);
        *data = buf;
        *data_size = AUDIO_SINK_SAMPLES_BUFFER_SIZE;
    }

    return retval;
}

int audio_sink_push_samples(uint8_t* data)
{
    int retval = 0;

    if (data == NULL)
    {
        LOG_ERROR("AUDIO NULL DATA PTR");
        retval = -1;
    }

    if (s_audio_sink_full_buffers_handle == NULL)
    {
        LOG_ERROR("NULL FULL BUFFER QUEUE");
        retval = -1;
    }

    if (retval == 0)
    {
        bool found = false;
        for (uint32_t i=0; i<AUDIO_BUFFERS_CNT; ++i)
        {
            if (data == s_audio_buffer[i])
            {
                found = true;
                break;
            }
        }

        if (found == false)
        {
            LOG_ERROR("INVALID BUFFER PROVIDED. ONLY BUFFERS ACQUIRED FROM AUDIO SINK ARE ALLOWED");
            retval = -1;
        }
    }

    if (retval == 0)
    {
        xQueueSend(s_audio_sink_full_buffers_handle, &data, portMAX_DELAY);
    }

    return retval;
}


static void s_audio_sink_throw_volume_up_evt(void)
{
    if (s_audio_sink_evt_queue != NULL)
    {
        uint32_t event = AUDIO_SINK_EVT_VOL_UP;
        xQueueSendFromISR(s_audio_sink_evt_queue, &event, NULL);
    }
}

static void s_audio_sink_throw_volume_down_evt(void)
{
    if (s_audio_sink_evt_queue != NULL)
    {
        uint32_t event = AUDIO_SINK_EVT_VOL_DOWN;
        xQueueSendFromISR(s_audio_sink_evt_queue, &event, NULL);
    }
}

static int s_audio_sink_volume_up(void)
{
    int retval = 0;

    if (s_volume_level < 50)
    {
        s_volume_level += 10;
    }
    else
    if (s_volume_level < 75)
    {
        s_volume_level += 5;
    }
    else
    if (s_volume_level < 90)
    {
        s_volume_level += 2;
    }
    else
    {
        s_volume_level += 1;
    }

    if (s_volume_level > 100)
    {
        s_volume_level = 100;
    }

    LOG_TRACE("VOLUME UP: %hd%%", s_volume_level);

    if (retval == 0)
    {
        retval = cs43l22_set_master_volume(s_volume_level);
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO INCREASE VOLUME LEVEL");
        }
    }

    return retval;
}

static int s_audio_sink_volume_down(void)
{
    int retval = 0;

    if (s_volume_level <= 50)
    {
        s_volume_level -= 10;
    }
    else
    if (s_volume_level <= 75)
    {
        s_volume_level -= 5;
    }
    else
    if (s_volume_level <= 90)
    {
        s_volume_level -= 2;
    }
    else
    {
        s_volume_level -= 1;
    }

    if (s_volume_level < 0)
    {
        s_volume_level = 0;
    }

    LOG_TRACE("VOLUME DOWN: %hd%%", s_volume_level);

    if (retval == 0)
    {
        retval = cs43l22_set_master_volume(s_volume_level);
        if (retval != 0)
        {
            LOG_ERROR("FAILED TO LOWER VOLUME LEVEL");
        }
    }

    return retval;
}

static void s_audio_sink_task_worker(void* args)
{
    LOG_INFO("AUDIO SINK TASK STARTED");

    int retval = 0;

    while (1)
    {
        if (s_audio_sink_evt_queue != NULL)
        {
            uint32_t event = 0;
            if (xQueueReceive(s_audio_sink_evt_queue, &event, 0) == pdTRUE)
            {
                if (event & AUDIO_SINK_EVT_VOL_UP)
                {
                    retval = s_audio_sink_volume_up();
                    if (retval != 0)
                    {
                        LOG_ERROR("FAILED TO VOLUME UP");
                    }
                }

                if (event & AUDIO_SINK_EVT_VOL_DOWN)
                {
                    retval = s_audio_sink_volume_down();
                    if (retval != 0)
                    {
                        LOG_ERROR("FAILED TO VOLUME DOWN");
                    }
                }
            }
        }

        if (s_sink_playing == false)
        {
            if (xQueueReceive(s_audio_sink_full_buffers_handle, &s_current_audio_buffer, pdMS_TO_TICKS(200)) == pdTRUE)
            {
                retval = driver_write(s_sai_fd, s_audio_buffer, sizeof(s_audio_buffer));
                if (retval != 0)
                {
                    LOG_ERROR("FAILED TO WRITE AUDIO SAMPLES...");
                }
                else
                {
                    s_sink_playing = true;
                }
            }
        }
        else
        {
            bsp_sai_event_e sai_event = 0;
            bsp_sai_get_events(&sai_event, pdMS_TO_TICKS(1000));

            if (sai_event != 0)
            {
                if (sai_event & BSP_SAI_EVENT_TX_COMPLETED)
                {
                    if (xQueueSend(s_audio_sink_empty_buffers_handle, &s_current_audio_buffer, 0) == errQUEUE_FULL)
                    {
                        LOG_ERROR("FAILED TO PUSH EMPTY BUFFER TO THE QUEUE");
                    }

                    if (xQueueReceive(s_audio_sink_full_buffers_handle, &s_current_audio_buffer, 0) == errQUEUE_EMPTY)
                    {
                        LOG_ERROR("FAILED TO GET ANOTHER EMPTY BUFFER");
                        s_sink_playing = false;
                    }
                }

                if (sai_event & BSP_SAI_EVENT_TX_HALF_COMPLETED)
                {
                    if (xQueueSend(s_audio_sink_empty_buffers_handle, &s_current_audio_buffer, 0) == errQUEUE_FULL)
                    {
                        LOG_ERROR("FAILED TO PUSH EMPTY BUFFER TO THE QUEUE");
                    }

                    if (xQueueReceive(s_audio_sink_full_buffers_handle, &s_current_audio_buffer, 0) == errQUEUE_EMPTY)
                    {
                        LOG_ERROR("FAILED TO GET ANOTHER EMPTY BUFFER");
                        s_sink_playing = false;
                    }
                }

                if (s_sink_playing == false)
                {
                    bsp_ioctl_args_t arg;
                    arg.command = BSP_SAI_IOCTL_CMD_ABORT;
                    retval = driver_ioctl(s_sai_fd, &arg);
                    if (retval != 0)
                    {
                        LOG_ERROR("FAILED TO ABORT THE SAI TRANSFER");
                    }
                }
            }
        }

    }
}
