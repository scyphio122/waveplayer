/*
 * usb_storage_task.h
 *
 *  Created on: Feb 13, 2020
 *      Author: konrad
 */

#ifndef TASKS_USB_USB_STORAGE_TASK_H_
#define TASKS_USB_USB_STORAGE_TASK_H_

#include <bsp_types.h>

bsp_status_e usb_storage_task_init(void);

int usb_store_data(uint8_t *buf, uint32_t blk_addr, uint16_t blk_len);

#endif /* TASKS_USB_USB_STORAGE_TASK_H_ */
