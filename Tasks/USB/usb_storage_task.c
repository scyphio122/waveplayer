/*
 * usb_storage_task.c
 *
 *  Created on: Feb 13, 2020
 *      Author: konrad
 */

#include <usb_storage_task.h>
#include <ff_qspi_disk.h>

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>

#include <driver_manager.h>
#include <log.h>

#define BUFFERS_CNT         8

typedef struct
{
    uint32_t block_addr;
    uint32_t block_len;
    uint8_t  data[QSPI_SECTOR_SIZE];
} usb_storage_data_t;

static void s_usb_storage_task_worker(void* args);

static QueueHandle_t        s_usb_queue;

static TaskHandle_t         s_usb_storage_task_handle;

static int                  s_memblk_fd = -1;

bsp_status_e usb_storage_task_init(void)
{
    bsp_status_e retval = 0;

    s_usb_queue = xQueueCreate(BUFFERS_CNT, sizeof(usb_storage_data_t));
    if (s_usb_queue == NULL)
    {
        LOG_ERROR("FAILED TO CREATE USB STORAGE QUEUE");
        retval = BSP_INTERNAL_ERROR;
    }

    if (retval == BSP_SUCCESS)
    {
        if (xTaskCreate(
                s_usb_storage_task_worker,
                "UsbStorage",
                512,
                NULL,
                2,
                &s_usb_storage_task_handle) != pdTRUE)
        {
            LOG_ERROR("FAILED TO CREATE USB STORAGE TASK");
            retval = BSP_INTERNAL_ERROR;
        }
    }

    s_memblk_fd = driver_open("/dev/memblk", NULL);
    if (s_memblk_fd < 0)
    {
        LOG_ERROR("FAILED TO OPEN BLOCK MEMORY DEVICE");
    }

    return retval;
}

int usb_store_data(uint8_t *buf, uint32_t blk_addr, uint16_t blk_len)
{
    int retval = 0;

//    BaseType_t high_prio_task_woken = 0;

//    while (blk_len != 0)
//    {
//        usb_storage_data_t elem;
//        elem.block_addr = blk_addr;
//        elem.block_len  = blk_len;
//        memcpy(elem.data, buf, QSPI_SECTOR_SIZE);
//
//        if (xQueueSendFromISR(s_usb_queue, &elem, &high_prio_task_woken) == errQUEUE_FULL)
//        {
//            retval = -1;
//            break;
//        }
//        else
//        {
//            blk_len--;
//        }
//    }

    if (retval == 0)
    {
        retval = driver_fseek(s_memblk_fd, blk_addr * QSPI_SECTOR_SIZE, SEEK_SET);
        if (retval != 0)
        {
            abort();
        }
    }

    if (retval == 0)
    {
        retval = driver_write(s_memblk_fd, buf, blk_len * QSPI_SECTOR_SIZE);
        if (retval != 0)
        {
            abort();
        }
    }

    return retval;
}

static void s_usb_storage_task_worker(void* args)
{
    usb_storage_data_t elem;

    int retval = 0;
    int memblk_fd = -1;

    memblk_fd = driver_open("/dev/memblk", NULL);
    if (memblk_fd < 0)
    {
        LOG_ERROR("FAILED TO OPEN BLOCK MEMORY DEVICE");
    }

    while (1)
    {
        xQueueReceive(s_usb_queue, &elem, portMAX_DELAY);

        LOG_TRACE("USB: STORING DATA AT BLOCK ADDRESS: 0x%08X. DATA LEN: %lu", elem.block_addr * QSPI_SECTOR_SIZE, elem.block_len);
        if (memblk_fd != -1)
        {
            if (retval == 0)
            {
                retval = driver_fseek(memblk_fd, elem.block_addr * QSPI_SECTOR_SIZE, SEEK_SET);
                if (retval != 0)
                {
                    abort();
                }
            }

            if (retval == 0)
            {
                retval = driver_write(memblk_fd, elem.data, elem.block_len * QSPI_SECTOR_SIZE);
                if (retval != 0)
                {
                    abort();
                }
            }
        }
    }
}
